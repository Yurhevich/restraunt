package com.epam.restraunt.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.restraunt.entity.Staff;

public class StaffPageFilter implements Filter {

	private static final String LOCATION = "location";		
	private  String redirectPage;
	
	@Override
	public void init(FilterConfig config) throws ServletException {
		 redirectPage = config.getInitParameter(LOCATION);
	}
	

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        HttpSession session = req.getSession();
        
        Staff staff = (Staff)session.getAttribute("staff");
        
        if (staff==null) {
        	resp.sendRedirect(redirectPage);
        }
       
        chain.doFilter(request, response);
		
	}

	
	@Override
	public void destroy() {
		
	}
	
}
