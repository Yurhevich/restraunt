package com.epam.restraunt.validate;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import com.epam.restraunt.entity.Staff;

public final class Validator {

	final String REGEX_LOGIN_AND_PASSWORD = "([A-Za-z0-9_-]){4,16}";
	final String REGEX_NAME_SURNAME = "[[A-ZА-Я]([a-zа-я]+)]+";
	final String REGEX_EMAIL = "[_A-Za-z0-9-]+(.[_A-Za-z0-9-]+)*@[_A-Za-z0-9-]+.[a-z]{2,}";
	final String NAME = "name";
	final String LOGIN = "login";
	final String PASSWORD = "password";
	final String SURNAME = "surname";
	final String EMAIL = "email";
	final String IMAGE_JPEG = "image/jpeg";
	final String IMAGE_PJPEG = "image/pjpeg";

	private static final Validator validator = new Validator();

	private Validator() {
	}

	public static Validator getInstance() {
		return validator;
	}

	public boolean validateUserRegistrationParameters(HttpServletRequest request) {

		if (!Pattern.matches(REGEX_LOGIN_AND_PASSWORD, request.getParameter(LOGIN))) {
			return false;
		}
		if (!Pattern.matches(REGEX_LOGIN_AND_PASSWORD, request.getParameter(PASSWORD))) {
			return false;
		}
		if (!Pattern.matches(REGEX_NAME_SURNAME, request.getParameter(NAME))) {
			return false;
		}

		if (!Pattern.matches(REGEX_NAME_SURNAME, request.getParameter(SURNAME))) {
			return false;
		}

		if (!Pattern.matches(REGEX_EMAIL, request.getParameter(EMAIL))) {
			return false;
		}
		return true;
	}

	public boolean validateStaffParameters(Staff staff) {

		if (!Pattern.matches(REGEX_LOGIN_AND_PASSWORD, staff.getLogin())) {
			return false;
		}
		if (!Pattern.matches(REGEX_LOGIN_AND_PASSWORD, staff.getPassword())) {
			return false;
		}
		if (!Pattern.matches(REGEX_NAME_SURNAME, staff.getName())) {
			return false;
		}

		if (!Pattern.matches(REGEX_NAME_SURNAME, staff.getSurname())) {
			return false;
		}

		if (!Pattern.matches(REGEX_EMAIL, staff.getEmail())) {
			return false;
		}

		if (staff.getPost() == null || staff.getPost().isEmpty()) {
			return false;
		}

		if (staff.getTelephone() == null || staff.getTelephone().isEmpty()) {
			return false;
		}

		if (staff.getAddress() == null || staff.getAddress().isEmpty()) {
			return false;
		}

		return true;

	}

	public boolean validateStaffPhoto(Part part) {

		String mimeType = part.getContentType().trim();
		if (mimeType.equals(IMAGE_JPEG) || mimeType.equals(IMAGE_PJPEG)) {
			return true;
		}

		return false;

	}

	public boolean validateLoginParameters(HttpServletRequest request) {
		if (!Pattern.matches(REGEX_LOGIN_AND_PASSWORD, request.getParameter(LOGIN))) {
			return false;
		}
		if (!Pattern.matches(REGEX_LOGIN_AND_PASSWORD, request.getParameter(PASSWORD))) {
			return false;
		}
		return true;
	}

}
