package com.epam.restraunt.listener;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


import com.epam.restraunt.dao.pool.ConnectionPool;
import com.epam.restraunt.dao.pool.Database;

public class FinishingApplicationListener implements ServletContextListener {
	
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		ConnectionPool.getInstance().dispose();
	}
	

	@Override
	public void contextInitialized(ServletContextEvent event) {
	}
	
}
