package com.epam.restraunt.controller;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.restraunt.command.CommandExecutor;
import com.epam.restraunt.command.ajax.AjaxCommandEnum;
import com.epam.restraunt.command.ajax.AjaxCommandExecutor;

@MultipartConfig
public class Controller extends HttpServlet {

	private static final String ACTION = "action";

	private static final long serialVersionUID = 1L;

	@Override
	public void init() {
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		processRequest(request, response);
	}

	@Override
	public void destroy() {
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println(request.getContentType());

		   System.out.println(Locale.getDefault());

		   System.out.println("Encoding: "+response.getCharacterEncoding());
		   System.out.println("Encoding: "+request.getCharacterEncoding());
		   System.out.println(System.getProperty("java.class.path"));
		Enumeration<String> f = request.getParameterNames();
		
		while(f.hasMoreElements()){
			String name= f.nextElement();
			System.out.println(name + " " + request.getParameter(name));
		}
		
		String command = request.getParameter(ACTION).toUpperCase().trim();

		if (AjaxCommandEnum.isAjaxCommand(command)) {
			String responseData = AjaxCommandExecutor.execute(request, command);
			response.getWriter().write(responseData);
			

		} else {
			String page = CommandExecutor.execute(request, command);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
			response.setContentType("text/html;charset=UTF-8");
			dispatcher.forward(request, response);
		}

	}

}
