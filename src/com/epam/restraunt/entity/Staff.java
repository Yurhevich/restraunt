package com.epam.restraunt.entity;

public class Staff extends Person {

	private String post;
	private String photo;
	private String telephone;
	private String address;
	boolean isAdmin;

	

	public Staff() {
	}

	public Staff(int id, String name, String surname, String login, String password, String email, String post,
			String photo, String telephone, String address, boolean isAdmin) {
		super(id, name, surname, login, password, email);
		this.post = post;
		this.photo = photo;
		this.telephone = telephone;
		this.address = address;
		this.isAdmin = isAdmin;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	

	
	
}
