package com.epam.restraunt.entity;

public class MenuItem extends Entity {
	private String name;
	private int price;
	private int discount;

	public MenuItem() {
	}

	public MenuItem(int id, String name, int price, int discount) {
		super(id);
		this.name = name;
		this.price = price;
		this.discount = discount;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}
	
	


	

	

	
	
}
