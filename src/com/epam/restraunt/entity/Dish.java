package com.epam.restraunt.entity;

public class Dish extends MenuItem {

	private int timeToCook;
	private String picture;
	private String consists;

	public Dish() {
	}

	public Dish(int id, String name, int price, int discount, int timeToCook, String picture, String consists) {
		super(id, name, price, discount);
		this.timeToCook = timeToCook;
		this.picture = picture;
		this.consists = consists;
	}

	public int getTimeToCook() {
		return timeToCook;
	}

	public void setTimeToCook(int timeToCook) {
		this.timeToCook = timeToCook;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getConsists() {
		return consists;
	}

	public void setConsists(String consists) {
		this.consists = consists;
	}



	
	
	

}
