package com.epam.restraunt.entity;

public class Client extends Person {

	private int balance;

	public Client() {
	}

	public Client(int id, String name, String surname, String login, String password, String email, int balance,int discount) {
		super(id, name, surname, login, password, email);
		this.balance = balance;
	}

	
	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}


	
	
}
