package com.epam.restraunt.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Order extends Entity {

	private int idClient;
	private Date time;
	private int totalCost;
	private String state;
	private boolean inPlace;

	private Map<MenuItem, Integer> items;

	public Order() {
	}

	public Order(int id, int idClient, int totalCost, String state, boolean inPlace) {
		super(id);
		this.idClient = idClient;
		this.totalCost = totalCost;
		this.state = state;
		this.inPlace = inPlace;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public int getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(int totalCost) {
		this.totalCost = totalCost;
	}

	public String getState() {
		return state;
	}

	public void setStatus(String state) {
		this.state = state;
	}

	public boolean isInPlace() {
		return inPlace;
	}

	public void setInPlace(boolean inPlace) {
		this.inPlace = inPlace;
	}


	public Map<MenuItem, Integer> getItems() {
		return items;
	}

	public void setItems(Map<MenuItem, Integer> items) {
		this.items = items;
	}



	
	
}
