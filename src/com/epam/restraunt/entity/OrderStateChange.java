package com.epam.restraunt.entity;

import java.util.Date;

public class OrderStateChange extends Entity {
	
private int idOrder;
private Staff staff;
private String establishedState;
private Date timeOfChange;

public OrderStateChange(){
	super();
}

public OrderStateChange(int id,int idOrder, Staff staff, String establishedState, Date timeOfChange) {
	super(id);
	this.idOrder = idOrder;
	this.staff  = staff;
	this.establishedState = establishedState;
	this.timeOfChange = timeOfChange;
}

public int getIdOrder() {
	return idOrder;
}

public void setIdOrder(int idOrder) {
	this.idOrder = idOrder;
}

public Staff getStaff() {
	return staff;
}

public void setStaff(Staff staff) {
	this.staff = staff;
}

public String getEstablishedState() {
	return establishedState;
}

public void setEstablishedState(String establishedState) {
	this.establishedState = establishedState;
}

public Date getTimeOfChange() {
	return timeOfChange;
}

public void setTimeOfChange(Date timeOfChange) {
	this.timeOfChange = timeOfChange;
}


}
