package com.epam.restraunt.entity;

public class Delivery extends Entity {

	private int orderId;
	private String telephone;
	private String address;

	public Delivery() {
	}

	public Delivery(int id, int orderId, String telephone, String address) {
		super(id);
		this.orderId = orderId;
		this.telephone = telephone;
		this.address = address;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	

	
}
