package com.epam.restraunt.entity;

public class Drink extends MenuItem {
	private int volume;
	private boolean alcoholic;

	public Drink() {
		super();
	}

	public Drink(int id, String name, int price, int discount, int volume, boolean alcoholic) {
		super(id, name, price, discount);
		this.volume = volume;
		this.alcoholic = alcoholic;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public boolean isAlcoholic() {
		return alcoholic;
	}

	public void setAlcoholic(boolean alcoholic) {
		this.alcoholic = alcoholic;
	}

	

	
}
