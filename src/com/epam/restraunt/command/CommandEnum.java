package com.epam.restraunt.command;

public enum CommandEnum {
	
	REGISTRATION,
	LOGIN,
	LOGOUT,
	VIEW_MENU,
	REMOVE_ACC,
	ORDER,
	VIEW_HISTORY,
	LOCAL,
	VIEW_ORDER_INFO,
	VIEW_STAFF_INFO,
	VIEW_MENU_ITEM,
	VIEW_CLIENT_INFO;
	
	
	//more
}
