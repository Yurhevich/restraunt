package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.command.builder.BuilderFactory;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.StaffService;
import com.epam.restraunt.service.exception.ServiceException;
import com.epam.restraunt.validate.Validator;

public class AjaxUpdateStaff implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		if (!staff.isAdmin()) {
			return NOT_ALLOWED;
		}

		Builder<Staff> builder = BuilderFactory.getStaffBuilder();
		Staff updatedStaff = null;
		String idUpdatedStaff = request.getParameter(ID);
		int id = 0;

		try {
			id = Integer.parseInt(idUpdatedStaff);
		} catch (NumberFormatException e) {
			return FAIL;
		}

		try {
			updatedStaff = builder.create(request);
		} catch (Exception e) {
			return NOT_VALID;
		}

		updatedStaff.setId(id);

		Validator validator = Validator.getInstance();

		if (validator.validateStaffParameters(updatedStaff)) {

			StaffService service = ServiceFactory.getStaffService();

			try {

				service.updateStaff(updatedStaff);
				return SUCCESS;

			} catch (ServiceException e) {
				return FAIL;
			}

		} else {
			return NOT_VALID;
		}
	}

}
