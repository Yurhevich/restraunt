package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.command.builder.BuilderFactory;
import com.epam.restraunt.entity.Dish;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class AjaxChangeDish implements AjaxCommand{

	@Override
	public String execute(HttpServletRequest request)  {
		
		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		if (!staff.isAdmin()) {
			return NOT_ALLOWED;
		}
		
		String idUpdatedDish = request.getParameter(ID);
		int id = 0;

		try {
			id = Integer.parseInt(idUpdatedDish);
		} catch (NumberFormatException e) {
			return FAIL;
		}
		

		Builder<Dish> builder = BuilderFactory.getDishBuilder();

			
		Dish updatedDish = null;

		try{
			updatedDish =  builder.create(request);
		}catch(NumberFormatException e){
			return NOT_VALID;
		}
		
		
		updatedDish.setId(id);
	
			MenuService service = ServiceFactory.getMenuService();

			try {

				service.updateDish(updatedDish);
				return SUCCESS;
			} catch (ServiceException e) {
				return FAIL;
			}

		
	}

}
