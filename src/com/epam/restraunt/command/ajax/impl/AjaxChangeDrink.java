package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.command.builder.BuilderFactory;
import com.epam.restraunt.entity.Drink;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class AjaxChangeDrink implements AjaxCommand{

	@Override
	public String execute(HttpServletRequest request)  {
		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		if (!staff.isAdmin()) {
			return NOT_ALLOWED;
		}
		
		String idUpdatedDrink = request.getParameter(ID);
		int id = 0;

		try {
			id = Integer.parseInt(idUpdatedDrink);
		} catch (NumberFormatException e) {
			return FAIL;
		}
		

		Builder<Drink> builder = BuilderFactory.getDrinkBuilder();

			
		Drink updatedDrink = null;

		try{
			updatedDrink =  builder.create(request);
		}catch(NumberFormatException e){
			return NOT_VALID;
		}
		
		
		updatedDrink.setId(id);
	
			MenuService service = ServiceFactory.getMenuService();

			try {

				service.updateDrink(updatedDrink);
				return SUCCESS;
			} catch (ServiceException e) {
				return FAIL;
			}
	}

}
