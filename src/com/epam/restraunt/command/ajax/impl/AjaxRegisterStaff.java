package com.epam.restraunt.command.ajax.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.command.builder.BuilderFactory;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.StaffService;
import com.epam.restraunt.service.exception.ServiceException;
import com.epam.restraunt.validate.Validator;

@MultipartConfig
public class AjaxRegisterStaff implements AjaxCommand {

	private final static String PHOTO = "photo";

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		if (!staff.isAdmin()) {
			return NOT_ALLOWED;
		}

		Builder<Staff> builder = BuilderFactory.getStaffBuilder();

		Staff newStaff = builder.create(request);

		Validator validator = Validator.getInstance();

		if (validator.validateStaffParameters(newStaff)) {
			Part part;
			try {
				part = request.getPart(PHOTO);
			} catch (ServletException e) {
				return FAIL;
			}
			if (validator.validateStaffPhoto(part)) {

				Path path = Paths
						.get(request.getServletContext().getRealPath("img") + "/photo/" + part.getSubmittedFileName());

				try (InputStream filecontent = part.getInputStream();) {
					Files.copy(filecontent, path);

				} catch (Exception e) {
					return FAIL;
				}

				newStaff.setPhoto("/restraunt/img/photo/" + part.getSubmittedFileName());

				StaffService service = ServiceFactory.getStaffService();

				try {

					if (service.registrateStaff(newStaff)) {
						return SUCCESS;
					} else {
						return ALREADY_EXISTS;

					}
				} catch (ServiceException e) {
					return FAIL;
				}

			} else {
				return FAIL;
			}

		} else {
			return FAIL;

		}

	}
}