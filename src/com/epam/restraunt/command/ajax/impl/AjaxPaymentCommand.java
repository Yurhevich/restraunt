package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.command.builder.BuilderFactory;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.entity.Delivery;
import com.epam.restraunt.entity.Order;
import com.epam.restraunt.service.OrderService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class AjaxPaymentCommand implements AjaxCommand {

	private static final String EMPTY_ORDER = "empty_order";
	private static final String NEED_MONEY = "need_money";
	private static final String YES = "yes";
	private static final String NO = "no";

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Client client = (Client) request.getSession().getAttribute(CLIENT);

		if (client == null) {
			return NEED_LOGIN;
		}

		Order order = (Order) request.getSession().getAttribute(ORDER);
		OrderService service = ServiceFactory.getOrderService();

		if (order == null || order.getTotalCost() == 0) {
			return EMPTY_ORDER;
		}

		int debt = order.getTotalCost();

		if (client.getBalance() < debt) {

			return NEED_MONEY;
		}

		String inPlace = request.getParameter(IN_PLACE);

		if (inPlace.equals(NO)) {

			Builder<Delivery> builder = BuilderFactory.getDeliveryBuilder();
			Delivery delivery = builder.create(request);

			if (delivery.getAddress() == null || delivery.getAddress().isEmpty()) {
				return FAIL;
			}

			if (delivery.getTelephone() == null || delivery.getTelephone().isEmpty()) {
				return FAIL;
			}

			try {
				order.setInPlace(false);
				service.addOrderWithDelivery(order, delivery);
				client.setBalance(client.getBalance() - debt);
				order.setItems(null);
				order.setTotalCost(0);
				return SUCCESS;
			} catch (ServiceException e) {
				return FAIL;
			}

		} else if (inPlace.equals(YES)) {
			try {
				order.setInPlace(true);
				service.addOrder(order);
				client.setBalance(client.getBalance() - debt);
				order.setItems(null);
				order.setTotalCost(0);
				return SUCCESS;
			} catch (ServiceException e) {
				return FAIL;
			}

		} else {
			return FAIL;

		}

	}

}
