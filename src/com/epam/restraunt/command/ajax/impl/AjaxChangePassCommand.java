package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.service.ClientService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class AjaxChangePassCommand implements AjaxCommand {

	private static final String PASSWORD = "password";
	private static final String NEW_PASSWORD = "newPassword";
	private static final String WRONG_PASS = "wrong_pass";

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Client client = (Client) request.getSession().getAttribute(CLIENT);

		if (client == null) {
			return NEED_LOGIN;
		}

		int clientId = client.getId();
		String password = request.getParameter(PASSWORD);
		String newPassword = request.getParameter(NEW_PASSWORD);
		ClientService service = ServiceFactory.getClientService();
		try {
			if (service.checkPassword(clientId, password)) {
				service.changePassword(clientId, newPassword);

				return SUCCESS;
			} else {
				return WRONG_PASS;
			}
		} catch (ServiceException e) {
			return FAIL;
		}

	}

}
