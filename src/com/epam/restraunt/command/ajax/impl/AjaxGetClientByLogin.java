package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.ClientService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;
import com.google.gson.Gson;

public class AjaxGetClientByLogin implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		if (!staff.isAdmin()) {
			return NOT_ALLOWED;
		}

		String login = request.getParameter(NAME);

		if (login == null) {
			return FAIL;
		}

		Client client = null;

		ClientService service = ServiceFactory.getClientService();
		Map<String, Object> mapJson = new HashMap<>(2);

		try {
			client = service.fetchByLogin(login);
			if (client == null) {
				return NOT_FOUND;
			}

		} catch (ServiceException e) {
			return FAIL;
		}

		mapJson.put(NUM_OF_PAGES, 1);
		mapJson.put(CLIENT, client);

		return new Gson().toJson(mapJson);

	}

}
