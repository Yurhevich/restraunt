package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.ClientService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.StaffService;
import com.epam.restraunt.service.exception.ServiceException;
import com.google.gson.Gson;

public class AjaxGetAllClients implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		if (!staff.isAdmin()) {
			return NOT_ALLOWED;
		}

		int page = 1;

		String numberPage = request.getParameter(PAGE);

		if (numberPage != null) {
			try {
				page = Integer.parseInt(numberPage);
			} catch (NumberFormatException e) {
				return FAIL;
			}
		}

		List<Client> listClients = null;
		int numberOfPages = 0;
		Map<String, Object> mapJson = new HashMap<>(2);

		ClientService service = ServiceFactory.getClientService();

		try {
			listClients = service.getAllClients((page - 1) * ORDERS_PER_PAGE, ORDERS_PER_PAGE);

			if (numberPage == null) {
				int countOfRecords = service.getCountOfClients();
				numberOfPages = (int) Math.ceil(countOfRecords * 1.0 / ORDERS_PER_PAGE);
				mapJson.put(NUM_OF_PAGES, numberOfPages);
			}

		} catch (ServiceException e) {
			return FAIL;
		}

		mapJson.put(LIST_CLIENTS, listClients);

		return new Gson().toJson(mapJson);

	}

}
