package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Order;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.OrderService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;
import com.google.gson.Gson;

public class AjaxViewStaffHistory implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		int page = 1;

		String numberPage = request.getParameter(PAGE);

		if (numberPage != null) {
			try {
				page = Integer.parseInt(numberPage);
			} catch (NumberFormatException e) {
				return FAIL;
			}
		}

		int numberOfPages = 0;
		Map<String, Object> mapJson = new HashMap<>(2);
		List<Order> orders = null;

		OrderService service = ServiceFactory.getOrderService();

		try {
			orders = service.getOrdersByStaffId(staff.getId(), (page - 1) * ORDERS_PER_PAGE, ORDERS_PER_PAGE);
			if (numberPage == null) {
				int countOfRecords = service.getCountOfServedOrders(staff.getId());
				numberOfPages = (int) Math.ceil(countOfRecords * 1.0 / ORDERS_PER_PAGE);
				mapJson.put(NUM_OF_PAGES, numberOfPages);
			}
		} catch (ServiceException e) {
			return FAIL;
		}

		mapJson.put(ORDERS, orders);

		return new Gson().toJson(mapJson);

	}

}
