package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.command.builder.BuilderFactory;
import com.epam.restraunt.entity.Dish;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;
import com.epam.restraunt.validate.Validator;

@MultipartConfig
public class AjaxAddDishToMenu implements AjaxCommand {

	private static final String IMG = "img";

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);
		ResourceManager manager = ResourceManager.getInstance();
		

		if (staff == null) {
			return NEED_LOGIN;
		}

		if (!staff.isAdmin()) {
			return NOT_ALLOWED;
		}

		Builder<Dish> builder = BuilderFactory.getDishBuilder();

		Dish newDish = builder.create(request);

		Validator validator = Validator.getInstance();

		Part part;
		try {
			part = request.getPart(PICTURE);
		} catch (ServletException e) {
			return FAIL;
		}
		if (validator.validateStaffPhoto(part)) {

			Path path = Paths.get(request.getServletContext().getRealPath(IMG) + "/" + part.getSubmittedFileName());

			try (InputStream filecontent = part.getInputStream();) {
				Files.copy(filecontent, path);
			} catch (Exception e) {
				return FAIL;
			}

			newDish.setPicture("/restraunt/img/" + part.getSubmittedFileName());

			MenuService service = ServiceFactory.getMenuService();

			try {

				service.addDish(newDish);
				return SUCCESS;
			} catch (ServiceException e) {
				return FAIL;
			}

		} else {
			return FAIL;
		}

	}

}
