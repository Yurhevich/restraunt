package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.MenuItem;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;
import com.google.gson.Gson;

public class AjaxGetAllDishes implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		int page = 1;

		String numberPage = request.getParameter(PAGE);

		if (numberPage != null) {
			try {
				page = Integer.parseInt(numberPage);
			} catch (NumberFormatException e) {
				return FAIL;
			}
		}

		List<MenuItem> listItems = null;
		int numberOfPages = 0;
		Map<String, Object> mapJson = new HashMap<>(2);

		MenuService service = ServiceFactory.getMenuService();

		try {
			listItems = service.getAllDishes((page - 1) * ORDERS_PER_PAGE, ORDERS_PER_PAGE);

			if (numberPage == null) {
				int countOfRecords = service.getCountOfDrinks();
				numberOfPages = (int) Math.ceil(countOfRecords * 1.0 / ORDERS_PER_PAGE);
				mapJson.put(NUM_OF_PAGES, numberOfPages);
			}

		} catch (ServiceException e) {
			return FAIL;
		}

		mapJson.put(MENU_ITEMS, listItems);

		return new Gson().toJson(mapJson);

	}

}
