package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.service.ClientService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class AjaxDepositCommand implements AjaxCommand {

	private static final String SUM = "sum";

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Client client = (Client) request.getSession().getAttribute(CLIENT);
		if (client == null) {
			return NEED_LOGIN;
		}

		ClientService service = ServiceFactory.getClientService();
		int depositSum;

		try {
			depositSum = Integer.parseInt(request.getParameter(SUM));

		} catch (NumberFormatException e) {
			return FAIL;
		}

		int clientId = client.getId();

		try {
			service.deposit(clientId, depositSum);
			client.setBalance(client.getBalance() + depositSum);
			return String.valueOf(client.getBalance());
		} catch (ServiceException e) {
			return FAIL;
		}

	}

}
