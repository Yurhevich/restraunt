package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.OrderStateChange;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.OrderService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;
import com.google.gson.Gson;

public class AjaxViewOrderChanges implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		String idOrder = request.getParameter(ORDER_ID);
		int id;

		if (idOrder == null) {
			return FAIL;
		}

		try {
			id = Integer.parseInt(idOrder);
		} catch (NumberFormatException e) {
			return FAIL;
		}

		List<OrderStateChange> changes = null;

		OrderService orderService = ServiceFactory.getOrderService();

		try {

			changes = orderService.getOrderStateChanges(id);

		} catch (ServiceException e) {
			return FAIL;
		}

		return new Gson().toJson(changes);

	}

}
