package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.StaffService;
import com.epam.restraunt.service.exception.ServiceException;
import com.google.gson.Gson;

public class AjaxGetStaffByName implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		if (!staff.isAdmin()) {
			return NOT_ALLOWED;
		}

		String surname = request.getParameter(NAME);

		if (surname == null || surname.isEmpty()) {
			return FAIL;
		}

		Staff person = null;

		StaffService service = ServiceFactory.getStaffService();
		Map<String, Object> mapJson = new HashMap<>(2);

		try {
			person = service.getMainInfoAboutStaff(surname);
			if (person == null) {
				return NOT_FOUND;
			}

		} catch (ServiceException e) {
			return FAIL;
		}

		mapJson.put(NUM_OF_PAGES, 1);
		mapJson.put(STAFF, person);

		return new Gson().toJson(mapJson);

	}

}
