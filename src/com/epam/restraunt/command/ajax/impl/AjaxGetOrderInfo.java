package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Delivery;
import com.epam.restraunt.entity.MenuItem;
import com.epam.restraunt.entity.Order;
import com.epam.restraunt.entity.OrderStateChange;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.OrderService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;
import com.google.gson.Gson;

public class AjaxGetOrderInfo implements AjaxCommand {

	private final static String CURRENT_STATE = "current";
	private final static String NEXT_STATE = "next";

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;

		}

		String idOrder = request.getParameter(ID);
		int id;

		if (idOrder == null) {
			return FAIL;
		}

		try {
			id = Integer.parseInt(idOrder);
		} catch (NumberFormatException e) {
			return FAIL;
		}
		Order order = null;

		Map<MenuItem, Integer> items = null;
		List<OrderStateChange> changes = null;
		Delivery delivery = null;
		Map<String, String> stateInfo = new HashMap<String, String>(2);

		MenuService menuService = ServiceFactory.getMenuService();
		OrderService orderService = ServiceFactory.getOrderService();

		try {
			order = orderService.getOrderById(id);
			items = menuService.getItemsByOrderId(id);
			changes = orderService.getOrderStateChanges(id);
			String cur = orderService.getCurrentStateOfOrder(id);

			stateInfo.put(CURRENT_STATE, cur);

			stateInfo.put(NEXT_STATE, orderService.getNextState(cur, order.isInPlace()));

			if (!order.isInPlace()) {
				delivery = orderService.getDeliveryById(id);
			}

		} catch (ServiceException e) {
			return FAIL;
		}

		Map<String, String> itemsMap = new HashMap<>(items.size());

		for (Map.Entry<MenuItem, Integer> pair : items.entrySet()) {
			itemsMap.put(pair.getKey().getName(), String.valueOf(pair.getValue()));
		}

		Map<String, Object> resultMap = null;

		if (delivery == null) {
			resultMap = new HashMap<String, Object>(3);
		} else {
			resultMap = new HashMap<String, Object>(4);
			resultMap.put(DELIVERY, delivery);
		}

		resultMap.put(TOTAL_COST, order.getTotalCost());
		resultMap.put(ID_CLIENT, order.getIdClient());
		resultMap.put(ITEMS, itemsMap);
		resultMap.put(CHANGES, changes);
		resultMap.put(STATE, stateInfo);
		resultMap.put(ID_ORDER, order.getId());

		return new Gson().toJson(resultMap);

	}

}
