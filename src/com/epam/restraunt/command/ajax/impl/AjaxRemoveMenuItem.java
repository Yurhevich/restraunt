package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.command.builder.BuilderFactory;
import com.epam.restraunt.entity.Dish;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class AjaxRemoveMenuItem implements AjaxCommand {

	private static final String DISH = "dish";
	private static final String DRINK = "drink";

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		if (!staff.isAdmin()) {
			return NOT_ALLOWED;
		}

		String idDeletedDish = request.getParameter(ID);
		int id = 0;

		try {
			id = Integer.parseInt(idDeletedDish);
		} catch (NumberFormatException e) {
			return FAIL;
		}

		String type = request.getParameter(TYPE);

		if (type == null || type.isEmpty()) {
			return FAIL;
		}

		MenuService service = ServiceFactory.getMenuService();

		if (type.equals(DISH)) {
			try {

				service.deleteDishFromMenu(id);
				return SUCCESS;
			} catch (ServiceException e) {
				return FAIL;
			}
		} else if (type.equals(DRINK)) {
			try {

				service.deleteDrinkFromMenu(id);
				return SUCCESS;
			} catch (ServiceException e) {
				return FAIL;
			}

		} else
			return FAIL;

	}

}
