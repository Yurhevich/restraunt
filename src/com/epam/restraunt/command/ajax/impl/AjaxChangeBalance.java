package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.ClientService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class AjaxChangeBalance implements AjaxCommand {

	private static final String SUM = "sum";

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		if (!staff.isAdmin()) {
			return NOT_ALLOWED;
		}

		String idUpdatedUser = request.getParameter(ID);
		int id = 0;

		try {
			id = Integer.parseInt(idUpdatedUser);
		} catch (NumberFormatException e) {
			return FAIL;
		}

		String balance = request.getParameter(SUM);
		int invoice = 0;

		try {
			invoice = Integer.parseInt(balance);
		} catch (NumberFormatException e) {
			return FAIL;
		}

		ClientService service = ServiceFactory.getClientService();

		try {
			service.setInvoice(id, invoice);
			return SUCCESS;
		} catch (ServiceException e) {
			return FAIL;
		}

	}

}
