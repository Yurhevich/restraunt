package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.command.builder.BuilderFactory;
import com.epam.restraunt.entity.Dish;
import com.epam.restraunt.entity.Drink;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;
import com.epam.restraunt.validate.Validator;

public class AjaxAddDrinkToMenu implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		if (!staff.isAdmin()) {
			return NOT_ALLOWED;
		}

		Builder<Drink> builder = BuilderFactory.getDrinkBuilder();

		Drink newDrink = builder.create(request);

		// Validator validator = Validator.getInstance();

		MenuService service = ServiceFactory.getMenuService();

		try {
			service.addDrink(newDrink);
			return SUCCESS;
		} catch (ServiceException e) {
			return FAIL;
		}

	}

}
