package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.StaffService;
import com.epam.restraunt.service.exception.ServiceException;

public class AjaxDeleteStaff implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;
		}

		if (!staff.isAdmin()) {
			return NOT_ALLOWED;
		}

		String idStaff = request.getParameter(ID);
		int id = 0;

		try {
			id = Integer.parseInt(idStaff);
		} catch (NumberFormatException e) {
			return FAIL;
		}
		
		StaffService service = ServiceFactory.getStaffService();

		try {

			service.deleteStaff(id);
			return SUCCESS;

		} catch (ServiceException e) {
			return FAIL;
		}

	}

}
