package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Order;
import com.epam.restraunt.entity.OrderStateChange;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.OrderService;
import com.epam.restraunt.service.OrderState;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class AjaxUpdateOrderState implements AjaxCommand {

	private static final Object LOCK = new Object();

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return NEED_LOGIN;

		}

		String idOrder = request.getParameter(ORDER_ID);
		String newState = request.getParameter(STATE);

		int id;

		if (idOrder == null || newState == null || newState.isEmpty()) {
			return FAIL;
		}

		try {
			id = Integer.parseInt(idOrder);
		} catch (NumberFormatException e) {
			return FAIL;
		}

		String state = OrderState.checkState(newState);

		if (state == null) {
			return FAIL;
		}

		OrderService service = ServiceFactory.getOrderService();

		synchronized (LOCK) {

			Order order;

			try {
				order = service.getOrderById(id);
			} catch (ServiceException e) {
				return FAIL;
			}

			if (!staff.isAdmin()) {

				String expectedState = OrderState.getNextState(order.getState(), order.isInPlace());

				if (!expectedState.equals(newState)) {
					return NOT_ALLOWED;
				}

			} else {

				if (order.getState().equals(state)) {
					return NOT_ALLOWED;
				}
			}

			OrderStateChange change = new OrderStateChange(0, id, staff, state, new Date());

			try {
				service.updateOrderState(change);
			} catch (ServiceException e) {
				return FAIL;
			}

		}
		return SUCCESS;

	}

}
