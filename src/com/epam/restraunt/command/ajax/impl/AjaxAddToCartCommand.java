package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.entity.MenuItem;
import com.epam.restraunt.entity.Order;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class AjaxAddToCartCommand implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Client client = (Client) request.getSession().getAttribute(CLIENT);
		if (client == null) {
			return NEED_LOGIN;
		}

		String idItem = request.getParameter(ID_ITEM);
		int id;

		if (idItem == null) {
			return FAIL;
		}

		try {
			id = Integer.parseInt(idItem);
		} catch (NumberFormatException e) {
			return NEED_LOGIN;
		}

		Order order = (Order) request.getSession().getAttribute(ORDER);
		Map<MenuItem, Integer> map = null;

		if (order == null) {
			order = new Order();
			map = new LinkedHashMap<>();
			order.setItems(map);
			request.getSession().setAttribute(ORDER, order);
		} else {
			map = order.getItems();
			if (map == null) {
				map = new LinkedHashMap<>();
				order.setItems(map);
			}
		}

		MenuService service = ServiceFactory.getMenuService();

		try {
			MenuItem item = service.getMenuItemById(id);
			if (map.containsKey(item)) {
				Integer count = map.get(item);
				map.put(item, count + 1);
			} else {
				map.put(item, 1);
			}

			int updatedTotalCost = order.getTotalCost() + item.getPrice();

			order.setTotalCost(updatedTotalCost);

			return SUCCESS;

		} catch (ServiceException e) {
			return FAIL;
		}

	}

}
