package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.entity.MenuItem;
import com.epam.restraunt.entity.Order;
import com.google.gson.Gson;

public class AjaxRemoveFromCartCommand implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Client client = (Client) request.getSession().getAttribute(CLIENT);
		if (client == null) {
			return NEED_LOGIN;
		}

		String idItem = request.getParameter(ID_ITEM);

		int id;

		if (idItem == null) {
			return FAIL;
		}

		try {
			id = Integer.parseInt(idItem);
		} catch (NumberFormatException e) {
			return FAIL;
		}

		Order order = (Order) request.getSession().getAttribute(ORDER);
		Map<MenuItem, Integer> map = null;

		if (order == null) {
			return FAIL;
		}

		map = order.getItems();

		if (map == null || map.isEmpty()) {
			return FAIL;
		}

		boolean found = false;
		int count = 0;
		int price = 0;
		Iterator<Entry<MenuItem, Integer>> iterator = map.entrySet().iterator();

		while (iterator.hasNext()) {
			Entry<MenuItem, Integer> pair = iterator.next();

			if (pair.getKey().getId() == id) {
				count = pair.getValue();
				price = pair.getKey().getPrice();
				if (count > 1) {
					pair.setValue(--count);
				} else {
					count = 0;
					iterator.remove();
				}
				found = true;
				break;
			}
		}

		if (found) {

			List<String> list = new ArrayList<>(2);
			list.add(String.valueOf(count));

			int updatedTotalCost = order.getTotalCost() - price;

			order.setTotalCost(updatedTotalCost);

			list.add(String.valueOf(updatedTotalCost));

			return new Gson().toJson(list);

		} else {
			return FAIL;
		}

	}

}
