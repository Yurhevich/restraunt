package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.command.builder.BuilderFactory;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.StaffService;
import com.epam.restraunt.service.exception.ServiceException;
import com.epam.restraunt.validate.Validator;

public class AjaxLoginStaffCommand implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		HttpSession session = request.getSession();

		Builder<Staff> builder = BuilderFactory.getStaffBuilder();
		StaffService staffService = ServiceFactory.getStaffService();
		Staff staff = builder.create(request);
		Validator validator = Validator.getInstance();

		if (validator.validateLoginParameters(request)) {
			try {

				if (staffService.login(staff)) {
					session.setAttribute(STAFF, staff);
					return SUCCESS;
				} else {
					return NOT_VALID;

				}
			} catch (ServiceException e) {
				return FAIL;
			}

		} else {
			return NOT_VALID;
		}

	}

}
