package com.epam.restraunt.command.ajax.impl;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceManager {

	private static final ResourceManager instance = new ResourceManager();

	private ResourceManager() {

	}

	public static ResourceManager getInstance() {
		return instance;
	}

	public String getString(String locale, String key){
		Locale local = new Locale(key);
		ResourceBundle bundle = ResourceBundle.getBundle(key, local);
		return bundle.getString(key);
	}
	
}
