package com.epam.restraunt.command.ajax.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.ajax.AjaxCommand;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.entity.MenuItem;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;
import com.google.gson.Gson;

public class AjaxViewOrderItems implements AjaxCommand {

	@Override
	public String execute(HttpServletRequest request) throws IOException {

		Client client = (Client) request.getSession().getAttribute(CLIENT);

		if (client == null) {
			Staff staff = (Staff) request.getSession().getAttribute(STAFF);
			if (staff == null) {
				return NEED_LOGIN;
			}
		}

		String idOrder = request.getParameter(ORDER_ID);
		int id;

		if (idOrder == null) {
			return FAIL;
		}

		try {
			id = Integer.parseInt(idOrder);
		} catch (NumberFormatException e) {
			return FAIL;
		}

		Map<MenuItem, Integer> items = null;

		MenuService menuService = ServiceFactory.getMenuService();

		try {
			items = menuService.getItemsByOrderId(id);

		} catch (ServiceException e) {
			return FAIL;
		}

		Map<String, String> mapJson = new HashMap<>();
		for (Map.Entry<MenuItem, Integer> pair : items.entrySet()) {
			mapJson.put(pair.getKey().getName(), String.valueOf(pair.getValue()));
		}

		return new Gson().toJson(mapJson);

	}

}
