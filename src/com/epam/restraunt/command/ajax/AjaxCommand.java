package com.epam.restraunt.command.ajax;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;


public interface AjaxCommand {

	String ID_ITEM = "itemId";
	String SUCCESS = "success";
	String ORDER = "order";
	String CLIENT = "client";
	String STAFF = "staff";
	String FAIL = "failed";
	String IN_PLACE = "inPlace";
	String ORDER_ID = "idOrder";
	String NEED_LOGIN = "need_login";
	String TEXT_PLAIN = "text/plain";
	String NOT_VALID = "not_valid";
	String STATE = "state";
	String ITEMS = "items";
	String ORDERS = "orders";
    String CHANGES = "changes";
    String DELIVERY = "delivery";
    String PAGE = "page";
    String NUM_OF_PAGES = "count_pages";
    String TOTAL_COST = "totalCost";
    String ID_STAFF = "idStaff";
    String ID_ORDER = "idOrder";
    String ID_CLIENT = "idClient";
    String NOT_ALLOWED = "not_allowed";
    String LIST_STAFF = "list_staff";
    String LIST_CLIENTS = "list_clients";
    String ID = "id";
    String NOT_FOUND = "not_found";
    String NAME = "name";
    String ALREADY_EXISTS = "already_exist";
    String MENU_ITEMS = "menu_items";
    String PICTURE = "picture";
    String TYPE = "type";
    
    int ORDERS_PER_PAGE = 8;

	String execute(HttpServletRequest request) throws IOException;

}
