package com.epam.restraunt.command.ajax;

public enum AjaxCommandEnum {
	
	ADD_TO_CART,
	REMOVE_FROM_CART,
	PAY,
	DEPOSIT,
	CHANGE_PASSWORD,
	VIEW_ORDER_ITEMS,
	LOGIN_STAFF,
	GET_ORDERS_BY_STATE,
	TODAY_ORDERS,
	YESTARDAY_ORDERS,
	GET_STAFF_ORDERS_HISTORY,
	GET_ORDER_CHANGES,
	GET_ORDER_INFO,
	UPDATE_ORDER_STATE,
	GET_ALL_PERSONAL,
	GET_STAFF_BY_ID,
	GET_STAFF_BY_NAME,
	REGISTER_STAFF,
	GET_ALL_DISHES,
	GET_ALL_DRINKS,
	ADD_DISH,
	ADD_DRINK,
	GET_ALL_CLIENTS,
	GET_CLIENT_BY_ID,
	GET_CLIENT_BY_LOGIN,
	CLIENT_ORDER_HISTORY,
	UPDATE_STAFF,
	DELETE_STAFF,
	CHANGE_DISH,
	CHANGE_DRINK,
	REMOVE_ITEM,
	REMOVE_USER,
	CHANGE_BALANCE;

	public static boolean isAjaxCommand(String command) {
		for (AjaxCommandEnum c : AjaxCommandEnum.values()) {
			if (c.name().equalsIgnoreCase(command)) {
				return true;
			}
		}

		return false;
	}
}
