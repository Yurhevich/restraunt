package com.epam.restraunt.command.ajax;

import java.io.IOException;
import java.util.EnumMap;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.ajax.impl.AjaxAddDishToMenu;
import com.epam.restraunt.command.ajax.impl.AjaxAddDrinkToMenu;
import com.epam.restraunt.command.ajax.impl.AjaxAddToCartCommand;
import com.epam.restraunt.command.ajax.impl.AjaxChangeBalance;
import com.epam.restraunt.command.ajax.impl.AjaxChangeDish;
import com.epam.restraunt.command.ajax.impl.AjaxChangeDrink;
import com.epam.restraunt.command.ajax.impl.AjaxChangePassCommand;
import com.epam.restraunt.command.ajax.impl.AjaxClientOrderHistory;
import com.epam.restraunt.command.ajax.impl.AjaxDeleteStaff;
import com.epam.restraunt.command.ajax.impl.AjaxDepositCommand;
import com.epam.restraunt.command.ajax.impl.AjaxGetAllClients;
import com.epam.restraunt.command.ajax.impl.AjaxGetAllDishes;
import com.epam.restraunt.command.ajax.impl.AjaxGetAllDrinks;
import com.epam.restraunt.command.ajax.impl.AjaxGetAllPersonal;
import com.epam.restraunt.command.ajax.impl.AjaxGetChangesMadeByStaff;
import com.epam.restraunt.command.ajax.impl.AjaxGetClientById;
import com.epam.restraunt.command.ajax.impl.AjaxGetClientByLogin;
import com.epam.restraunt.command.ajax.impl.AjaxGetDefindStaff;
import com.epam.restraunt.command.ajax.impl.AjaxGetOrderInfo;
import com.epam.restraunt.command.ajax.impl.AjaxGetStaffByName;
import com.epam.restraunt.command.ajax.impl.AjaxGetTodayOrders;
import com.epam.restraunt.command.ajax.impl.AjaxGetYestardayOrders;
import com.epam.restraunt.command.ajax.impl.AjaxLoginStaffCommand;
import com.epam.restraunt.command.ajax.impl.AjaxOrdersByStateCommand;
import com.epam.restraunt.command.ajax.impl.AjaxPaymentCommand;
import com.epam.restraunt.command.ajax.impl.AjaxRegisterStaff;
import com.epam.restraunt.command.ajax.impl.AjaxRemoveFromCartCommand;
import com.epam.restraunt.command.ajax.impl.AjaxRemoveMenuItem;
import com.epam.restraunt.command.ajax.impl.AjaxRemoveUser;
import com.epam.restraunt.command.ajax.impl.AjaxUpdateOrderState;
import com.epam.restraunt.command.ajax.impl.AjaxUpdateStaff;
import com.epam.restraunt.command.ajax.impl.AjaxViewOrderItems;
import com.epam.restraunt.command.ajax.impl.AjaxViewStaffHistory;

public class AjaxCommandExecutor {

	private AjaxCommandExecutor() {
	}

	private static EnumMap<AjaxCommandEnum, AjaxCommand> map = null;

	static {
		map = new EnumMap<>(AjaxCommandEnum.class);
		map.put(AjaxCommandEnum.ADD_TO_CART, new AjaxAddToCartCommand());
		map.put(AjaxCommandEnum.REMOVE_FROM_CART, new AjaxRemoveFromCartCommand());
		map.put(AjaxCommandEnum.PAY, new AjaxPaymentCommand());
		map.put(AjaxCommandEnum.DEPOSIT, new AjaxDepositCommand());
		map.put(AjaxCommandEnum.CHANGE_PASSWORD, new AjaxChangePassCommand());
		map.put(AjaxCommandEnum.VIEW_ORDER_ITEMS, new AjaxViewOrderItems());
		map.put(AjaxCommandEnum.LOGIN_STAFF, new AjaxLoginStaffCommand());
		map.put(AjaxCommandEnum.GET_ORDERS_BY_STATE, new AjaxOrdersByStateCommand());
		map.put(AjaxCommandEnum.TODAY_ORDERS, new AjaxGetTodayOrders());
		map.put(AjaxCommandEnum.YESTARDAY_ORDERS, new AjaxGetYestardayOrders());
		map.put(AjaxCommandEnum.GET_STAFF_ORDERS_HISTORY, new AjaxViewStaffHistory());
		map.put(AjaxCommandEnum.GET_ORDER_CHANGES, new AjaxGetChangesMadeByStaff());
		map.put(AjaxCommandEnum.GET_ORDER_INFO, new AjaxGetOrderInfo());
		map.put(AjaxCommandEnum.UPDATE_ORDER_STATE, new AjaxUpdateOrderState());
		map.put(AjaxCommandEnum.GET_ALL_PERSONAL, new AjaxGetAllPersonal());
		map.put(AjaxCommandEnum.GET_STAFF_BY_ID, new AjaxGetDefindStaff());
		map.put(AjaxCommandEnum.GET_STAFF_BY_NAME, new AjaxGetStaffByName());
		map.put(AjaxCommandEnum.REGISTER_STAFF, new AjaxRegisterStaff());
		map.put(AjaxCommandEnum.GET_ALL_DISHES, new AjaxGetAllDishes());
		map.put(AjaxCommandEnum.GET_ALL_DRINKS, new AjaxGetAllDrinks());
		map.put(AjaxCommandEnum.ADD_DISH, new AjaxAddDishToMenu());
		map.put(AjaxCommandEnum.ADD_DRINK, new AjaxAddDrinkToMenu());
		map.put(AjaxCommandEnum.GET_ALL_CLIENTS, new AjaxGetAllClients());
		map.put(AjaxCommandEnum.GET_CLIENT_BY_ID, new AjaxGetClientById());
		map.put(AjaxCommandEnum.GET_CLIENT_BY_LOGIN, new AjaxGetClientByLogin());
		map.put(AjaxCommandEnum.CLIENT_ORDER_HISTORY, new AjaxClientOrderHistory());
		map.put(AjaxCommandEnum.UPDATE_STAFF, new AjaxUpdateStaff());
		map.put(AjaxCommandEnum.DELETE_STAFF, new AjaxDeleteStaff());
		map.put(AjaxCommandEnum.CHANGE_DISH, new AjaxChangeDish());
		map.put(AjaxCommandEnum.CHANGE_DRINK, new AjaxChangeDrink());
		map.put(AjaxCommandEnum.REMOVE_ITEM, new AjaxRemoveMenuItem());
		map.put(AjaxCommandEnum.REMOVE_USER, new AjaxRemoveUser());
		map.put(AjaxCommandEnum.CHANGE_BALANCE, new AjaxChangeBalance());

	}

	public static String execute(HttpServletRequest request, String command)
			throws IOException {
		AjaxCommandEnum commandEnum = AjaxCommandEnum.valueOf(command);
		return map.get(commandEnum).execute(request);
	}

}
