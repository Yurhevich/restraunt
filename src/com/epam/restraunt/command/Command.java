package com.epam.restraunt.command;

import javax.servlet.http.HttpServletRequest;

public interface Command {
	
	String LOGIN_PAGE = "/common/reg/login.jsp";
    String MAIN_PAGE = "/common/main.jsp";
    String REGISTRATION_PAGE = "/common/reg/registration.jsp";
    String ERROR_PAGE = "common/error.jsp";
    String MENU_PAGE = "/common/menu.jsp";
    String ORDER_PAGE = "/user/order.jsp";
    String ACC_INFO_PAGE = "/user/accinfo.jsp";
    String VIEW_ORDER_HISTORY= "/user/orderHistory.jsp";
    String VIEW_ORDER_INFO = "/staff/orderInfo.jsp";
    String STAFF_MAIN_PAGE = "/staff/staffMain.jsp";
    String VIEW_STAFF_HISTORY = "staff/history.jsp";
    String STAFF_ACC_INFO = "/staff/staffInfo.jsp";
    String CLIENT_ACC_INFO = "/user/accinfo.jsp";
    String MENU_ITEM_INFO = "/staff/menuItemInfo.jsp";
    String ID_ITEM = "idItem";
    String CLIENT  = "client";
    String ORDER = "order";
    String STAFF = "staff";
    String ORDERS = "orders";
    String ID_STAFF = "idStaff";
    String ID_CLIENT = "idClient";
    String ITEM = "item";
   
    String execute(HttpServletRequest request);
}
