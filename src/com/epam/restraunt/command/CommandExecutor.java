package com.epam.restraunt.command;

import java.util.EnumMap;

import javax.servlet.http.HttpServletRequest;


import com.epam.restraunt.command.impl.LocalCommand;
import com.epam.restraunt.command.impl.LoginCommand;
import com.epam.restraunt.command.impl.LogoutCommand;
import com.epam.restraunt.command.impl.OrderCommand;
import com.epam.restraunt.command.impl.RegistrationCommand;
import com.epam.restraunt.command.impl.ViewClientInfo;
import com.epam.restraunt.command.impl.ViewHistoryCommand;
import com.epam.restraunt.command.impl.ViewMenuCommand;
import com.epam.restraunt.command.impl.ViewMenuItemInfo;
import com.epam.restraunt.command.impl.ViewOrderInfo;
import com.epam.restraunt.command.impl.ViewStaffInfo;
import com.epam.restraunt.command.CommandEnum;


public class CommandExecutor {
	
	 private CommandExecutor(){}

	    private static EnumMap<CommandEnum, Command> map = null;

	    static{
	        map = new EnumMap<>(CommandEnum.class);
	        map.put(CommandEnum.REGISTRATION,new RegistrationCommand());
	        map.put(CommandEnum.LOGIN,new LoginCommand());
	        map.put(CommandEnum.LOGOUT,new LogoutCommand() );
	        map.put(CommandEnum.VIEW_MENU, new ViewMenuCommand());
	        map.put(CommandEnum.ORDER, new OrderCommand());
	        map.put(CommandEnum.VIEW_HISTORY, new ViewHistoryCommand());
	        map.put(CommandEnum.LOCAL, new LocalCommand());
	        map.put(CommandEnum.VIEW_ORDER_INFO, new ViewOrderInfo());
	        map.put(CommandEnum.VIEW_STAFF_INFO, new ViewStaffInfo());
	        map.put(CommandEnum.VIEW_MENU_ITEM, new ViewMenuItemInfo());
	        map.put(CommandEnum.VIEW_CLIENT_INFO, new ViewClientInfo());
	        
	        
	    }

	    public static String execute(HttpServletRequest request, String command){
	    	CommandEnum commandEnum = CommandEnum.valueOf(command);
	        return map.get(commandEnum).execute(request);
	    }
	    
	   

}
