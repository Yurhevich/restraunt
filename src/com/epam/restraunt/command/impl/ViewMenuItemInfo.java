package com.epam.restraunt.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.Command;
import com.epam.restraunt.entity.Dish;
import com.epam.restraunt.entity.Drink;
import com.epam.restraunt.entity.MenuItem;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class ViewMenuItemInfo implements Command {
	private final static String TYPE = "type";
	private final static String DISH = "dish";
	private final static String DRINK = "drink";

	@Override
	public String execute(HttpServletRequest request) {
		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return MAIN_PAGE;
		}

		String idItem = request.getParameter(ID_ITEM);
		int id = 0;

		if (idItem == null || idItem.isEmpty()) {
			return ERROR_PAGE;
		}

		try {
			id = Integer.parseInt(idItem);
		} catch (NumberFormatException e) {
			return ERROR_PAGE;
		}

		MenuItem requiredItem = null;

		MenuService service = ServiceFactory.getMenuService();

		try {
			requiredItem = service.getMenuItem(id);
		} catch (ServiceException e) {
			return ERROR_PAGE;
		}
		
		if(requiredItem ==null){
			return STAFF_MAIN_PAGE;
		}

		if (requiredItem.getClass().equals(Dish.class)) {
			request.setAttribute(TYPE, DISH);
			request.setAttribute(ITEM, (Dish) requiredItem);
		} else {
			request.setAttribute(TYPE, DRINK);
			request.setAttribute(ITEM, (Drink) requiredItem);
		}

		return MENU_ITEM_INFO;
	}

}
