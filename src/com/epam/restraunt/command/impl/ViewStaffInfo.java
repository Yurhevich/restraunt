package com.epam.restraunt.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.Command;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.StaffService;
import com.epam.restraunt.service.exception.ServiceException;

public class ViewStaffInfo implements Command {

	@Override
	public String execute(HttpServletRequest request) {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return MAIN_PAGE;
		}

		String idStaff = request.getParameter(ID_STAFF);
		int id = 0;

		if (idStaff != null) {

			if (idStaff.isEmpty()) {
				return ERROR_PAGE;
			}

			try {
				id = Integer.parseInt(idStaff);
			} catch (NumberFormatException e) {
				return ERROR_PAGE;
			}

		} else {
			id = staff.getId();
		}

		Staff requiredStaff = null;

		StaffService service = ServiceFactory.getStaffService();

		try {
			requiredStaff = service.fetchById(id);
		} catch (ServiceException e) {
			return ERROR_PAGE;
		}
		
		if(requiredStaff==null){
			return STAFF_MAIN_PAGE;
		}
		request.setAttribute(STAFF, requiredStaff);

		return STAFF_ACC_INFO;
	}

}
