package com.epam.restraunt.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.Command;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.entity.Order;
import com.epam.restraunt.service.OrderService;
import com.epam.restraunt.service.ServiceFactory;

public class ViewHistoryCommand implements Command {

	private final static int ORDERS_PER_PAGE = 8;
	private final static String PAGE = "page";
	private final static String ORDERS = "orders";
	private final static String MIN_PAGE = "min_page";
	private final static String MAX_PAGE = "max_page";
	private final static String LEFT_SWITCH = "left_switch";
	private final static String RIGHT_SWITCH = "right_switch";
	private static final String CUR_PAGE = "cur_page";

	@Override
	public String execute(HttpServletRequest request) {

		String forwardPage = VIEW_ORDER_HISTORY;

		Client client = (Client) request.getSession().getAttribute(CLIENT);

		if (client == null) {
			return LOGIN_PAGE;
		}

		int page = 1;

		String requestedPage = request.getParameter(PAGE);

		if (requestedPage != null) {
			try {
				page = Integer.parseInt(requestedPage);
			} catch (NumberFormatException e) {
				return ERROR_PAGE;
			}
		}

		OrderService service = ServiceFactory.getOrderService();
		List<Order> orders = null;
		int countOfRecords = 0;
		int numberOfPages = 0;
		int id = client.getId();

		try {

			orders = service.getOrdersByClientId(id, (page - 1) * ORDERS_PER_PAGE, ORDERS_PER_PAGE);

			countOfRecords = service.getCountOfClientOrders(id);
			numberOfPages = (int) Math.ceil(countOfRecords * 1.0 / ORDERS_PER_PAGE);

			request.setAttribute(ORDERS, orders);

			int maxPage = 0;
			int minPage;

			int count = 0;

			if ((float) page % 3 == 0.0) {
				maxPage = page;
				minPage = page - 2;
			} else {
				count = page;
				count = page + (3 - page % 3);
				minPage = page / 3 * 3 + 1;
				maxPage = count;
			}

			if (numberOfPages < maxPage) {
				maxPage = numberOfPages;
			}

			
			
			request.setAttribute(MAX_PAGE, maxPage);
			request.setAttribute(MIN_PAGE, minPage);
			request.setAttribute(CUR_PAGE, page);

			if (maxPage < numberOfPages) {
				request.setAttribute(RIGHT_SWITCH, maxPage + 1);
			}

			if (minPage != 1) {
				request.setAttribute(LEFT_SWITCH, minPage - 1);
			}

		} catch (Exception e) {
			return ERROR_PAGE;
		}

		return forwardPage;

	}

}
