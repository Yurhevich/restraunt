package com.epam.restraunt.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.restraunt.command.Command;
import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.command.builder.BuilderFactory;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.service.ClientService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;
import com.epam.restraunt.validate.Validator;

public class LoginCommand implements Command {
	private final static String CLIENT = "client";

	@Override
	public String execute(HttpServletRequest request) {
		String page = MAIN_PAGE;
		HttpSession session = request.getSession();

		Builder<Client> builder = BuilderFactory.getClientBuilder();
		ClientService userService = ServiceFactory.getClientService();
		Client client = builder.create(request);
		Validator validator = Validator.getInstance();

		if (validator.validateLoginParameters(request)) {
			try {
				if (userService.login(client)) {
					session.setAttribute(CLIENT, client);
				} else {
					request.setAttribute("message", "Проверьте правильность логина и пароля");
					page = LOGIN_PAGE;
				}
			} catch (ServiceException e) {
				return ERROR_PAGE;
			}

		} else {
			request.setAttribute("message", "Проверьте правильность логина и пароля");
			page = LOGIN_PAGE;
		}

		return page;

	}

}
