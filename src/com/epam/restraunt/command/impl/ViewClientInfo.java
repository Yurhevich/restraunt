package com.epam.restraunt.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.Command;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.ClientService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class ViewClientInfo implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null || !staff.isAdmin()) {
			return MAIN_PAGE;
		}

		String idClient = request.getParameter(ID_CLIENT);
		int id = 0;

		if (idClient != null) {

			if (idClient.isEmpty()) {
				return ERROR_PAGE;
			}

			try {
				id = Integer.parseInt(idClient);
			} catch (NumberFormatException e) {
				return ERROR_PAGE;
			}

		}

		Client requiredClient = null;

		ClientService service = ServiceFactory.getClientService();

		try {
			requiredClient = service.fetchById(id);
		} catch (ServiceException e) {
			return ERROR_PAGE;
		}
		
		if(requiredClient==null){
			return STAFF_MAIN_PAGE;
		}

		request.setAttribute(CLIENT, requiredClient);

		return CLIENT_ACC_INFO;
	}

}
