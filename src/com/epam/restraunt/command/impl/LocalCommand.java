package com.epam.restraunt.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.Command;

public class LocalCommand implements Command{

	private static final String LOCAL = "locale";
	
	@Override
	public String execute(HttpServletRequest request) {
		request.getSession(true).setAttribute(LOCAL, request.getParameter(LOCAL) );
		return MAIN_PAGE;
	}

}
