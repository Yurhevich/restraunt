package com.epam.restraunt.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.Command;


public class LogoutCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		 request.getSession().invalidate();
	        return MAIN_PAGE;
	}

}
