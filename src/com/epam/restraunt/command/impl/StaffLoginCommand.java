package com.epam.restraunt.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.restraunt.command.Command;
import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.command.builder.BuilderFactory;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.StaffService;
import com.epam.restraunt.service.exception.ServiceException;
import com.epam.restraunt.validate.Validator;

public class StaffLoginCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		String page = MAIN_PAGE;
		HttpSession session = request.getSession();

		Builder<Staff> builder = BuilderFactory.getStaffBuilder();
		StaffService staffService = ServiceFactory.getStaffService();
		Staff staff = builder.create(request);
		Validator validator = Validator.getInstance();

		if (validator.validateLoginParameters(request)) {
			try {
				if (staffService.login(staff)) {
					session.setAttribute(STAFF, staff);
				} else {
					page = STAFF_MAIN_PAGE;
				}
			} catch (ServiceException e) {
				return ERROR_PAGE;
			}

		}

		return page;
	}

}
