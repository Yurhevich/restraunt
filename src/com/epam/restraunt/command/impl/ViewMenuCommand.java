package com.epam.restraunt.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.Command;
import com.epam.restraunt.entity.Dish;
import com.epam.restraunt.entity.Drink;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class ViewMenuCommand implements Command {

	private final static String DRINK = "drink";
	private final static String DISHES = "dishes";

	@Override
	public String execute(HttpServletRequest request) {

		String page = MENU_PAGE;

		MenuService service = ServiceFactory.getMenuService();

		List<Dish> dishes = null;
		List<Drink> drink = null;

		try {

			dishes = service.getAllDishes();
			drink = service.getAllDrinks();

		} catch (ServiceException e) {
			return ERROR_PAGE;
		}

		request.setAttribute(DISHES, dishes);
		request.setAttribute(DRINK, drink);

		return page;
	}

}
