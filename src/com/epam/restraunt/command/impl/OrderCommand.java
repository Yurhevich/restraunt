package com.epam.restraunt.command.impl;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.Command;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.entity.MenuItem;
import com.epam.restraunt.entity.Order;

public class OrderCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {

		Order order = (Order) request.getSession().getAttribute(ORDER);
		Client client = (Client) request.getSession().getAttribute(CLIENT);

		if (client == null) {
			return LOGIN_PAGE;
		}

		if (order == null) {
			order = new Order();
			Map<MenuItem, Integer> items = new HashMap<>();
			order.setItems(items);
			order.setTotalCost(0);
			request.getSession().setAttribute(ORDER, order);
		}

		order.setIdClient(client.getId());

		return ORDER_PAGE;
	}

}
