package com.epam.restraunt.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.Command;
import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.command.builder.BuilderFactory;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.service.ClientService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;
import com.epam.restraunt.validate.Validator;

public class RegistrationCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {

		String page = MAIN_PAGE;

		ClientService service = ServiceFactory.getClientService();

		Builder<Client> builder = BuilderFactory.getClientBuilder();

		Validator validator = Validator.getInstance();

		if (validator.validateUserRegistrationParameters(request)) {

			Client client = builder.create(request);

			try {
				if (service.register(client)) {
					request.getSession().setAttribute("client", client);
				} else {
					page = REGISTRATION_PAGE;
					request.setAttribute("message", " Аккаунт с такой почтой или логином уже существует ...");
				}
			} catch (ServiceException e) {
				page = ERROR_PAGE;
			}

			return page;
		} else {
			request.setAttribute("message", " Проверьте правильность введенных данных ...");
			return REGISTRATION_PAGE;
		}
	}
}