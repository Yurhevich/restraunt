package com.epam.restraunt.command.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.Command;
import com.epam.restraunt.entity.MenuItem;
import com.epam.restraunt.entity.Order;
import com.epam.restraunt.entity.OrderStateChange;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.OrderService;
import com.epam.restraunt.service.ServiceFactory;
import com.epam.restraunt.service.exception.ServiceException;

public class ViewOrderInfo implements Command {

	private static final String ORDER_ID = "idOrder";
	private static final String ITEMS = "items";
	private static final String CHANGES = "changes";

	@Override
	public String execute(HttpServletRequest request) {

		Staff staff = (Staff) request.getSession().getAttribute(STAFF);

		if (staff == null) {
			return MAIN_PAGE;
		}

		String idOrder = request.getParameter(ORDER_ID);
		int id;

		if (idOrder == null) {
			return ERROR_PAGE;
		}

		try {
			id = Integer.parseInt(idOrder);
		} catch (NumberFormatException e) {
			return ERROR_PAGE;
		}

		Order order = null;
		Map<MenuItem, Integer> items = null;
		List<OrderStateChange> changes = null;

		OrderService orderService = ServiceFactory.getOrderService();
		MenuService menuService = ServiceFactory.getMenuService();

		try {
			order = orderService.getOrderById(id);
			items = menuService.getItemsByOrderId(id);
			changes = orderService.getOrderStateChanges(id);

		} catch (ServiceException e) {
			return ERROR_PAGE;
		}

		request.setAttribute(ORDER, order);
		request.setAttribute(ITEMS, items);
		request.setAttribute(CHANGES, changes);

		return VIEW_ORDER_INFO;

	}

}
