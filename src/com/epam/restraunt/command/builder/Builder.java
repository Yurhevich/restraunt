package com.epam.restraunt.command.builder;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.entity.Entity;

public interface Builder<T extends Entity> {

    String NAME = "name";
    String SURNAME = "surname";
	String EMAIL = "email";
	String LOGIN = "login";
	String BALANCE = "balance";
	String DISCOUNT = "discount";
	String PASSWORD = "password";
	String ADDRESS = "address";
	String TELEPHONE = "telephone";
	String POST = "post";
	String PHOTO = "photo";
	String IS_ADMIN = "isAdmin";
	String PRICE = "price";
	String TIME_TO_COOK = "timeToCook";
	String 	CONSISTS = "consists";
	String VOLUME = "volume";
	String IS_ALCOHOLIC ="isAlcoholic";
	
	T create(HttpServletRequest data);

}
