package com.epam.restraunt.command.builder.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.entity.Dish;

public class DishBuilder implements Builder<Dish> {

	private final static Builder<Dish> builder = new DishBuilder();

	public static Builder<Dish> getInstance() {
		return builder;
	}

	@Override
	public Dish create(HttpServletRequest request) {

		Dish dish = new Dish();

		dish.setName(request.getParameter(NAME));

		dish.setPrice(Integer.parseInt(request.getParameter(PRICE)));

		dish.setTimeToCook(Integer.parseInt(request.getParameter(TIME_TO_COOK)));

		dish.setConsists(request.getParameter(CONSISTS));

		return dish;
	}

}
