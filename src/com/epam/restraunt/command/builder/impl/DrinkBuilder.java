package com.epam.restraunt.command.builder.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.entity.Drink;

public class DrinkBuilder implements Builder<Drink> {

	private final static Builder<Drink> builder = new DrinkBuilder();

	public static Builder<Drink> getInstance() {
		return builder;
	}

	@Override
	public Drink create(HttpServletRequest request) {
		
		Drink drink = new Drink();
		
		drink.setName(request.getParameter(NAME));
		
		drink.setPrice(Integer.parseInt(request.getParameter(PRICE)));
		
		drink.setVolume(Integer.parseInt(request.getParameter(VOLUME)));
		
		drink.setAlcoholic(Boolean.parseBoolean(request.getParameter(IS_ALCOHOLIC)));

		return drink;
	}

}
