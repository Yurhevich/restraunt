package com.epam.restraunt.command.builder.impl;

import javax.servlet.annotation.MultipartConfig;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.entity.Staff;
@MultipartConfig
public class StaffBuilder implements Builder<Staff> {

	private final static Builder<Staff> builder = new StaffBuilder();

	public static Builder<Staff> getInstance() {
		return builder;
	}

	@Override
	public Staff create(HttpServletRequest request) {
		
		Staff staff = new Staff();
		
		staff.setLogin(request.getParameter(LOGIN));
		
		staff.setPassword(request.getParameter(PASSWORD));

		String name = request.getParameter(NAME);

		if (name == null) {
			return staff;
		}

		staff.setName(name);
		
		staff.setSurname(request.getParameter(SURNAME));
		
		staff.setTelephone(request.getParameter(TELEPHONE));
		
		staff.setPost(request.getParameter(POST));
		
		staff.setEmail(request.getParameter(EMAIL));
		
		staff.setAddress(request.getParameter(ADDRESS));
		
		staff.setAdmin(Boolean.parseBoolean(request.getParameter(IS_ADMIN)));
		
		return staff;
	}

}
