package com.epam.restraunt.command.builder.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.entity.Delivery;

public class DeliveryBuilder implements Builder<Delivery> {

	private final static String ADDRESS = "address";
	private final static String TELEPHONE = "telephone";

	private final static Builder<Delivery> builder = new DeliveryBuilder();

	public static Builder<Delivery> getInstance() {
		return builder;
	}

	@Override
	public Delivery create(HttpServletRequest request) {

		Delivery delivery = new Delivery();

		delivery.setAddress(request.getParameter(ADDRESS));

		delivery.setTelephone(request.getParameter(TELEPHONE));

		return delivery;
	}

}
