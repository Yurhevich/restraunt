package com.epam.restraunt.command.builder.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.restraunt.command.builder.Builder;
import com.epam.restraunt.entity.Client;

public final class ClientBuilder implements Builder<Client> {

	private static final Builder<Client> builder = new ClientBuilder();

	public static Builder<Client> getInstance() {
		return builder;
	}

	@Override
	public Client create(HttpServletRequest request) {

		Client client = new Client();

		client.setLogin(request.getParameter(LOGIN));

		client.setPassword(request.getParameter(PASSWORD));

		String email = request.getParameter(EMAIL);

		if (email == null) {
			return client;
		}

		client.setEmail(email);

		client.setName(request.getParameter(NAME));

		client.setSurname(request.getParameter(SURNAME));

		return client;
	}

}
