package com.epam.restraunt.command.builder;


import com.epam.restraunt.command.builder.impl.ClientBuilder;
import com.epam.restraunt.command.builder.impl.DeliveryBuilder;
import com.epam.restraunt.command.builder.impl.DishBuilder;
import com.epam.restraunt.command.builder.impl.DrinkBuilder;
import com.epam.restraunt.command.builder.impl.StaffBuilder;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.entity.Delivery;
import com.epam.restraunt.entity.Dish;
import com.epam.restraunt.entity.Drink;
import com.epam.restraunt.entity.Staff;

public final class BuilderFactory {

	private BuilderFactory() {
	}

	public static Builder<Client> getClientBuilder() {
		return ClientBuilder.getInstance();
	}

	public static Builder<Delivery> getDeliveryBuilder() {
		return DeliveryBuilder.getInstance();
	}
	
	public static Builder<Staff> getStaffBuilder(){
		return StaffBuilder.getInstance();
	}

	public static Builder<Dish> getDishBuilder() {
		return DishBuilder.getInstance();
	}
	
	public static Builder<Drink> getDrinkBuilder() {
		return DrinkBuilder.getInstance();
	}
	


}
