package com.epam.restraunt.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.epam.restraunt.dao.OrderDAO;
import com.epam.restraunt.dao.exception.DAOException;
import com.epam.restraunt.dao.pool.ConnectionPool;
import com.epam.restraunt.dao.pool.exeption.ConnectionPoolException;
import com.epam.restraunt.entity.Delivery;
import com.epam.restraunt.entity.MenuItem;
import com.epam.restraunt.entity.Order;
import com.epam.restraunt.entity.OrderStateChange;
import com.epam.restraunt.entity.Staff;

public class OrderDAOImpl implements OrderDAO {

	private final static Logger LOGGER = Logger.getLogger(OrderDAOImpl.class);

	private final static String INSERT_ORDER = "INSERT INTO clientOrder VALUES(null, ?, ?, ?, 'awaiting', ?)";
	private final static String INSERT_ORDER_ITEM = "INSERT INTO Order_has_MenuItem VALUES(LAST_INSERT_ID(),?, ?)";
	private final static String GET_ORDER_BY_ID = "SELECT idOrder, idClient, totalCost, time, currentState, inPlace FROM clientOrder WHERE idOrder=?";
	private final static String GET_ORDERS_BY_CLIENT_ID = "SELECT idOrder, idClient, totalCost, time, currentState, inPlace FROM clientOrder WHERE idClient = ? ORDER BY time DESC";
	private final static String GET_ORDERS_BY_STAFF_ID = "SELECT DISTINCT o.idOrder, o.idClient, o.totalCost, o.time, o.currentState, o.inPlace FROM ClientOrder o INNER JOIN OrderStateChange s ON s.idOrder = o.idOrder WHERE s.idStaff = ? ORDER BY time DESC LIMIT ?, ?";
	private final static String UPDATE_ORDER_STATE = "UPDATE clientOrder SET currentState = ? WHERE idOrder = ?";
	private final static String GET_DELIVERY_BY_ID = "SELECT idOrder, telephone, address FROM Delivery WHERE idOrder = ?";
	private final static String INSERT_DELIVERY = "INSERT INTO Delivery VALUES(LAST_INSERT_ID(),?, ?)";
	private final static String PAYMENT = "UPDATE client SET balance = balance - ? WHERE idClient = ?";
	private final static String GET_ORDERS_BY_STATE = "SELECT idOrder, idClient, totalCost, time, currentState, inPlace FROM clientOrder WHERE currentState = ? ORDER BY time DESC LIMIT ?, ?";
	private final static String GET_ORDERS_BY_STATE_SINCE = "SELECT idOrder, idClient, totalCost, time, currentState, inPlace FROM clientOrder WHERE currentState = ? AND time > ? ORDER BY time DESC";
	private final static String GET_CURRENT_ORDERS = "SELECT idOrder, idClient, totalCost, time, currentState, inPlace FROM clientOrder WHERE currentState = 'awaiting' ORDER BY time DESC";
	private final static String INSERT_ORDER_CHANGE = "INSERT INTO OrderStateChange VALUES(null, ?, ?, ?, ?)";
	private final static String CHECK_ORDER_STATE = "SELECT currentState FROM clientOrder WHERE idOrder = ?";
	private final static String GET_STAFF_CHANGES = "SELECT idOrderStateChange, idOrder, idStaff, establishedState, timeOfChange FROM OrderStateChange WHERE idStaff = ? ORDER BY timeOfChange DESC";
	private final static String GET_STAFF_CHANGES_SINCE = "SELECT idOrderStateChange, idOrder, idStaff, establishedState, timeOfChange FROM OrderStateChange WHERE idStaff = ? AND timeOfChange > ? ORDER BY timeOfChange DESC";
	private static final String GET_TODAY_ORDERS = "SELECT idOrder, idClient, totalCost, time, currentState, inPlace FROM clientOrder WHERE time >= CURDATE() ORDER BY time DESC LIMIT ?, ?";
	private static final String GET_YESTARDAY_ORDERS = "SELECT idOrder, idClient, totalCost, time, currentState, inPlace FROM clientOrder WHERE time >= DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND time < CURDATE() ORDER BY time DESC LIMIT ?, ?";
	private static final String GET_LAST_WEEK_ORDERS = "SELECT idOrder, idClient, totalCost, time, currentState, inPlace FROM clientOrder WHERE time >= DATE_SUB(CURDATE(), INTERVAL DAYOFWEEK(CURDATE())-1 DAY) ORDER BY time DESC";
	private final static String GET_ORDER_STATE_CHANGES = "SELECT o.idOrderStateChange,  o.idOrder, o.idStaff, o.establishedState, o.timeOfChange , s.name, s.surname, s.post FROM OrderStateChange o INNER JOIN Staff s ON o.idStaff = s.idStaff WHERE o.idOrder = ? ORDER BY o.timeOfChange DESC";
	private final static String GET_ORDERS_COUNT_CERTAIN_STATE = "SELECT COUNT(*) AS CountOfRecords FROM ClientOrder WHERE currentState=?";
	private static final String COUNT_ORDERS_SERVED_BY_STAFF = "SELECT COUNT(DISTINCT s.idOrder) AS CountOfRecords  FROM ClientOrder o INNER JOIN OrderStateChange s ON s.idOrder = o.idOrder WHERE s.idStaff = ? ORDER BY time DESC ";
	private static final String COUNT_YESTARDAY_ORDERS = "SELECT COUNT(*) AS CountOfRecords FROM ClientOrder WHERE time >= DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND time < CURDATE()";
	private static final String COUNT_TODAY_ORDERS = "SELECT COUNT(*) AS CountOfRecords FROM ClientOrder WHERE time >= CURDATE() ";
	private static final String COUNT_CHANGES_MADE_BY_STAFF = "SELECT COUNT(*) AS CountOfRecords FROM OrderStateChange WHERE idStaff = ? ";
	private static final String CHANGES_MADE_BY_STAFF = "SELECT idOrderStateChange, idOrder, idStaff, establishedState, timeOfChange FROM OrderStateChange WHERE idStaff = ? ORDER BY timeOfChange DESC LIMIT ?, ?";
	private static final String ALL_ORDERS_BY_CLIENT_ID = "SELECT idOrder, idClient, totalCost, time, currentState, inPlace FROM clientOrder WHERE idClient = ? ORDER BY time DESC LIMIT ?, ?";
	private static final String COUNT_CLIENT_ORDERS = "SELECT COUNT(*) AS CountOfRecords FROM ClientOrder WHERE idClient = ?";

	private static OrderDAO instance = new OrderDAOImpl();

	private OrderDAOImpl() {
	}

	public static OrderDAO getInstance() {
		return instance;
	}

	@Override
	public void addOrder(Order order) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;

		try {

			connection = ConnectionPool.getInstance().takeConnection();

			connection.setAutoCommit(false);

			statement = connection.prepareStatement(INSERT_ORDER);

			statement.setInt(1, order.getIdClient());
			statement.setInt(2, order.getTotalCost());
			statement.setTimestamp(3, new java.sql.Timestamp(new Date().getTime()));

			statement.setBoolean(4, order.isInPlace());

			statement.executeUpdate();

			statement.close();

			for (Map.Entry<MenuItem, Integer> map : order.getItems().entrySet()) {

				statement = connection.prepareStatement(INSERT_ORDER_ITEM);

				statement.setInt(1, map.getKey().getId());
				statement.setInt(2, map.getValue());

				statement.executeUpdate();
				statement.close();

			}

			statement = connection.prepareStatement(PAYMENT);
			statement.setInt(1, order.getTotalCost());
			statement.setInt(2, order.getIdClient());

			statement.executeUpdate();

			connection.commit();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);

		} catch (SQLException ex) {
			LOGGER.error("Inserting new order failed", ex);
			try {
				connection.rollback();
			} catch (SQLException e) {
				LOGGER.error("Rolling back transaction failed", ex);
			}
			throw new DAOException(ex);

		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Enabling autocommit failed", e);
			}
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public void addOrderWithDelivery(Order order, Delivery delivery) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;

		try {

			connection = ConnectionPool.getInstance().takeConnection();

			connection.setAutoCommit(false);

			statement = connection.prepareStatement(INSERT_ORDER);

			statement.setInt(1, order.getIdClient());
			statement.setInt(2, order.getTotalCost());
			statement.setTimestamp(3, new java.sql.Timestamp(new Date().getTime()));
			statement.setBoolean(4, order.isInPlace());

			statement.executeUpdate();

			statement.close();

			statement = connection.prepareStatement(INSERT_DELIVERY);
			statement.setString(1, delivery.getTelephone());
			statement.setString(2, delivery.getAddress());

			statement.executeUpdate();

			statement.close();

			for (Map.Entry<MenuItem, Integer> map : order.getItems().entrySet()) {

				statement = connection.prepareStatement(INSERT_ORDER_ITEM);

				statement.setInt(1, map.getKey().getId());
				statement.setInt(2, map.getValue());

				statement.executeUpdate();
				statement.close();

			}

			statement = connection.prepareStatement(PAYMENT);
			statement.setInt(1, order.getTotalCost());
			statement.setInt(2, order.getIdClient());

			statement.executeUpdate();

			connection.commit();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);

		} catch (SQLException ex) {
			LOGGER.error("Inserting new order with delivery failed", ex);
			try {
				connection.rollback();
			} catch (SQLException e) {
				LOGGER.error("Rolling back transaction failed", ex);
			}
			throw new DAOException(ex);

		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Enabling autocommit failed", e);
			}
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public Order getOrderById(int id) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		Order order = new Order();

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_ORDER_BY_ID);

			statement.setInt(1, id);

			rs = statement.executeQuery();

			if (rs.next()) {
				order.setId(rs.getInt(ID_ORDER));
				order.setIdClient(rs.getInt(ID_CLIENT));
				order.setTotalCost(rs.getInt(TOTAL_COST));
				order.setTime(new Date(rs.getTimestamp(TIME).getTime()));
				order.setStatus(rs.getString(CURRENT_STATE));
				order.setInPlace(rs.getBoolean(IN_PLACE));
				
			}

			return order;
		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Fetching order by id failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<Order> getOrdersByClientId(int idClient) throws DAOException {

		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Order> list = new ArrayList<>();

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_ORDERS_BY_CLIENT_ID);

			statement.setInt(1, idClient);

			rs = statement.executeQuery();

			while (rs.next()) {
				Order order = new Order();
				order.setId(rs.getInt(ID_ORDER));
				order.setIdClient(rs.getInt(ID_CLIENT));
				order.setTotalCost(rs.getInt(TOTAL_COST));
				order.setTime(new Date(rs.getTimestamp(TIME).getTime()));
				order.setStatus(rs.getString(CURRENT_STATE));
				order.setInPlace(rs.getBoolean(IN_PLACE));
				list.add(order);
			}

			return list;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting client history orders failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public void updateOrderState(OrderStateChange change) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			connection.setAutoCommit(false);

			statement = connection.prepareStatement(UPDATE_ORDER_STATE);

			statement.setString(1, change.getEstablishedState());
			System.out.println(change.getEstablishedState() + " " + change.getIdOrder());
			statement.setInt(2, change.getIdOrder());

			statement.executeUpdate();

			statement.close();

			statement = connection.prepareStatement(INSERT_ORDER_CHANGE);

			statement.setInt(1, change.getIdOrder());
			statement.setInt(2, change.getStaff().getId());
			statement.setString(3, change.getEstablishedState());
			statement.setTimestamp(4, new java.sql.Timestamp(change.getTimeOfChange().getTime()));

			statement.executeUpdate();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);

		} catch (SQLException ex) {
			LOGGER.error("Updating order state failed", ex);
			try {
				connection.rollback();
			} catch (SQLException e) {
				LOGGER.error("Rolling back transaction failed", ex);
			}
			throw new DAOException(ex);

		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Enabling autocommit failed", e);
			}
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public Delivery getDeliveryById(int id) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		Delivery delivery = new Delivery();

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_DELIVERY_BY_ID);

			statement.setInt(1, id);

			rs = statement.executeQuery();

			if (rs.next()) {
				delivery.setId(rs.getInt(ID_ORDER));
				delivery.setAddress(rs.getString(ADDRESS));
				delivery.setTelephone(rs.getString(TELEPHONE));
				return delivery;
			}

			throw new DAOException("Not existing order id");

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Fetching delivery by id failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<Order> getOrdersByState(String state, int offset, int count) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Order> orders = new ArrayList<>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_ORDERS_BY_STATE);

			statement.setString(1, state);
			statement.setInt(2, offset);
			statement.setInt(3, count);

			rs = statement.executeQuery();

			while (rs.next()) {

				Order order = new Order();
				order.setId(rs.getInt(ID_ORDER));
				order.setIdClient(rs.getInt(ID_CLIENT));
				order.setTotalCost(rs.getInt(TOTAL_COST));
				order.setTime(new Date(rs.getTimestamp(TIME).getTime()));
				order.setStatus(rs.getString(CURRENT_STATE));
				order.setInPlace(rs.getBoolean(IN_PLACE));
				orders.add(order);
			}

			return orders;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Trying to get orders by state failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<Order> getOrdersByStateSince(String state, Date date) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Order> orders = new ArrayList<>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_ORDERS_BY_STATE_SINCE);

			statement.setString(1, state);
			statement.setDate(2, new java.sql.Date(date.getTime()));

			rs = statement.executeQuery();

			while (rs.next()) {
				Order order = new Order();
				order.setId(rs.getInt(ID_ORDER));
				order.setIdClient(rs.getInt(ID_CLIENT));
				order.setTotalCost(rs.getInt(TOTAL_COST));
				order.setTime(new Date(rs.getTimestamp(TIME).getTime()));
				order.setStatus(rs.getString(CURRENT_STATE));
				order.setInPlace(rs.getBoolean(IN_PLACE));
				orders.add(order);
			}

			return orders;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Trying to get orders by state since failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<Order> getAwaitingOrders() throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Order> orders = new ArrayList<>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_CURRENT_ORDERS);

			rs = statement.executeQuery();

			while (rs.next()) {
				Order order = new Order();
				order.setId(rs.getInt(ID_ORDER));
				order.setIdClient(rs.getInt(ID_CLIENT));
				order.setTotalCost(rs.getInt(TOTAL_COST));
				order.setTime(new Date(rs.getTimestamp(TIME).getTime()));
				order.setStatus(rs.getString(CURRENT_STATE));
				order.setInPlace(rs.getBoolean(IN_PLACE));
				orders.add(order);
			}

			return orders;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Trying to get current orders failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<OrderStateChange> getCertainStaffHistory(int idStaff) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<OrderStateChange> changes = new ArrayList<>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_STAFF_CHANGES);
			statement.setInt(1, idStaff);
			rs = statement.executeQuery();

			while (rs.next()) {
				OrderStateChange change = new OrderStateChange();
				change.setId(rs.getInt(ID_CHANGE));
				change.setIdOrder(rs.getInt(ID_ORDER));
				change.setTimeOfChange(new Date(rs.getTimestamp(TIME_OF_CHANGE).getTime()));
				change.setEstablishedState(ESTABLISHED_STATE);
				changes.add(change);
			}

			return changes;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Trying to get certain staff state changes failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<OrderStateChange> getCertainStaffHistorySince(int idStaff, Date time) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<OrderStateChange> changes = new ArrayList<>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_STAFF_CHANGES_SINCE);
			statement.setInt(1, idStaff);
			statement.setDate(2, new java.sql.Date(time.getTime()));
			rs = statement.executeQuery();

			while (rs.next()) {
				OrderStateChange change = new OrderStateChange();
				change.setId(rs.getInt(ID_CHANGE));
				change.setIdOrder(rs.getInt(ID_ORDER));
				change.setTimeOfChange(new Date(rs.getTimestamp(TIME_OF_CHANGE).getTime()));
				change.setEstablishedState(ESTABLISHED_STATE);
				changes.add(change);
			}

			return changes;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Trying to get certain staff order state changes since failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public String getCurrentStateOfOrder(int idOrder) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(CHECK_ORDER_STATE);

			statement.setInt(1, idOrder);
			rs = statement.executeQuery();

			if (rs.next()) {
				return rs.getString(CURRENT_STATE);
			}

			throw new DAOException("Not existing order id");

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting current order state by id failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<Order> getTodayOrders(int offset, int count) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Order> orders = new ArrayList<>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_TODAY_ORDERS);
			statement.setInt(1, offset);
			statement.setInt(2, count);

			rs = statement.executeQuery();

			while (rs.next()) {
				Order order = new Order();
				order.setId(rs.getInt(ID_ORDER));
				order.setIdClient(rs.getInt(ID_CLIENT));
				order.setTotalCost(rs.getInt(TOTAL_COST));
				order.setTime(new Date(rs.getTimestamp(TIME).getTime()));
				order.setStatus(rs.getString(CURRENT_STATE));
				order.setInPlace(rs.getBoolean(IN_PLACE));
				orders.add(order);
			}

			return orders;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Trying to get today orders failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<Order> getYestardayOrders(int offset, int count) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Order> orders = new ArrayList<>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_YESTARDAY_ORDERS);
			statement.setInt(1, offset);
			statement.setInt(2, count);

			rs = statement.executeQuery();

			while (rs.next()) {
				Order order = new Order();
				order.setId(rs.getInt(ID_ORDER));
				order.setIdClient(rs.getInt(ID_CLIENT));
				order.setTotalCost(rs.getInt(TOTAL_COST));
				order.setTime(new Date(rs.getTimestamp(TIME).getTime()));
				order.setStatus(rs.getString(CURRENT_STATE));
				order.setInPlace(rs.getBoolean(IN_PLACE));
				orders.add(order);
			}

			return orders;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Trying to get yestarday orders failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<Order> getLastWeekOrders() throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Order> orders = new ArrayList<>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_LAST_WEEK_ORDERS);

			rs = statement.executeQuery();

			while (rs.next()) {
				Order order = new Order();
				order.setId(rs.getInt(ID_ORDER));
				order.setIdClient(rs.getInt(ID_CLIENT));
				order.setTotalCost(rs.getInt(TOTAL_COST));
				order.setTime(new Date(rs.getTimestamp(TIME).getTime()));
				order.setStatus(rs.getString(CURRENT_STATE));
				order.setInPlace(rs.getBoolean(IN_PLACE));
				orders.add(order);
			}

			return orders;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Trying to get last week orders failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<OrderStateChange> getOrderStateChanges(int idOrder) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<OrderStateChange> changes = new ArrayList<>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_ORDER_STATE_CHANGES);
			statement.setInt(1, idOrder);
			rs = statement.executeQuery();

			while (rs.next()) {
				OrderStateChange change = new OrderStateChange();
				change.setId(rs.getInt(ID_CHANGE));
				change.setIdOrder(rs.getInt(ID_ORDER));
				change.setEstablishedState(rs.getString(ESTABLISHED_STATE));
				change.setTimeOfChange(new Date(rs.getTimestamp(TIME_OF_CHANGE).getTime()));
				Staff staff = new Staff();
				staff.setName(rs.getString(NAME));
				staff.setSurname(rs.getString(SURNAME));
				staff.setPost(rs.getString(POST));
				staff.setId(rs.getInt(ID_STAFF));
				change.setStaff(staff);
				changes.add(change);
			}

			return changes;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Trying to get order state changes  failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<Order> getOrdersByStaffId(int idStaff, int offset, int count) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Order> list = new ArrayList<>();

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_ORDERS_BY_STAFF_ID);

			statement.setInt(1, idStaff);
			statement.setInt(2, offset);
			statement.setInt(3, count);

			rs = statement.executeQuery();

			while (rs.next()) {
				Order order = new Order();
				order.setId(rs.getInt(ID_ORDER));
				order.setIdClient(rs.getInt(ID_CLIENT));
				order.setTotalCost(rs.getInt(TOTAL_COST));
				order.setTime(new Date(rs.getTimestamp(TIME).getTime()));
				order.setStatus(rs.getString(CURRENT_STATE));
				order.setInPlace(rs.getBoolean(IN_PLACE));
				list.add(order);
			}

			return list;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting staff order history failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public int getCountOfOrders(String state) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		int count = 0;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_ORDERS_COUNT_CERTAIN_STATE);
			statement.setString(1, state);

			rs = statement.executeQuery();

			if (rs.next()) {

				count = rs.getInt(COUNT_OF_RECORDS);

			}

			return count;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting count of orders failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public int getCountOfServedOrders(int idStaff) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		int count = 0;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(COUNT_ORDERS_SERVED_BY_STAFF);
			statement.setInt(1, idStaff);

			rs = statement.executeQuery();

			if (rs.next()) {

				count = rs.getInt(COUNT_OF_RECORDS);

			}

			return count;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting count of orders failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public int getCountOfYestardayOrders() throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		int count = 0;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(COUNT_YESTARDAY_ORDERS);

			rs = statement.executeQuery();

			if (rs.next()) {

				count = rs.getInt(COUNT_OF_RECORDS);

			}

			return count;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting count of orders failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public int getCountOfTodayOrders() throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		int count = 0;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(COUNT_TODAY_ORDERS);

			rs = statement.executeQuery();

			if (rs.next()) {

				count = rs.getInt(COUNT_OF_RECORDS);

			}

			return count;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting count of orders failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<OrderStateChange> getChangesMadeByStaff(int idStaff, int offset, int count) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<OrderStateChange> changes = new ArrayList<>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(CHANGES_MADE_BY_STAFF);
			statement.setInt(1, idStaff);
			statement.setInt(2, offset);
			statement.setInt(3, count);

			rs = statement.executeQuery();

			while (rs.next()) {
				OrderStateChange change = new OrderStateChange();
				change.setId(rs.getInt(ID_CHANGE));
				change.setIdOrder(rs.getInt(ID_ORDER));
				change.setTimeOfChange(new Date(rs.getTimestamp(TIME_OF_CHANGE).getTime()));
				change.setEstablishedState(rs.getString(ESTABLISHED_STATE));
				changes.add(change);
			}

			return changes;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Trying to get changes made by staff failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public int getCountOfChangesMadeByStaff(int idStaff) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		int count = 0;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(COUNT_CHANGES_MADE_BY_STAFF);
			statement.setInt(1, idStaff);

			rs = statement.executeQuery();

			if (rs.next()) {

				count = rs.getInt(COUNT_OF_RECORDS);

			}

			return count;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting count of changes failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<Order> getOrdersByClientId(int idClient, int offset, int count) throws DAOException {

		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Order> list = new ArrayList<>();

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(ALL_ORDERS_BY_CLIENT_ID);

			statement.setInt(1, idClient);
			statement.setInt(2, offset);
			statement.setInt(3, count);

			rs = statement.executeQuery();

			while (rs.next()) {
				Order order = new Order();
				order.setId(rs.getInt(ID_ORDER));
				order.setIdClient(rs.getInt(ID_CLIENT));
				order.setTotalCost(rs.getInt(TOTAL_COST));
				order.setTime(new Date(rs.getTimestamp(TIME).getTime()));
				order.setStatus(rs.getString(CURRENT_STATE));
				order.setInPlace(rs.getBoolean(IN_PLACE));
				list.add(order);
			}

			return list;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting client history orders failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public int getCountOfClientOrders(int clientId) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		int count = 0;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(COUNT_CLIENT_ORDERS);
			statement.setInt(1, clientId);

			rs = statement.executeQuery();

			if (rs.next()) {

				count = rs.getInt(COUNT_OF_RECORDS);

			}

			return count;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting count of orders of the client failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

}
