package com.epam.restraunt.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.epam.restraunt.dao.MenuDAO;
import com.epam.restraunt.dao.exception.DAOException;
import com.epam.restraunt.dao.pool.ConnectionPool;
import com.epam.restraunt.dao.pool.exeption.ConnectionPoolException;
import com.epam.restraunt.entity.Dish;
import com.epam.restraunt.entity.Drink;
import com.epam.restraunt.entity.MenuItem;

public class MenuDAOImpl implements MenuDAO {

	private final static String GET_DISH_BY_ID = "SELECT d.idMenuItem, d.timeToCook, d.picture, d.consists, m.name, m.price, m.discount FROM Dish d INNER JOIN MenuItem m ON d.idMenuItem = m.idMenuItem WHERE d.idMenuItem = ?";
	private final static String GET_DRINK_BY_ID = "SELECT d.idMenuItem, d.volume, d.isAlcoholic, m.name, m.price, m.discount FROM Drink d INNER JOIN MenuItem m ON d.idMenuItem = m.idMenuItem WHERE d.idMenuItem = ?";
	private final static String GET_MENU_ITEM_BY_ID = "SELECT idMenuItem, name, price, discount FROM MenuItem WHERE idMenuItem = ?";
	private final static String GET_ALL_DISH = "SELECT d.idMenuItem, d.timeToCook, d.picture, d.consists, m.name, m.price, m.discount FROM Dish d INNER JOIN MenuItem m ON d.idMenuItem = m.idMenuItem ORDER BY m.price DESC";
	private final static String GET_ALL_DRINK = "SELECT d.idMenuItem, d.volume, d.isAlcoholic, m.name, m.price, m.discount FROM Drink d INNER JOIN MenuItem m ON d.idMenuItem = m.idMenuItem ORDER BY m.price DESC";
	private final static String INSERT_INTO_DISH = "INSERT INTO Dish VALUES(LAST_INSERT_ID(), ?, ?, ?)";
	private final static String INSERT_INTO_DRINK = "INSERT INTO Drink VALUES(LAST_INSERT_ID(), ?, ?)";
	private final static String INSERT_INTO_MENU_ITEM = "INSERT INTO MenuItem VALUES(null, ?, ?, ?)";
	private final static String REMOVE_MENU_ITEM = "DELETE FROM MenuItem  WHERE idMenuItem = ?";
	private final static String REMOVE_DISH = "DELETE FROM Dish WHERE idMenuItem = ?";
	private final static String REMOVE_DRINK = "DELETE FROM Drink WHERE idMenuItem = ?";
	private final static String UPDATE_MENU_ITEM = "UPDATE MenuItem SET name = ?, price = ? WHERE idMenuItem = ?";
	private final static String UPDATE_DISH = "UPDATE Dish SET timeToCook = ?, consists = ? WHERE idMenuItem = ?";
	private final static String UPDATE_DRINK = "UPDATE Drink SET volume = ?, isAlcoholic = ? WHERE idMenuItem = ?";
	private final static String GET_ITEMS_BY_ORDER_ID = "SELECT m.idMenuItem, m.name, m.price, m.discount, o.count FROM MenuItem m INNER JOIN Order_has_MenuItem o ON m.idMenuItem = o.idMenuItem WHERE o.idOrder = ? ";

	private static final MenuDAO instance = new MenuDAOImpl();

	private final static Logger LOGGER = Logger.getLogger(MenuDAOImpl.class);
	private static final String GET_MAIN_INFO_ALL_DISHES = "SELECT d.idMenuItem, m.name, m.price, m.discount FROM Dish d INNER JOIN MenuItem m ON d.idMenuItem = m.idMenuItem ORDER BY m.name LIMIT ?, ? ";
	private static final String GET_MAIN_INFO_ALL_DRINKS = "SELECT d.idMenuItem, m.name, m.price, m.discount FROM Drink d INNER JOIN MenuItem m ON d.idMenuItem = m.idMenuItem ORDER BY m.name LIMIT ?, ? ";
	private final static String GET_COUNT_OF_DISHES = "SELECT COUNT(*) AS CountOfRecords FROM Dish ";
	private final static String GET_COUNT_OF_DRINKS = "SELECT COUNT(*) AS CountOfRecords FROM Drink ";

	private MenuDAOImpl() {
	}

	public static MenuDAO getInstance() {
		return instance;
	}

	@Override
	public void insertDish(Dish dish) throws DAOException {

		PreparedStatement statement = null;
		Connection connection = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			connection.setAutoCommit(false);

			statement = connection.prepareStatement(INSERT_INTO_MENU_ITEM);

			statement.setString(1, dish.getName());
			statement.setInt(2, dish.getPrice());
			statement.setInt(3, dish.getDiscount());

			statement.executeUpdate();

			statement.close();

			statement = connection.prepareStatement(INSERT_INTO_DISH);

			statement.setInt(1, dish.getTimeToCook());
			statement.setString(2, dish.getPicture());
			statement.setString(3, dish.getConsists());

			statement.executeUpdate();

			connection.commit();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);

		} catch (SQLException ex) {
			LOGGER.error("Inserting new dish failed", ex);
			try {
				connection.rollback();
			} catch (SQLException e) {
				LOGGER.error("Rolling back transaction failed", ex);
			}
			throw new DAOException(ex);

		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Enabling autocommit failed", e);
			}
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public void insertDrink(Drink drink) throws DAOException {

		PreparedStatement statement = null;
		Connection connection = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			connection.setAutoCommit(false);

			statement = connection.prepareStatement(INSERT_INTO_MENU_ITEM);

			statement.setString(1, drink.getName());
			statement.setInt(2, drink.getPrice());
			statement.setInt(3, drink.getDiscount());

			statement.executeUpdate();

			statement.close();

			statement = connection.prepareStatement(INSERT_INTO_DRINK);

			statement.setInt(1, drink.getVolume());
			statement.setBoolean(2, drink.isAlcoholic());

			statement.executeUpdate();

			connection.commit();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);

		} catch (SQLException ex) {
			LOGGER.error("Inserting new drink failed", ex);
			try {
				connection.rollback();
			} catch (SQLException e) {
				LOGGER.error("Rolling back transaction failed", ex);
			}
			throw new DAOException(ex);

		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Enabling autocommit failed", e);
			}
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public Dish getDishById(int id) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		Dish dish = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_DISH_BY_ID);

			statement.setInt(1, id);

			rs = statement.executeQuery();

			if (rs.next()) {
				dish = new Dish();
				dish.setId(rs.getInt(ID));
				dish.setName(rs.getString(NAME));
				dish.setPrice(rs.getInt(PRICE));
				dish.setDiscount(rs.getInt(DISCOUNT));
				dish.setTimeToCook(rs.getInt(TIME_TO_COOK));
				dish.setPicture(rs.getString(PICTURE));
				dish.setConsists(rs.getString(CONSISTS));

			}
			return dish;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Fetching dish by id failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public Drink getDrinkById(int id) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		Drink drink = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_DRINK_BY_ID);

			statement.setInt(1, id);

			rs = statement.executeQuery();

			if (rs.next()) {
				drink = new Drink();
				drink.setId(rs.getInt(ID));
				drink.setName(rs.getString(NAME));
				drink.setPrice(rs.getInt(PRICE));
				drink.setDiscount(rs.getInt(DISCOUNT));
				drink.setVolume(rs.getInt(VOLUME));
				drink.setAlcoholic(rs.getBoolean(IS_ALCOHOLIC));

			}
			return drink;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Fetching drink by id failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<Dish> getAllDishes() throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Dish> list = new ArrayList<Dish>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_ALL_DISH);

			rs = statement.executeQuery();

			while (rs.next()) {
				Dish dish = new Dish();
				dish.setId(rs.getInt(ID));
				dish.setName(rs.getString(NAME));
				dish.setPrice(rs.getInt(PRICE));
				dish.setDiscount(rs.getInt(DISCOUNT));
				dish.setTimeToCook(rs.getInt(TIME_TO_COOK));
				dish.setPicture(rs.getString(PICTURE));
				dish.setConsists(rs.getString(CONSISTS));
				list.add(dish);
			}

			return list;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Geting all dishes failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<Drink> getAllDrinks() throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Drink> list = new ArrayList<Drink>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_ALL_DRINK);

			rs = statement.executeQuery();

			while (rs.next()) {
				Drink drink = new Drink();
				drink.setId(rs.getInt(ID));
				drink.setName(rs.getString(NAME));
				drink.setPrice(rs.getInt(PRICE));
				drink.setDiscount(rs.getInt(DISCOUNT));
				drink.setVolume(rs.getInt(VOLUME));
				drink.setAlcoholic(rs.getBoolean(IS_ALCOHOLIC));
				list.add(drink);
			}

			return list;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Geting all drinks failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public void removeDish(int id) throws DAOException {

		PreparedStatement statement = null;
		Connection connection = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			connection.setAutoCommit(false);

			statement = connection.prepareStatement(REMOVE_DISH);

			statement.setInt(1, id);

			statement.executeUpdate();

			statement.close();

			statement = connection.prepareStatement(REMOVE_MENU_ITEM);

			statement.setInt(1, id);

			statement.executeUpdate();

			connection.commit();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);

		} catch (SQLException ex) {
			LOGGER.error("Deleting dish from menu failed", ex);
			try {
				connection.rollback();
			} catch (SQLException e) {
				LOGGER.error("Rolling back deleting transaction failed", ex);
			}
			throw new DAOException(ex);

		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Enabling autocommit failed", e);
			}
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public void removeDrink(int id) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			connection.setAutoCommit(false);

			statement = connection.prepareStatement(REMOVE_DRINK);

			statement.setInt(1, id);

			statement.executeUpdate();

			statement.close();

			statement = connection.prepareStatement(REMOVE_MENU_ITEM);

			statement.setInt(1, id);

			statement.executeUpdate();

			connection.commit();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);

		} catch (SQLException ex) {
			LOGGER.error("Deleting drink from menu failed", ex);
			try {
				connection.rollback();
			} catch (SQLException e) {
				LOGGER.error("Rolling back deleting transaction failed", ex);
			}
			throw new DAOException(ex);

		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Enabling autocommit failed", e);
			}
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public List<MenuItem> getAllItems() throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<MenuItem> list = new ArrayList<MenuItem>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(GET_ALL_DISH);

			rs = statement.executeQuery();

			while (rs.next()) {
				Dish dish = new Dish();
				dish.setId(rs.getInt(ID));
				dish.setName(rs.getString(NAME));
				dish.setPrice(rs.getInt(PRICE));
				dish.setDiscount(rs.getInt(DISCOUNT));
				dish.setTimeToCook(rs.getInt(TIME_TO_COOK));
				dish.setPicture(rs.getString(PICTURE));
				dish.setConsists(rs.getString(CONSISTS));
				list.add(dish);
			}

			rs.close();
			statement.close();

			statement = connection.prepareStatement(GET_ALL_DRINK);
			rs = statement.executeQuery();

			while (rs.next()) {
				Drink drink = new Drink();
				drink.setId(rs.getInt(ID));
				drink.setName(rs.getString(NAME));
				drink.setPrice(rs.getInt(PRICE));
				drink.setDiscount(rs.getInt(DISCOUNT));
				drink.setVolume(rs.getInt(VOLUME));
				drink.setAlcoholic(rs.getBoolean(IS_ALCOHOLIC));
				list.add(drink);
			}

			return list;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Geting all menuItems failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public void updateDish(Dish dish) throws DAOException {

		PreparedStatement statement = null;
		Connection connection = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			connection.setAutoCommit(false);

			statement = connection.prepareStatement(UPDATE_MENU_ITEM);

			statement.setString(1, dish.getName());
			statement.setInt(2, dish.getPrice());
			statement.setInt(3, dish.getId());

			statement.executeUpdate();

			statement.close();

			statement = connection.prepareStatement(UPDATE_DISH);

			statement.setInt(1, dish.getTimeToCook());
			statement.setString(2, dish.getConsists());
			statement.setInt(3, dish.getId());

			statement.executeUpdate();

			connection.commit();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);

		} catch (SQLException ex) {
			LOGGER.error("Updating dish failed", ex);
			try {
				connection.rollback();
			} catch (SQLException e) {
				LOGGER.error("Rolling back transaction failed", ex);
			}
			throw new DAOException(ex);

		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Enabling autocommit failed", e);
			}
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public void updateDrink(Drink drink) throws DAOException {

		PreparedStatement statement = null;
		Connection connection = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			connection.setAutoCommit(false);

			statement = connection.prepareStatement(UPDATE_MENU_ITEM);

			statement.setString(1, drink.getName());
			statement.setInt(2, drink.getPrice());
			statement.setInt(3, drink.getId());

			statement.executeUpdate();

			statement.close();

			statement = connection.prepareStatement(UPDATE_DRINK);

			statement.setInt(1, drink.getVolume());
			statement.setBoolean(2, drink.isAlcoholic());
			statement.setInt(3, drink.getId());

			statement.executeUpdate();

			connection.commit();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);

		} catch (SQLException ex) {
			LOGGER.error("Updating drink failed", ex);
			try {
				connection.rollback();
			} catch (SQLException e) {
				LOGGER.error("Rolling back transaction failed", ex);
			}
			throw new DAOException(ex);

		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Enabling autocommit failed", e);
			}
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public MenuItem getMenuItemById(int id) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		MenuItem item = new MenuItem();

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_MENU_ITEM_BY_ID);

			statement.setInt(1, id);

			rs = statement.executeQuery();

			if (rs.next()) {
				item.setId(rs.getInt(ID));
				item.setName(rs.getString(NAME));
				item.setPrice(rs.getInt(PRICE));
				item.setDiscount(rs.getInt(DISCOUNT));
				return item;
			}

			throw new DAOException("Not existing menu item id");

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Fetching menu item by id failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public Map<MenuItem, Integer> getItemsByOrderId(int orderId) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		Map<MenuItem, Integer> items = new HashMap<>();

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_ITEMS_BY_ORDER_ID);

			statement.setInt(1, orderId);

			rs = statement.executeQuery();

			while (rs.next()) {
				MenuItem item = new MenuItem();
				item.setId(rs.getInt(ID));
				item.setName(rs.getString(NAME));
				item.setPrice(rs.getInt(PRICE));
				item.setDiscount(rs.getInt(DISCOUNT));
				int count = rs.getInt(COUNT);
				items.put(item, count);

			}

			return items;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting menu items by order id failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<MenuItem> getAllDishes(int offset, int count) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<MenuItem> list = new ArrayList<MenuItem>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_MAIN_INFO_ALL_DISHES);
			statement.setInt(1, offset);
			statement.setInt(2, count);

			rs = statement.executeQuery();

			while (rs.next()) {
				MenuItem item = new MenuItem();
				item.setId(rs.getInt(ID));
				item.setName(rs.getString(NAME));
				item.setPrice(rs.getInt(PRICE));
				item.setDiscount(rs.getInt(DISCOUNT));

				list.add(item);
			}

			return list;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Geting main info of all dishes failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public int getCountOfDishes() throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		int count = 0;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(GET_COUNT_OF_DISHES);

			rs = statement.executeQuery();

			if (rs.next()) {

				count = rs.getInt(COUNT_OF_RECORDS);

			}

			return count;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting  count dishes failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<MenuItem> getAllDrinks(int offset, int count) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		List<MenuItem> list = new ArrayList<MenuItem>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_MAIN_INFO_ALL_DRINKS);
			statement.setInt(1, offset);
			statement.setInt(2, count);

			rs = statement.executeQuery();

			while (rs.next()) {
				MenuItem item = new MenuItem();
				item.setId(rs.getInt(ID));
				item.setName(rs.getString(NAME));
				item.setPrice(rs.getInt(PRICE));
				item.setDiscount(rs.getInt(DISCOUNT));

				list.add(item);
			}

			return list;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Geting main info of all drinks failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public int getCountOfDrinks() throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		int count = 0;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(GET_COUNT_OF_DRINKS);

			rs = statement.executeQuery();

			if (rs.next()) {

				count = rs.getInt(COUNT_OF_RECORDS);

			}

			return count;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting  count drinks failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public void removeItem(int id) throws DAOException {

		PreparedStatement statement = null;
		Connection connection = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(REMOVE_MENU_ITEM);

			statement.setInt(1, id);

			statement.executeUpdate();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Removing menu item  failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}
	}
}
