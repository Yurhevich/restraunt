package com.epam.restraunt.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.restraunt.dao.ClientDAO;
import com.epam.restraunt.dao.exception.DAOException;
import com.epam.restraunt.dao.pool.ConnectionPool;
import com.epam.restraunt.dao.pool.exeption.ConnectionPoolException;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.entity.Staff;

public class ClientDAOImpl implements ClientDAO {

	private final static String CHECK_CLIENT = "SELECT idClient, name, surname, email, password, balance FROM Client WHERE login = ?";
	private final static String CHECK_EMAIL = "SELECT email FROM Client WHERE email = ?";
	private final static String INSERT_CLIENT = "INSERT INTO Client VALUES(null, ?, ?, ?, ?, 0, ?)";
	private final static String DEPOSIT = "UPDATE client SET balance = balance + ? WHERE idClient = ?";
	private final static String CHECK_PASSWORD = "SELECT password FROM Client WHERE idClient = ?";
	private final static String CHANGE_PASSWORD = "UPDATE client SET password = ? WHERE idClient = ?";
	private final static String PAYMENT = "UPDATE client SET balance = balance - ? WHERE idClient = ?";
	private final static String DELETE_ACC = "DELETE FROM client WHERE idClient = ?";
	private final static String GET_BY_ID = "SELECT idClient, login, name, surname, email, balance FROM Client WHERE IdClient = ?";

	private final static Logger LOGGER = Logger.getLogger(ClientDAOImpl.class);
	private static final String GET_COUNT_OF_CLIENTS = "SELECT COUNT(*) AS CountOfRecords FROM Client ";
	private static final String GET_ALL_CLIENTS = "SELECT idClient, login, email FROM Client ORDER BY balance DESC LIMIT ?, ?";
	private static final String GET_BY_LOGIN = "SELECT idClient, login, email FROM Client WHERE login = ?";
	private static final String SET_CLIENT_BALANCE = "UPDATE Client SET balance=? WHERE idClient=?";

	private static final ClientDAO instance = new ClientDAOImpl();

	private ClientDAOImpl() {
	}

	public static ClientDAO getInstance() {
		return instance;
	}

	@Override
	public void insert(Client client) throws DAOException {

		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(INSERT_CLIENT);

			statement.setString(1, client.getName());
			statement.setString(2, client.getSurname());
			statement.setString(3, client.getLogin());
			statement.setString(4, client.getPassword());
			statement.setString(5, client.getEmail());

			statement.executeUpdate();

			statement.close();

			statement = connection.prepareStatement(CHECK_CLIENT);

			statement.setString(1, client.getLogin());
			rs = statement.executeQuery();

			int id = 0;
			if (rs.next()) {
				id = rs.getInt(ID);
			}
			client.setId(id);

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);

		} catch (SQLException ex) {
			LOGGER.error("Inserting client failed", ex);
			throw new DAOException(ex);

		} finally {
			if (rs != null) {
				ConnectionPool.getInstance().closeConnection(connection, statement, rs);
			} else
				ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public boolean ifAlreadyExistLogin(Client client) throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {

			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(CHECK_CLIENT);
			statement.setString(1, client.getLogin());
			rs = statement.executeQuery();
			if (rs.next()) {
				return true;
			}

			return false;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			throw new DAOException(e);
		}

		catch (SQLException e) {
			LOGGER.error("Checking existing login statement failed", e);
			throw new DAOException(e);

		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}

	}

	@Override
	public boolean ifAlreadyExistEmail(Client client) throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(CHECK_EMAIL);
			statement.setString(1, client.getEmail());
			rs = statement.executeQuery();
			if (rs.next()) {
				return true;
			}
			return false;
		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		}

		catch (SQLException e) {
			LOGGER.error("Checking existing email statement failed", e);
			throw new DAOException(e);

		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public boolean checkLoginAndPassword(Client client) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(CHECK_CLIENT);
			statement.setString(1, client.getLogin());
			rs = statement.executeQuery();
			if (rs.next()) {

				if (rs.getString(PASSWORD).equals(client.getPassword())) {
					client.setId(rs.getInt(ID));
					client.setName(rs.getString(NAME));
					client.setSurname(rs.getString(SURNAME));
					client.setBalance(rs.getInt(BALANCE));
					client.setEmail(rs.getString(EMAIL));

					return true;
				}

			}

			return false;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		}

		catch (SQLException e) {
			LOGGER.error("Checking login and password statemnets failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public void deposit(int id, int depositSum) throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(DEPOSIT);
			statement.setInt(1, depositSum);
			statement.setInt(2, id);
			statement.executeUpdate();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		}

		catch (SQLException e) {
			LOGGER.error("Deposit statement failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public boolean checkPassword(int userId, String password) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(CHECK_PASSWORD);
			statement.setInt(1, userId);
			rs = statement.executeQuery();
			if (rs.next()) {

				if (rs.getString(PASSWORD).equals(password)) {
					return true;
				}

			}

			return false;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Check password statement failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}

	}

	@Override
	public void changePassword(int userId, String newPassword) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(CHANGE_PASSWORD);
			statement.setString(1, newPassword);
			statement.setInt(2, userId);
			statement.executeUpdate();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Changing user password failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public void pay(int clientId, int debt) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(PAYMENT);
			statement.setInt(1, debt);
			statement.setInt(2, clientId);
			rs = statement.executeQuery();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Payment statement failed", e);
			throw new DAOException(e);

		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}

	}

	@Override
	public void deleteAccount(int id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(DELETE_ACC);
			statement.setInt(1, id);
			statement.executeUpdate();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Delete account statement failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public Client fetchById(int id) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		Client client = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_BY_ID);

			statement.setInt(1, id);

			rs = statement.executeQuery();

			if (rs.next()) {
				client = new Client();
				client.setId(rs.getInt(ID));
				client.setLogin(rs.getString(LOGIN));
				client.setName(rs.getString(NAME));
				client.setSurname(rs.getString(SURNAME));
				client.setBalance(rs.getInt(BALANCE));
				client.setEmail(rs.getString(EMAIL));

			}
			return client;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Fetching client by id failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement);
		}
	}

	@Override
	public List<Client> getAllClients(int offset, int count) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		List<Client> clientList = new ArrayList<>();

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(GET_ALL_CLIENTS);
			statement.setInt(1, offset);
			statement.setInt(2, count);

			rs = statement.executeQuery();

			while (rs.next()) {
				Client client = new Client();
				client.setId(rs.getInt(ID));
				client.setLogin(rs.getString(LOGIN));
				client.setEmail(rs.getString(EMAIL));
				clientList.add(client);
			}

			return clientList;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting all staff failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public int getCountOfClients() throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		int count = 0;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(GET_COUNT_OF_CLIENTS);

			rs = statement.executeQuery();

			if (rs.next()) {

				count = rs.getInt(COUNT_OF_RECORDS);

			}

			return count;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting clients count failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public Client fetchByLogin(String login) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		Client client = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_BY_LOGIN);

			statement.setString(1, login);

			rs = statement.executeQuery();

			if (rs.next()) {
				client = new Client();
				client.setId(rs.getInt(ID));
				client.setLogin(rs.getString(LOGIN));
				client.setEmail(rs.getString(EMAIL));

			}
			return client;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Fetching client by login failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement);
		}
	}

	@Override
	public void setBalance(int id, int invoice) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(SET_CLIENT_BALANCE);
			statement.setInt(1, invoice);
			statement.setInt(2, id);

			statement.executeUpdate();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Setting client balance failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

}
