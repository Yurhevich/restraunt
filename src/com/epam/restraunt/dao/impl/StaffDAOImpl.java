package com.epam.restraunt.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.restraunt.dao.StaffDAO;
import com.epam.restraunt.dao.exception.DAOException;
import com.epam.restraunt.dao.pool.ConnectionPool;
import com.epam.restraunt.dao.pool.exeption.ConnectionPoolException;
import com.epam.restraunt.entity.Staff;

public class StaffDAOImpl implements StaffDAO {

	private final static String CHECK_STAFF = "SELECT idStaff, name, surname, post, photo, telephone, email, address, login, password, isAdmin FROM Staff WHERE login = ?";
	private final static String CHECK_EMAIL = "SELECT email FROM Staff WHERE email = ?";
	private final static String INSERT_STAFF = "INSERT INTO Staff VALUES(null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private final static String DELETE_ACC = "DELETE FROM Staff WHERE idStaff = ?";
	private final static String GET_BY_ID = "SELECT idStaff, name, surname, post, photo, telephone, email, address, login, password, isAdmin FROM Staff WHERE IdStaff = ?";
	private static final String GET_COUNT_OF_PERSONAL = "SELECT COUNT(*) AS CountOfRecords FROM Staff ";
	private static final String GET_ALL_STAFF = "SELECT idStaff, name, surname, post FROM Staff ORDER BY surname, name LIMIT ?, ?";
	private static final String GET_MAIN_INF_ID = "SELECT idStaff, name, surname, post FROM Staff WHERE idStaff = ?";
	private static final String GET_MAIN_INF_SURNAME = "SELECT idStaff, name, surname, post FROM Staff WHERE surname = ?";
    private static final String UPDATE_STAFF = "UPDATE Staff SET name=?, surname=?, post=?, telephone=?, email=?, address=?, login=?, password=? WHERE idStaff=?";
    private static final String DELETE_STAFF = "DELETE FROM Staff WHERE idStaff=?";
	
	
	private static final StaffDAO INSTANCE = new StaffDAOImpl();

	private final static Logger LOGGER = Logger.getLogger(StaffDAOImpl.class);

	private StaffDAOImpl() {
	}

	public static StaffDAO getInstance() {
		return INSTANCE;
	}

	@Override
	public void insert(Staff person) throws DAOException {

		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(INSERT_STAFF);

			statement.setString(1, person.getName());
			statement.setString(2, person.getSurname());
			statement.setString(3, person.getPost());
			statement.setString(4, person.getLogin());
			statement.setString(5, person.getPassword());
			statement.setString(6, person.getPhoto());
			statement.setString(7, person.getTelephone());
			statement.setString(8, person.getEmail());
			statement.setString(9, person.getAddress());
			statement.setBoolean(10, person.isAdmin());

			statement.executeUpdate();

			statement.close();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);

		} catch (SQLException ex) {
			LOGGER.error("Inserting staff failed", ex);
			throw new DAOException(ex);

		} finally {
			if (rs != null) {
				ConnectionPool.getInstance().closeConnection(connection, statement, rs);
			} else
				ConnectionPool.getInstance().closeConnection(connection, statement);
		}

	}

	@Override
	public Staff fetchById(int idStaff) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;
		Staff person = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_BY_ID);

			statement.setInt(1, idStaff);

			rs = statement.executeQuery();

			if (rs.next()) {
				person = new Staff();
				person.setId(rs.getInt(ID));
				person.setLogin(rs.getString(LOGIN));
				person.setName(rs.getString(NAME));
				person.setSurname(rs.getString(SURNAME));
				person.setEmail(rs.getString(EMAIL));
				person.setAddress(rs.getString(ADDRESS));
				person.setAdmin(rs.getBoolean(IS_ADMIN));
				person.setPassword(rs.getString(PASSWORD));
				person.setPhoto(rs.getString(PHOTO));
				person.setTelephone(rs.getString(TELEPHONE));
				person.setPost(rs.getString(POST));

			}
			return person;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Fetching staff by id failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}

	}

	@Override
	public boolean ifAlreadyExistLogin(Staff staff) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(CHECK_STAFF);
			statement.setString(1, staff.getLogin());
			rs = statement.executeQuery();
			if (rs.next()) {
				return true;
			}

			return false;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		}

		catch (SQLException e) {
			LOGGER.error("Checking existing staff login statement failed", e);
			throw new DAOException(e);

		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public boolean ifAlreadyExistEmail(Staff client) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(CHECK_EMAIL);
			statement.setString(1, client.getEmail());
			rs = statement.executeQuery();
			if (rs.next()) {
				return true;
			}
			return false;
		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		}

		catch (SQLException e) {
			LOGGER.error("Checking existing staff email statement failed", e);
			throw new DAOException(e);

		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public boolean staffLogin(Staff person) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(CHECK_STAFF);
			statement.setString(1, person.getLogin());
			rs = statement.executeQuery();

			if (rs.next()) {

				if (rs.getString(PASSWORD).equals(person.getPassword())) {
					person.setId(rs.getInt(ID));
					person.setName(rs.getString(NAME));
					person.setSurname(rs.getString(SURNAME));
					person.setEmail(rs.getString(EMAIL));
					person.setAddress(rs.getString(ADDRESS));
					person.setPhoto(rs.getString(PHOTO));
					person.setAdmin(rs.getBoolean(IS_ADMIN));
					person.setTelephone(rs.getString(TELEPHONE));
					person.setPost(rs.getString(POST));

					return true;
				}

			}

			return false;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		}

		catch (SQLException e) {
			LOGGER.error("Checking staff login and password statemnets failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public void deleteAccount(int idStaff) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(DELETE_ACC);
			statement.setInt(1, idStaff);
			rs = statement.executeQuery();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Delete staff account statement failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}

	}

	@Override
	public int getCountOfPersonal() throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		int count = 0;

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(GET_COUNT_OF_PERSONAL);

			rs = statement.executeQuery();

			if (rs.next()) {

				count = rs.getInt(COUNT_OF_RECORDS);

			}

			return count;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting staff count failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public List<Staff> getAllPersonal(int offset, int count) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		List<Staff> staffList = new ArrayList<>();

		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(GET_ALL_STAFF);
			statement.setInt(1, offset);
			statement.setInt(2, count);

			rs = statement.executeQuery();

			while (rs.next()) {
				Staff staff = new Staff();
				staff.setId(rs.getInt(ID));
				staff.setName(rs.getString(NAME));
				staff.setSurname(rs.getString(SURNAME));
				staff.setPost(rs.getString(POST));
				staffList.add(staff);
			}

			return staffList;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Getting all staff failed", e);
			throw new DAOException(e);
		} finally {
			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public Staff getMainInf(int idStaff) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_MAIN_INF_ID);

			statement.setInt(1, idStaff);

			rs = statement.executeQuery();
			Staff person = null;
			if (rs.next()) {
				person = new Staff();
				person.setId(rs.getInt(ID));
				person.setName(rs.getString(NAME));
				person.setSurname(rs.getString(SURNAME));
				person.setPost(rs.getString(POST));

			}
			return person;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Fetching main inf staff by id failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public Staff getMainInf(String surname) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet rs = null;

		try {
			connection = ConnectionPool.getInstance().takeConnection();
			statement = connection.prepareStatement(GET_MAIN_INF_SURNAME);

			statement.setString(1, surname);

			rs = statement.executeQuery();
			Staff person = null;
			if (rs.next()) {
				person = new Staff();
				person.setId(rs.getInt(ID));
				person.setName(rs.getString(NAME));
				person.setSurname(rs.getString(SURNAME));
				person.setPost(rs.getString(POST));

			}
			return person;

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);
		} catch (SQLException e) {
			LOGGER.error("Fetching main inf staff by surname failed", e);
			throw new DAOException(e);

		} finally {

			ConnectionPool.getInstance().closeConnection(connection, statement, rs);
		}
	}

	@Override
	public void updateStaff(Staff staff) throws DAOException {
		PreparedStatement statement = null;
		Connection connection = null;
		try {
			connection = ConnectionPool.getInstance().takeConnection();

			statement = connection.prepareStatement(UPDATE_STAFF);

			statement.setString(1, staff.getName());
			statement.setString(2, staff.getSurname());
			statement.setString(3, staff.getPost());
			statement.setString(4, staff.getTelephone());
			statement.setString(5, staff.getEmail());
			statement.setString(6, staff.getAddress());
			statement.setString(7, staff.getLogin());				
			statement.setString(8, staff.getPassword());		
			statement.setInt(9, staff.getId());

			statement.executeUpdate();

			statement.close();

		} catch (ConnectionPoolException e) {
			LOGGER.error("Attempt to take connection from connection pool failed", e);
			ConnectionPool.getInstance().closeConnection(connection);
			throw new DAOException(e);

		} catch (SQLException ex) {
			LOGGER.error("Updating staff failed", ex);
			throw new DAOException(ex);

		} finally {
			
				ConnectionPool.getInstance().closeConnection(connection, statement);
		}
	}

	@Override
	public void deleteStaff(int id) throws DAOException {
			PreparedStatement statement = null;
			Connection connection = null;
			try {
				connection = ConnectionPool.getInstance().takeConnection();

				statement = connection.prepareStatement(DELETE_STAFF);
			
				statement.setInt(1, id);

				statement.executeUpdate();

				statement.close();

			} catch (ConnectionPoolException e) {
				LOGGER.error("Attempt to take connection from connection pool failed", e);
				ConnectionPool.getInstance().closeConnection(connection);
				throw new DAOException(e);

			} catch (SQLException ex) {
				LOGGER.error("Deleting staff failed", ex);
				throw new DAOException(ex);

			} finally {
					ConnectionPool.getInstance().closeConnection(connection, statement);
			}	
	}

}
