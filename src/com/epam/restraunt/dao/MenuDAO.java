package com.epam.restraunt.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.epam.restraunt.dao.exception.DAOException;
import com.epam.restraunt.entity.Dish;
import com.epam.restraunt.entity.Drink;
import com.epam.restraunt.entity.MenuItem;

public interface MenuDAO {
	
	final static String NAME = "name";
	final static String PRICE = "price";
	final static String DISCOUNT = "discount";
	final static String TIME_TO_COOK = "timeToCook";
	final static String PICTURE = "picture";
	final static String CONSISTS = "consists";
	final static String VOLUME = "volume";
	final static String IS_ALCOHOLIC = "isAlcoholic";
    final static String ID = "idMenuItem";
    final static String COUNT = "count";
    String COUNT_OF_RECORDS = "CountOfRecords";
    
	void insertDish(Dish dish) throws DAOException;

	void insertDrink(Drink drink) throws DAOException;

	Dish getDishById(int id) throws DAOException;

	Drink getDrinkById(int id) throws DAOException;
	
	MenuItem getMenuItemById(int id) throws DAOException;

	List<Dish> getAllDishes() throws DAOException;

	List<Drink> getAllDrinks() throws DAOException;

	void removeDish(int id) throws DAOException;

	void removeDrink(int id) throws DAOException;

	List<MenuItem> getAllItems() throws DAOException;
	
	void updateDish(Dish dish) throws DAOException;
	
	void updateDrink(Drink drink) throws DAOException;

	Map<MenuItem, Integer> getItemsByOrderId(int orderId) throws DAOException;

	List<MenuItem> getAllDishes(int offset, int count) throws DAOException;

	int getCountOfDishes() throws DAOException;

	List<MenuItem> getAllDrinks(int offset, int count) throws DAOException;

	int getCountOfDrinks() throws DAOException;

	void removeItem(int id) throws DAOException;
	
	

}
