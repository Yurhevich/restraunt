package com.epam.restraunt.dao;

import com.epam.restraunt.dao.impl.ClientDAOImpl;
import com.epam.restraunt.dao.impl.MenuDAOImpl;
import com.epam.restraunt.dao.impl.OrderDAOImpl;
import com.epam.restraunt.dao.impl.StaffDAOImpl;

public final class DAOFactory {

	private DAOFactory(){
	}
		
	public static ClientDAO getClientDAO(){
		return ClientDAOImpl.getInstance();
	}
	
	public static MenuDAO getMenuDAO(){
		return MenuDAOImpl.getInstance();
	}
	
	public static OrderDAO getOrderDAO(){
		return OrderDAOImpl.getInstance();
	}
	
	public static StaffDAO getStaffDAO(){
		return StaffDAOImpl.getInstance();
	}
	
}
