package com.epam.restraunt.dao;

import java.util.List;

import com.epam.restraunt.dao.exception.DAOException;
import com.epam.restraunt.entity.Client;

public interface ClientDAO {

	 String NAME = "name";
	String SURNAME = "surname";
	String LOGIN = "login";
	 String BALANCE = "balance";
	String PASSWORD = "password";
	 String EMAIL= "email";
	 String ID = "idClient";
	 String COUNT_OF_RECORDS = "CountOfRecords";
	
	void insert(Client client) throws DAOException;
	
	Client fetchById(int id) throws DAOException;

	boolean ifAlreadyExistLogin(Client client) throws DAOException;

	boolean ifAlreadyExistEmail(Client client) throws DAOException;

	boolean checkLoginAndPassword(Client client) throws DAOException;
	
	void deposit(int clientId, int depositSum)throws DAOException;
	
	void changePassword(int clientId, String newPassword) throws DAOException;
	
	boolean checkPassword(int clientId, String password) throws DAOException;

	void pay(int clientId, int debt) throws DAOException;
	
	void deleteAccount(int clientId) throws DAOException;

	List<Client> getAllClients(int offset, int count) throws DAOException;

	int getCountOfClients() throws DAOException;

	Client fetchByLogin(String login) throws DAOException;

	void setBalance(int id, int invoice) throws DAOException;
	
	
	
}
