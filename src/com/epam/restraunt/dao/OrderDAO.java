package com.epam.restraunt.dao;

import java.util.Date;
import java.util.List;

import com.epam.restraunt.dao.exception.DAOException;
import com.epam.restraunt.entity.Delivery;
import com.epam.restraunt.entity.Order;
import com.epam.restraunt.entity.OrderStateChange;

public interface OrderDAO {

	String ID_CLIENT = "idClient";
	String ID_ORDER = "idOrder";
	String ID_STAFF = "idStaff";
	String ID_CHANGE = "idOrderStateChange";
	String TOTAL_COST = "totalCost";
	String TIME = "time";
	String CURRENT_STATE = "currentState";
	String IN_PLACE = "inPlace";
	String TELEPHONE = "telephone";
	String ADDRESS = "address";
	String ESTABLISHED_STATE = "establishedState";
	String TIME_OF_CHANGE = "timeOfChange";
	String NAME = "name";
	String SURNAME = "surname";
	String POST = "post";
	String COUNT_OF_RECORDS = "CountOfRecords";

	void addOrder(Order order) throws DAOException;

	void addOrderWithDelivery(Order order, Delivery delivery) throws DAOException;

	Order getOrderById(int id) throws DAOException;

	List<Order> getOrdersByClientId(int idClient) throws DAOException;

	void updateOrderState(OrderStateChange change) throws DAOException;

	Delivery getDeliveryById(int id) throws DAOException;

	List<Order> getOrdersByState(String state, int offset, int count) throws DAOException;

	List<Order> getOrdersByStateSince(String state, Date date) throws DAOException;

	List<Order> getAwaitingOrders() throws DAOException;

	List<OrderStateChange> getCertainStaffHistory(int idStaff) throws DAOException;

	List<OrderStateChange> getCertainStaffHistorySince(int idStaff, Date date) throws DAOException;

	String getCurrentStateOfOrder(int idOrder) throws DAOException;

	List<Order> getTodayOrders(int offset, int count) throws DAOException;

	List<Order> getYestardayOrders(int offset, int count) throws DAOException;

	List<OrderStateChange> getOrderStateChanges(int idOrder) throws DAOException;

	List<Order> getOrdersByStaffId(int idStaff, int offset, int count) throws DAOException;

	int getCountOfOrders(String state) throws DAOException;

	int getCountOfServedOrders(int idStaff) throws DAOException;

	int getCountOfYestardayOrders() throws DAOException;

	int getCountOfTodayOrders() throws DAOException;

	List<OrderStateChange> getChangesMadeByStaff(int idStaff, int offset, int count) throws DAOException;

	int getCountOfChangesMadeByStaff(int idStaff) throws DAOException;

	List<Order> getOrdersByClientId(int idClient, int offset, int count) throws DAOException;

	int getCountOfClientOrders(int clientId) throws DAOException;

	List<Order> getLastWeekOrders() throws DAOException;

}
