package com.epam.restraunt.dao;


import java.util.List;

import com.epam.restraunt.dao.exception.DAOException;
import com.epam.restraunt.entity.Staff;

public interface StaffDAO {
	 String NAME = "name";
	 String SURNAME = "surname";
	 String LOGIN = "login";
	 String PASSWORD = "password";
	 String EMAIL= "email";
	 String ID = "idStaff";
	 String POST = "post";
	 String PHOTO = "photo";
	 String TELEPHONE = "telephone";
	 String IS_ADMIN = "isAdmin";
	 String ADDRESS = "address";
	 String COUNT_OF_RECORDS = "CountOfRecords";
	
	
	void insert(Staff person) throws DAOException;
	
	Staff fetchById(int idStaff) throws DAOException;

	boolean ifAlreadyExistLogin(Staff staff) throws DAOException;

	boolean ifAlreadyExistEmail(Staff staff) throws DAOException;

	boolean staffLogin(Staff staff) throws DAOException;
	
	void deleteAccount(int idStaff) throws DAOException;

	int getCountOfPersonal() throws DAOException;

	List<Staff> getAllPersonal(int offset, int count) throws DAOException;

	Staff getMainInf(int idStaff) throws DAOException;

	Staff getMainInf(String surname) throws DAOException;

	void updateStaff(Staff updatedStaff) throws DAOException;

	void deleteStaff(int id) throws DAOException;
	

	
}
