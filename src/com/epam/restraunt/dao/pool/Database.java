package com.epam.restraunt.dao.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.epam.restraunt.dao.pool.exeption.DatabaseException;

public final class Database {

	private final static Logger LOGGER = Logger.getLogger(Database.class);
	private static final DBResourceManager resource = DBResourceManager.getInstance();
	private static final Database database = new Database();
	
	
	private final String userName;
	private final String password;
	private final String url;

	
	private Database() {
		userName = resource.getValue(DBParameter.DB_USER);
		password = resource.getValue(DBParameter.DB_PASSWORD);	
		url = resource.getValue(DBParameter.DB_URL);
		
	}


	static {
		try {
			Class.forName(resource.getValue(DBParameter.DB_DRIVER));
		} catch (ClassNotFoundException ex) {
			
			LOGGER.error("Error registering database driver", ex);
			throw new RuntimeException(ex);
		}
	}
	
	public static Database getInstance() {
		return database;
	}

	public Connection getConnection() throws DatabaseException {

		Connection con = null;

		try {

			con = DriverManager.getConnection(url, userName, password);

		} catch (SQLException e) {
			
			LOGGER.error("Attempt to get connection from database failed", e);

			try {
				con.close();
			} catch (SQLException e1) {
				LOGGER.error("Exception closing failed connection ", e);
			}

			try {
				con = DriverManager.getConnection(url, userName, password);
			} catch (SQLException e2) {
				LOGGER.error("Second attempt to get connection from database failed", e2);

				try {
					con.close();
				} catch (SQLException e3) {
					LOGGER.error("Exception closing second failed connection ", e3);
				}

				throw new DatabaseException("Exception during getting Connection from database", e);
			}

		}
		return con;

	}
}
