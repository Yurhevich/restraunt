package com.epam.restraunt.dao.pool.exeption;

public class DatabaseException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public DatabaseException(String message, Exception e) {
		super(message, e);
	}
}
