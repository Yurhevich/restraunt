package com.epam.restraunt.dao.pool;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;

import com.epam.restraunt.dao.pool.exeption.ConnectionPoolException;
import com.epam.restraunt.dao.pool.exeption.DatabaseException;

public class ConnectionPool {
	
	
	private final static Logger LOGGER = Logger.getLogger(ConnectionPool.class);

	
	private final ArrayBlockingQueue<PooledConnection> freeQueue;
	private final ArrayBlockingQueue<PooledConnection> givingAwayQueue;
	
	
	private static final Database database = Database.getInstance();
	
	
	private static final ConnectionPool instance = new ConnectionPool();

	private static int poolSize;

	private ConnectionPool() {

		

		DBResourceManager resource = DBResourceManager.getInstance();
	
		try {
			poolSize = Integer.parseInt(resource.getValue(DBParameter.DB_POOL_SIZE));
		} catch (NumberFormatException ex) {
			poolSize = 5;
		}

		freeQueue = new ArrayBlockingQueue<>(poolSize);
		givingAwayQueue = new ArrayBlockingQueue<>(poolSize);

		for (int i = 0; i < poolSize; i++) {
			PooledConnection con;
			try {
				con = new PooledConnection(database.getConnection());
				freeQueue.offer(con);
			} catch (DatabaseException e) {
				LOGGER.error("Error adding connection to freeConnectionQueue", e);
			}

		}
		
	}

	public static ConnectionPool getInstance() {
		return instance;
	}

	public PooledConnection takeConnection() throws ConnectionPoolException {
		PooledConnection connection = null;

		try {
			connection = freeQueue.take();
			givingAwayQueue.put(connection);
		} catch (InterruptedException e) {
			LOGGER.error("Thread Interrupted while trying taking connection", e);
			throw new ConnectionPoolException("Thread Interrupted while trying taking connection", e);

		}
		return connection;
	}

	public void closeConnection(Connection connection, Statement st, ResultSet rs) {

		closeConnection(connection);

		try {
			st.close();
		} catch (SQLException e) {
			LOGGER.error("Statement isn't closed");
		}

		try {
			rs.close();
		} catch (SQLException e) {
			LOGGER.error("Result set isn't closed");
		}

	}

	public void closeConnection(Connection connection, Statement st) {

		closeConnection(connection);

		try {
			st.close();
		} catch (SQLException e) {
			LOGGER.error("Statement isn't closed");
		}

	}

	public void closeConnection(Connection connection) {

		try {
			connection.close();
		} catch (SQLException e) {
			LOGGER.error("Connection isn't return to the pool");

			if (givingAwayQueue.contains(connection)) {
				givingAwayQueue.remove(connection);
			}
			if (freeQueue.contains(connection)) {
				freeQueue.remove(connection);
			}

			try {
				freeQueue.add(new PooledConnection(database.getConnection()));
			} catch (DatabaseException e1) {
				LOGGER.error("Trying replace failed connection by new in connection queue failed", e1);
			}

			((PooledConnection) connection).reallyClose();

		}

	}

	public void dispose() {
		clearConnectionQueues();
	}

	private void clearConnectionQueues() {
		try {
			closeConnectionQueue(freeQueue);
			closeConnectionQueue(givingAwayQueue);
		} catch (SQLException e) {
			// logger.error("Error closing the connection", e)
		}

	}

	private void closeConnectionQueue(BlockingQueue<PooledConnection> queue) throws SQLException {
		Connection connection;

		while ((connection = queue.poll()) != null) {

			if (!connection.getAutoCommit()) {
				connection.commit();
			}
			((PooledConnection) connection).reallyClose();
		}
	}

	private class PooledConnection implements Connection {

		private Connection connection;

		PooledConnection(final Connection connection) {
			this.connection = connection;
		}

		@Override
		public void close() throws SQLException {

			if (connection.isClosed()) {
				throw new SQLException("Attempting to close closed connection");
			}

			if (!givingAwayQueue.remove(this)) {
				throw new SQLException("Error deleting connection from the giving away connection pool ");
			}

			if (!freeQueue.offer(this)) {
				throw new SQLException("Error allocating connection in the pool ");
			}

		}

		void reallyClose() {
			try {
				connection.close();
			} catch (SQLException ex) {
				LOGGER.error("SQL connection error trying to close connection", ex);
			}
		}

		@Override
		public Statement createStatement() throws SQLException {
			return connection.createStatement();
		}

		@Override
		public void commit() throws SQLException {
			connection.commit();
		}

		@Override
		public boolean isClosed() throws SQLException {
			return connection.isClosed();
		}

		@Override
		public PreparedStatement prepareStatement(final String sql) throws SQLException {
			return connection.prepareStatement(sql);
		}

		@Override
		public void rollback() throws SQLException {
			connection.rollback();
		}

		@Override
		public void setAutoCommit(boolean flag) throws SQLException {
			connection.setAutoCommit(flag);
		}

		@Override
		public CallableStatement prepareCall(String sql) throws SQLException {
			return connection.prepareCall(sql);
		}

		@Override
		public String nativeSQL(String sql) throws SQLException {
			return connection.nativeSQL(sql);
		}

		@Override
		public boolean getAutoCommit() throws SQLException {
			return connection.getAutoCommit();
		}

		@Override
		public DatabaseMetaData getMetaData() throws SQLException {
			return connection.getMetaData();
		}

		@Override
		public void setReadOnly(boolean readOnly) throws SQLException {
			connection.setReadOnly(readOnly);
		}

		@Override
		public boolean isReadOnly() throws SQLException {
			return connection.isReadOnly();
		}

		@Override
		public void setCatalog(String catalog) throws SQLException {
			connection.setCatalog(catalog);
		}

		@Override
		public String getCatalog() throws SQLException {
			return connection.getCatalog();
		}

		@Override
		public void setTransactionIsolation(int level) throws SQLException {
			connection.setTransactionIsolation(level);
		}

		@Override
		public int getTransactionIsolation() throws SQLException {
			return connection.getTransactionIsolation();
		}

		@Override
		public SQLWarning getWarnings() throws SQLException {
			return connection.getWarnings();
		}

		@Override
		public void clearWarnings() throws SQLException {
			connection.clearWarnings();
		}

		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
			return connection.createStatement(resultSetType, resultSetConcurrency);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
				throws SQLException {
			return connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency)
				throws SQLException {
			return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
		}

		@Override
		public Map<String, Class<?>> getTypeMap() throws SQLException {
			return connection.getTypeMap();
		}

		@Override
		public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
			connection.setTypeMap(map);
		}

		@Override
		public void setHoldability(int holdability) throws SQLException {
			connection.setHoldability(holdability);
		}

		@Override
		public int getHoldability() throws SQLException {
			return connection.getHoldability();
		}

		@Override
		public Savepoint setSavepoint() throws SQLException {
			return connection.setSavepoint();
		}

		@Override
		public Savepoint setSavepoint(String name) throws SQLException {
			return connection.setSavepoint(name);
		}

		@Override
		public void rollback(Savepoint savepoint) throws SQLException {
			connection.rollback(savepoint);
		}

		@Override
		public void releaseSavepoint(Savepoint savepoint) throws SQLException {
			connection.releaseSavepoint(savepoint);
		}

		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)
				throws SQLException {
			return connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency,
				int resultSetHoldability) throws SQLException {
			return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency,
				int resultSetHoldability) throws SQLException {
			return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
			return connection.prepareStatement(sql, autoGeneratedKeys);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
			return connection.prepareStatement(sql, columnIndexes);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
			return connection.prepareStatement(sql, columnNames);
		}

		@Override
		public Clob createClob() throws SQLException {
			return connection.createClob();
		}

		@Override
		public Blob createBlob() throws SQLException {
			return connection.createBlob();
		}

		@Override
		public NClob createNClob() throws SQLException {
			return connection.createNClob();
		}

		@Override
		public SQLXML createSQLXML() throws SQLException {
			return connection.createSQLXML();
		}

		@Override
		public boolean isValid(int timeout) throws SQLException {
			return connection.isValid(timeout);
		}

		@Override
		public void setClientInfo(String name, String value) throws SQLClientInfoException {
			connection.setClientInfo(name, value);
		}

		@Override
		public void setClientInfo(Properties properties) throws SQLClientInfoException {
			connection.setClientInfo(properties);
		}

		@Override
		public String getClientInfo(String name) throws SQLException {
			return connection.getClientInfo(name);
		}

		@Override
		public Properties getClientInfo() throws SQLException {
			return connection.getClientInfo();
		}

		@Override
		public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
			return connection.createArrayOf(typeName, elements);
		}

		@Override
		public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
			return connection.createStruct(typeName, attributes);
		}

		@Override
		public void setSchema(String schema) throws SQLException {
			connection.setSchema(schema);
		}

		@Override
		public String getSchema() throws SQLException {
			return connection.getSchema();
		}

		@Override
		public void abort(Executor executor) throws SQLException {
			connection.abort(executor);
		}

		@Override
		public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
			connection.setNetworkTimeout(executor, milliseconds);
		}

		@Override
		public int getNetworkTimeout() throws SQLException {
			return connection.getNetworkTimeout();
		}

		@Override
		public <T> T unwrap(Class<T> iface) throws SQLException {
			return connection.unwrap(iface);
		}

		@Override
		public boolean isWrapperFor(Class<?> iface) throws SQLException {
			return connection.isWrapperFor(iface);
		}
	}

}
