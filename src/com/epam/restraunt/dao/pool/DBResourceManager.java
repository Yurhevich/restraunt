package com.epam.restraunt.dao.pool;

import java.util.ResourceBundle;

public class DBResourceManager {

	private final static DBResourceManager instance = new DBResourceManager();

	private ResourceBundle res;
	
	
	
	private DBResourceManager(){
		 res = ResourceBundle.getBundle("resources.database");
		
	}

	public static DBResourceManager getInstance() {
		return instance;
	}

	public String getValue(String key) {
		return res.getString(key);
	}

}
