package com.epam.restraunt.service;

import com.epam.restraunt.service.impl.ClientServiceImpl;
import com.epam.restraunt.service.impl.MenuServiceImpl;
import com.epam.restraunt.service.impl.OrderServiceImpl;
import com.epam.restraunt.service.impl.StaffServiceImpl;

public final class ServiceFactory {

	private ServiceFactory() {

	}

	public static ClientService getClientService() {
		return ClientServiceImpl.getInstance();
	
	}
	
	public static MenuService getMenuService(){
		return MenuServiceImpl.getInstance();
	}
	
	public static StaffService getStaffService(){
		return StaffServiceImpl.getInstance();
	}
	
	public static OrderService getOrderService(){
		return OrderServiceImpl.getInstance();
	}

	
}
