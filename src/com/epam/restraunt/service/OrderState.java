package com.epam.restraunt.service;


public enum OrderState{
		
		AWAITING("awaiting"),
		PROCESSING_COOKING("processing cooking"),
		AWAITING_DELIVERY("awaiting delivery"),
		PROCESSING_DELIVERY("processing delivery"),
		DONE("done"),
		CANCELLED("cancelled");
		
		private String str;

		private OrderState(String str) {
			this.str = str;
		}
		
		public static String checkState(String stateToCheck){
			for(OrderState state  : OrderState.values()){
				if(state.name().equalsIgnoreCase(stateToCheck)){
					return state.str;
				}
				
				
				
			}
			return null;
		}

		
		public static String getNextState(String cur, boolean inPlace) {
			
			if(cur.equalsIgnoreCase(OrderState.AWAITING.str)){
				return PROCESSING_COOKING.name().toLowerCase();
			}
			
			if(cur.equalsIgnoreCase(OrderState.PROCESSING_COOKING.str)){
				if(inPlace){
					return DONE.name().toLowerCase();
				}else{
					return AWAITING_DELIVERY.name().toLowerCase();
				}
			}
			
			
			if(cur.equalsIgnoreCase(OrderState.AWAITING_DELIVERY.str)){
				return PROCESSING_DELIVERY.name().toLowerCase();
			}
			
			if(cur.equalsIgnoreCase(OrderState.PROCESSING_DELIVERY.str)){
				return DONE.name().toLowerCase();
			}
			
			return null;
			
			
		}
	}

