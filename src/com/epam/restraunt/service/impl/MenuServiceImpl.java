package com.epam.restraunt.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.epam.restraunt.dao.DAOFactory;
import com.epam.restraunt.dao.MenuDAO;
import com.epam.restraunt.dao.exception.DAOException;
import com.epam.restraunt.entity.Dish;
import com.epam.restraunt.entity.Drink;
import com.epam.restraunt.entity.MenuItem;
import com.epam.restraunt.service.MenuService;
import com.epam.restraunt.service.exception.ServiceException;

public class MenuServiceImpl implements MenuService {

	private final static Logger LOGGER = Logger.getLogger(MenuServiceImpl.class);

	private static final MenuService service = new MenuServiceImpl();

	private MenuServiceImpl() {
	}

	public static MenuService getInstance() {
		return service;
	}

	@Override
	public void addDish(Dish dish) throws ServiceException {

		MenuDAO dao = DAOFactory.getMenuDAO();
		try {
			dao.insertDish(dish);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public void addDrink(Drink drink) throws ServiceException {

		MenuDAO dao = DAOFactory.getMenuDAO();
		try {
			dao.insertDrink(drink);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public Dish getDishById(int id) throws ServiceException {

		MenuDAO dao = DAOFactory.getMenuDAO();
		Dish dish = null;
		try {
			dish = dao.getDishById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return dish;
	}

	@Override
	public Drink getDrinkById(int id) throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		Drink drink = null;
		try {
			drink = dao.getDrinkById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return drink;
	}

	@Override
	public List<Dish> getAllDishes() throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		List<Dish> list = null;
		try {
			list = dao.getAllDishes();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;

	}

	@Override
	public List<Drink> getAllDrinks() throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		List<Drink> list = null;
		try {
			list = dao.getAllDrinks();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}

	@Override
	public void deleteDishFromMenu(int id) throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		try {
			dao.removeDish(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public void deleteDrinkFromMenu(int id) throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		try {
			dao.removeDrink(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public List<MenuItem> getAllItems() throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		List<MenuItem> list = null;
		try {
			list = dao.getAllItems();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}

	@Override
	public void updateDish(Dish dish) throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		try {
			dao.updateDish(dish);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public void updateDrink(Drink drink) throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		try {
			dao.updateDrink(drink);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public MenuItem getMenuItemById(int id) throws ServiceException {

		MenuDAO dao = DAOFactory.getMenuDAO();
		MenuItem item = null;
		try {
			item = dao.getMenuItemById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return item;
	}

	@Override
	public Map<MenuItem, Integer> getItemsByOrderId(int orderId) throws ServiceException {

		MenuDAO dao = DAOFactory.getMenuDAO();
		Map<MenuItem, Integer> items = null;
		try {
			items = dao.getItemsByOrderId(orderId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return items;
	}

	@Override
	public List<MenuItem> getAllDishes(int offset, int count) throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		List<MenuItem> list = null;
		try {
			list = dao.getAllDishes(offset, count);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}

	@Override
	public int getCountOfDishes() throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		int countOfRecords = 0;

		try {
			countOfRecords = dao.getCountOfDishes();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return countOfRecords;
	}

	@Override
	public List<MenuItem> getAllDrinks(int offset, int count) throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		List<MenuItem> list = null;
		try {
			list = dao.getAllDrinks(offset, count);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}

	@Override
	public int getCountOfDrinks() throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		int countOfRecords = 0;

		try {
			countOfRecords = dao.getCountOfDrinks();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return countOfRecords;
	}

	@Override
	public MenuItem getMenuItem(int id) throws ServiceException {
		MenuDAO dao = DAOFactory.getMenuDAO();
		MenuItem item = null;
		try {
			item = dao.getDishById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		if (item != null) {
			return item;
		} else {
			try {
				item = dao.getDrinkById(id);
			} catch (DAOException e) {
				throw new ServiceException(e);
			}
			return item;
		}
	}

	@Override
	public void removeMenuItem(int id) throws ServiceException {
		
		MenuDAO dao = DAOFactory.getMenuDAO();
		try {
			dao.removeItem(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

}
