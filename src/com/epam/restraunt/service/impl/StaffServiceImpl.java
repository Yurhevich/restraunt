package com.epam.restraunt.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.restraunt.dao.DAOFactory;
import com.epam.restraunt.dao.StaffDAO;
import com.epam.restraunt.dao.exception.DAOException;
import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.StaffService;
import com.epam.restraunt.service.exception.ServiceException;

public class StaffServiceImpl implements StaffService {

	private final static Logger LOGGER = Logger.getLogger(StaffServiceImpl.class);

	private static final StaffService service = new StaffServiceImpl();

	private StaffServiceImpl() {
	}

	public static StaffService getInstance() {
		return service;
	}

	@Override
	public void insert(Staff person) throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();

		try {
			dao.insert(person);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public Staff fetchById(int idStaff) throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();
		Staff staff = null;

		try {
			staff = dao.fetchById(idStaff);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return staff;
	}

	@Override
	public boolean ifAlreadyExistLogin(Staff staff) throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();
		boolean ifExists = false;

		try {
			ifExists = dao.ifAlreadyExistLogin(staff);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return ifExists;
	}

	@Override
	public boolean ifAlreadyExistEmail(Staff staff) throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();
		boolean ifExists = false;

		try {
			ifExists = dao.ifAlreadyExistEmail(staff);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return ifExists;
	}

	@Override
	public void deleteAccount(int idStaff) throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();

		try {
			dao.deleteAccount(idStaff);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public boolean login(Staff staff) throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();

		try {
			return dao.staffLogin(staff);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean registrateStaff(Staff staff) throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();

		try {
			if (!dao.ifAlreadyExistLogin(staff)) {
				if (!dao.ifAlreadyExistEmail(staff)) {
					dao.insert(staff);
					return true;
				}
			}

			return false;

		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public List<Staff> getAllPersonal(int offset, int count) throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();
		List<Staff> listStaff = null;

		try {
			listStaff = dao.getAllPersonal(offset, count);

		}

		catch (DAOException e) {
			throw new ServiceException(e);
		}
		return listStaff;
	}

	@Override
	public int getCountOfPersonal() throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();
		int numOfRecords = 0;

		try {
			numOfRecords = dao.getCountOfPersonal();

		}

		catch (DAOException e) {
			throw new ServiceException(e);
		}
		return numOfRecords;
	}

	@Override
	public Staff getMainInfoAboutStaff(int id) throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();
		Staff staff = null;

		try {
			staff = dao.getMainInf(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return staff;
	}

	@Override
	public Staff getMainInfoAboutStaff(String surname) throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();
		Staff staff = null;

		try {
			staff = dao.getMainInf(surname);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return staff;
	}

	@Override
	public void updateStaff(Staff updatedStaff) throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();

		try {
			dao.updateStaff(updatedStaff);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteStaff(int id) throws ServiceException {
		StaffDAO dao = DAOFactory.getStaffDAO();
		try {
			dao.deleteStaff(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

}
