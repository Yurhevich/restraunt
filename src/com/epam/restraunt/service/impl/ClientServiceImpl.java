package com.epam.restraunt.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.restraunt.dao.ClientDAO;
import com.epam.restraunt.dao.DAOFactory;
import com.epam.restraunt.dao.exception.DAOException;
import com.epam.restraunt.entity.Client;
import com.epam.restraunt.service.ClientService;
import com.epam.restraunt.service.exception.ServiceException;

public class ClientServiceImpl implements ClientService {

	private final static Logger LOGGER = Logger.getLogger(ClientServiceImpl.class);

	private static final ClientService service = new ClientServiceImpl();

	private ClientServiceImpl() {
	}

	public static ClientService getInstance() {
		return service;
	}

	@Override
	public boolean register(Client client) throws ServiceException {

		ClientDAO dao = DAOFactory.getClientDAO();
		try {
			if (!dao.ifAlreadyExistLogin(client)) {
				if (!dao.ifAlreadyExistEmail(client)) {
					dao.insert(client);
					return true;
				}
			}
			
			return false;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public boolean login(Client client) throws ServiceException {
		ClientDAO dao = DAOFactory.getClientDAO();
		try {
			if (dao.checkLoginAndPassword(client)) {
				return true;
			}
			return false;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deposit(int clientId, int sum) throws ServiceException {
		ClientDAO dao = DAOFactory.getClientDAO();
		try {
			dao.deposit(clientId, sum);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public boolean checkPassword(int userId, String password) throws ServiceException {
		ClientDAO dao = DAOFactory.getClientDAO();
		try {
			if (dao.checkPassword(userId, password)) {
				return true;
			}

			return false;

		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public void pay(int clientId, int debt) throws ServiceException {
		ClientDAO dao = DAOFactory.getClientDAO();
		try{
		dao.pay(clientId, debt);
		}catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	@Override
	public void changePassword(int userId, String newPassword) throws ServiceException {
		ClientDAO dao = DAOFactory.getClientDAO();
		try{
		dao.changePassword(userId, newPassword);
		}catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	@Override
	public void deleteAccount(int clientId) throws ServiceException {
		ClientDAO dao = DAOFactory.getClientDAO();
		try{
		dao.deleteAccount(clientId);
		}catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Client fetchById(int clientId) throws ServiceException {
		ClientDAO dao = DAOFactory.getClientDAO();
		Client client;
		try {
			client = dao.fetchById(clientId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return client;
	}

	@Override
	public List<Client> getAllClients(int offset, int count) throws ServiceException {
		ClientDAO dao = DAOFactory.getClientDAO();
		List<Client> listClients = null;
		try {
		listClients = dao.getAllClients(offset, count);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return listClients;
	}

	@Override
	public int getCountOfClients() throws ServiceException {
		ClientDAO dao = DAOFactory.getClientDAO();
		int count = 0;
		try {
		count = dao.getCountOfClients();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return count;
	}

	@Override
	public Client fetchByLogin(String login) throws ServiceException {
		ClientDAO dao = DAOFactory.getClientDAO();
		Client client;
		try {
			client = dao.fetchByLogin(login);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return client;
	}

	@Override
	public void setInvoice(int id, int invoice) throws ServiceException {
		ClientDAO dao = DAOFactory.getClientDAO();
		try {
		   dao.setBalance(id,invoice);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

}
