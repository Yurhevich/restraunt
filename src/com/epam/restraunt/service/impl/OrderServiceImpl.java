package com.epam.restraunt.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.restraunt.dao.DAOFactory;
import com.epam.restraunt.dao.OrderDAO;
import com.epam.restraunt.dao.exception.DAOException;
import com.epam.restraunt.entity.Delivery;
import com.epam.restraunt.entity.Order;
import com.epam.restraunt.entity.OrderStateChange;
import com.epam.restraunt.service.OrderService;
import com.epam.restraunt.service.OrderState;
import com.epam.restraunt.service.exception.ServiceException;

public class OrderServiceImpl implements OrderService {

	private final static Logger LOGGER = Logger.getLogger(OrderServiceImpl.class);

	private static final OrderService service = new OrderServiceImpl();

	private OrderServiceImpl() {
	}

	public static OrderService getInstance() {
		return service;
	}

	@Override
	public void addOrder(Order order) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		try {
			dao.addOrder(order);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public void addOrderWithDelivery(Order order, Delivery delivery) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		try {
			dao.addOrderWithDelivery(order, delivery);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Order getOrderById(int id) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();
		Order order = null;
		try {
			order = dao.getOrderById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return order;
	}



	

	@Override
	public List<Order> getOrdersByClientId(int idClient) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();
		List<Order> list = null;

		try {
			list = dao.getOrdersByClientId(idClient);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}

	@Override
	public void updateOrderState(OrderStateChange change) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		try {
			dao.updateOrderState(change);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Override
	public Delivery getDeliveryById(int id) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();
		Delivery delivery = null;

		try {
			delivery = dao.getDeliveryById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return delivery;
	}



	@Override
	public List<Order> getOrdersByState(String state, int offset, int count) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();
		List<Order> list = null;

		try {
			list = dao.getOrdersByState(state, offset, count);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return list;
	}



	@Override
	public List<Order> getAwaitingOrders() throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();
		List<Order> list = null;

		try {
			list = dao.getAwaitingOrders();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return list;
	}

	@Override
	public List<OrderStateChange> getCertainStaffHistory(int idStaff) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();
		List<OrderStateChange> list = null;

		try {
			list = dao.getCertainStaffHistory(idStaff);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}


	@Override
	public String getCurrentStateOfOrder(int idOrder) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();
		String state = null;

		try {
			state = dao.getCurrentStateOfOrder(idOrder);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return state;
	}

	@Override
	public List<Order> getTodayOrders(int offset, int count) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		List<Order> list = null;

		try {
			list = dao.getTodayOrders(offset, count);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}

	@Override
	public List<Order> getLastWeekOrders() throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		List<Order> list = null;

		try {
			list = dao.getLastWeekOrders();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}

	@Override
	public List<Order> getYestardayOrders(int offset, int count) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		List<Order> list = null;

		try {
			list = dao.getYestardayOrders(offset, count);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}

	@Override
	public List<OrderStateChange> getOrderStateChanges(int orderId) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		List<OrderStateChange> list = null;

		try {
			list = dao.getOrderStateChanges(orderId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}

	@Override
	public List<Order> getOrdersByStaffId(int id, int offset, int count) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		List<Order> list = null;

		try {
			list = dao.getOrdersByStaffId(id, offset, count);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}

	@Override
	public String getNextState(String cur, boolean inPlace) throws ServiceException {
		return OrderState.getNextState(cur,inPlace);
	}

	@Override
	public int getCountOfOrders(String state) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		int countOfRecords = 0;

		try {
			countOfRecords = dao.getCountOfOrders(state);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return countOfRecords;
	}

	@Override
	public int getCountOfServedOrders(int idStaff) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		int countOfRecords = 0;

		try {
			countOfRecords = dao.getCountOfServedOrders(idStaff);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return countOfRecords;
	}

	@Override
	public int getCountOfYestardayOrders() throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		int countOfRecords = 0;

		try {
			countOfRecords = dao.getCountOfYestardayOrders();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return countOfRecords;
	}

	@Override
	public int getCountOfTodayOrders() throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		int countOfRecords = 0;

		try {
			countOfRecords = dao.getCountOfTodayOrders();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return countOfRecords;
	}

	@Override
	public List<OrderStateChange> getChangesMadeByStaff(int idStaff, int offset, int count) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		List<OrderStateChange> list = null;

		try {
			list = dao.getChangesMadeByStaff(idStaff, offset, count);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}

	@Override
	public int getCountOfChangesMadeByStaff(int idStaff) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		int countOfRecords = 0;

		try {
			countOfRecords = dao.getCountOfChangesMadeByStaff(idStaff);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return countOfRecords;
	}

	@Override
	public List<Order> getOrdersByClientId(int idClient, int offset, int count) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();
		List<Order> list = null;

		try {
			list = dao.getOrdersByClientId(idClient,offset,count);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return list;
	}

	@Override
	public int getCountOfClientOrders(int clientId) throws ServiceException {
		OrderDAO dao = DAOFactory.getOrderDAO();

		int countOfRecords = 0;

		try {
			countOfRecords = dao.getCountOfClientOrders(clientId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return countOfRecords;
	}

	
	
}
