package com.epam.restraunt.service;

import java.util.List;
import java.util.Map;

import com.epam.restraunt.entity.Dish;
import com.epam.restraunt.entity.Drink;
import com.epam.restraunt.entity.MenuItem;
import com.epam.restraunt.service.exception.ServiceException;

public interface MenuService {
	
	void addDish(Dish dish) throws ServiceException;

	void addDrink(Drink drink) throws ServiceException;

	Dish getDishById(int id) throws ServiceException;

	Drink getDrinkById(int id) throws ServiceException;
	
	MenuItem getMenuItemById(int id) throws ServiceException;

	List<Dish> getAllDishes() throws ServiceException;

	List<Drink> getAllDrinks() throws ServiceException;

	void deleteDishFromMenu(int id) throws ServiceException;

	void deleteDrinkFromMenu(int id) throws ServiceException;

	List<MenuItem> getAllItems() throws ServiceException;
	
	void updateDish(Dish dish) throws ServiceException;
	
	void updateDrink(Drink drink) throws ServiceException;

	Map<MenuItem, Integer> getItemsByOrderId(int id) throws ServiceException;

	List<MenuItem> getAllDishes(int offset, int count) throws ServiceException;

	int getCountOfDishes() throws ServiceException;

	List<MenuItem> getAllDrinks(int offset, int count) throws ServiceException;

	int getCountOfDrinks() throws ServiceException;

	MenuItem getMenuItem(int id) throws ServiceException;

	void removeMenuItem(int id) throws ServiceException;
	
}
