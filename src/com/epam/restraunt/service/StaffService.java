package com.epam.restraunt.service;


import java.util.List;

import com.epam.restraunt.entity.Staff;
import com.epam.restraunt.service.exception.ServiceException;

public interface StaffService {


	void insert(Staff staff) throws ServiceException;
	
	boolean login(Staff staff) throws ServiceException;
	
	boolean registrateStaff(Staff staff) throws ServiceException;
	
	Staff fetchById(int idStaff) throws ServiceException;

	boolean ifAlreadyExistLogin(Staff staff) throws ServiceException;

	boolean ifAlreadyExistEmail(Staff client) throws ServiceException;
	
	void deleteAccount(int idStaff) throws ServiceException;

	List<Staff> getAllPersonal(int offset, int count) throws ServiceException;

	int getCountOfPersonal() throws ServiceException;

	Staff getMainInfoAboutStaff(int id) throws ServiceException;

	Staff getMainInfoAboutStaff(String surname) throws ServiceException;

	void updateStaff(Staff updatedStaff) throws ServiceException;

	void deleteStaff(int id) throws ServiceException;

	
}
