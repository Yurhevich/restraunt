package com.epam.restraunt.service;

import java.util.List;

import com.epam.restraunt.entity.Client;
import com.epam.restraunt.service.exception.ServiceException;

public interface ClientService {
	
	Client fetchById(int id) throws ServiceException;

	boolean register(Client client) throws ServiceException;

	boolean login(Client client) throws ServiceException;

	void deposit(int clientId, int sum) throws ServiceException;
	
	boolean checkPassword(int clientId, String password) throws ServiceException;
	
	void changePassword(int clientId, String newPassword) throws ServiceException;
	
	void deleteAccount(int clientId) throws ServiceException;
	
	void pay(int clientiD, int sum) throws ServiceException;

	List<Client> getAllClients(int offset, int count) throws ServiceException;

	int getCountOfClients() throws ServiceException;

	Client fetchByLogin(String login) throws ServiceException;

	void setInvoice(int id, int invoice) throws ServiceException;
	
   
	
}
