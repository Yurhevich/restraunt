package com.epam.restraunt.service;

import java.util.List;

import com.epam.restraunt.entity.Delivery;
import com.epam.restraunt.entity.Order;
import com.epam.restraunt.entity.OrderStateChange;
import com.epam.restraunt.service.exception.ServiceException;

public interface OrderService {

	
	void addOrder(Order order) throws ServiceException;

	void addOrderWithDelivery(Order order, Delivery delivery) throws ServiceException;

	Order getOrderById(int id) throws ServiceException;

	List<Order> getOrdersByClientId(int idClient) throws ServiceException;

	void updateOrderState(OrderStateChange change) throws ServiceException;

	Delivery getDeliveryById(int id) throws ServiceException;

	List<Order> getOrdersByState(String state, int offset, int count) throws ServiceException;

	List<Order> getAwaitingOrders() throws ServiceException;

	List<OrderStateChange> getCertainStaffHistory(int idStaff) throws ServiceException;

	String getCurrentStateOfOrder(int idOrder) throws ServiceException;

	List<Order> getTodayOrders(int offset, int count) throws ServiceException;
	
	List<Order> getYestardayOrders(int offset, int count) throws ServiceException;

	List<Order> getLastWeekOrders() throws ServiceException;

	List<OrderStateChange> getOrderStateChanges(int orderId) throws ServiceException;

	List<Order> getOrdersByStaffId(int idStaff, int offset, int count) throws ServiceException;

	String getNextState(String cur, boolean b) throws ServiceException;
	
	int getCountOfOrders(String state) throws ServiceException;

	int getCountOfServedOrders(int idStaff) throws ServiceException;
	
	int getCountOfYestardayOrders() throws ServiceException;
	
	int getCountOfTodayOrders() throws ServiceException;

	List<OrderStateChange> getChangesMadeByStaff(int idStaff, int offset, int count) throws ServiceException;

	int getCountOfChangesMadeByStaff(int idStaff) throws ServiceException;

	List<Order> getOrdersByClientId(int idClient,int offset, int count) throws ServiceException;

	int getCountOfClientOrders(int idClient) throws ServiceException;
	
	

}
