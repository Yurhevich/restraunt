<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Личный кабинет</title>
<link rel="stylesheet" href="/restraunt/css/main.css" />
</head>
<body>
	<div class="wrapper">
		<c:import url="/staff/fragment/staffHeader.jsp" />
		<div class="content">
			<div class="main-content">
				<h2 class="content-title">Информация о сотруднике</h2>


				<div class="account__container ">

					<div class="accinfo_menu">
						<div class="staff_accinfo_sidebar">
							<ul>
								<li><a class="account_button"
									href="/restraunt/staff/staffMain.jsp">Назад к главной</a></li>
								<c:if test="${sessionScope.staff.admin && not requestScope.staff.admin}">
									<li><a class="account_button" id="change_staff"
										href="#">Изменить параметры</a></li>
									<li><a class="account_button" id="delete_staff"
										href="#">Удалить аккаунт</a></li>
								</c:if>
							</ul>
						</div>
					</div>
					<div class="staff_photo">
						<div class="staff-photo-wrapper">
							<img src="${staff.photo}" alt="" />
						</div>
					</div>

					<div class="accinfo_content">
						<ul class="account__list">

							<li class="account__item"><span class="account__key">Имя:
							</span><span class="account__value">${staff.name}</span></li>
							<li class="account__item"><span class="account__key">Фамилия:
							</span><span class="account__value">${staff.surname}</span></li>
							<li class="account__item"><span class="account__key">Должность:
							</span><span class="account__value">${staff.post}</span></li>
							<li class="account__item"><span class="account__key">Телефон:
							</span><span class="account__value">${staff.telephone}</span></li>
							<li class="account__item"><span class="account__key">email:
							</span><span class="account__value">${staff.email}</span></li>
							<c:if test="${sessionScope.staff.admin}">
								<li class="account__item"><span class="account__key">Адрес:
								</span><span class="account__value">${staff.address}</span></li>
								<li class="account__item"><span class="account__key">Логин:
								</span><span class="account__value">${staff.login}</span></li>
							</c:if>
						</ul>

					</div>

				</div>
			</div>
		</div>
		<c:import url="/staff/fragment/staffFooter.jsp" />

	</div>

<c:if test="${sessionScope.staff.admin}">
<div class="popup-staff-register">
		<div class="popup">
			<form id="staff-change-form" 
				action="/restraunt/controller" method="post">
				<input type="text" name="action" value="update_staff"
					hidden="true">
				<input type="text" name="id" value="${staff.id}"
					hidden="true">
				<div class="popup_row">
					<p class="popup_label message_title">Изменить параметры :</p>
				</div>
				<p>
					<input type="text" name="name" value="${staff.name}" class="popup_input"
						placeholder="Введите имя" required>
				</p>
				<p>
					<input type="text" name="surname" value="${staff.surname}" class="popup_input"
						placeholder="Введите фамилию" required>
				</p>
				<p>
					<input type="text" name="post" value="${staff.post}" class="popup_input"
						placeholder="Введите должность" required>
				</p>
				<p>
					<input type="text" name="login" value="${staff.login}" class="popup_input"
						placeholder="Введите логин" required>
				</p>
				<p>
					<input type="text" name="password" value="${staff.password}" class="popup_input"
						placeholder="Введите пароль" required>
				</p>
				<p>
					<input type="text" name="telephone" value="${staff.telephone}" class="popup_input"
						placeholder="Введите телефон" required>
				</p>
				<p>
					<input type="text" name="email" value="${staff.email}" class="popup_input"
						placeholder="Введите email" required>
				</p>
				<p>
					<input type="text" name="address" value="${staff.address}" class="popup_input"
						placeholder="Введите адрес" required>
				</p>
		
				<div class="popup_row">
					<p></p>
					<button type="submit" class="popup_submit">Изменить</button>
				</div>
				<a href="#" class="close_button close-staff-register">X</a>
			</form>
		</div>
	</div>
</c:if>

<c:if test="${sessionScope.staff.admin}">

<div class="popup-staff-delete">
		<div class="popup">
			<form id="staff-delete-form" 
				action="/restraunt/controller" method="post">
				<input type="text" name="action" value="delete_staff"
					hidden="true">
				<input type="text" name="id" value="${staff.id}"
					hidden="true">
				<div class="popup_row">
					<p class="popup_label message_title">Подтверждение :</p>
				</div>
				
				<div class="popup_row">
					<p class="popup_label">Вы уверены что хотите удалить аккаунт?</p>
				</div>
								
				<div class="popup_row">
					<p></p>
					<button type="submit" class="popup_submit">Удалить</button>
				</div>
				<a href="#" class="close_button close" id="close_delete_staff">X</a>
			</form>
		</div>
	</div>
	
	</c:if>
	<div class="popup-message">
		<div class="popup">
			<div class="popup_row">
				<label for="name" class="popup_label message_title">Заказ:</label>
				<p id="message"></p>
			</div>
			<div class="popup_row">
				<button type="submit" class="popup_submit close_message">OK</button>
			</div>
			<a href="#" class="close_message close_button">X</a>
		</div>
	</div>

	<script src="/restraunt/js/vendors/jquery-1.9.1.min.js"></script>
	<script src="/restraunt/js/index.js"></script>
</body>
</html>