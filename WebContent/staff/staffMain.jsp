<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Текущие заказы</title>
<link rel="stylesheet" href="/restraunt/css/main.css" />
</head>
<body>
	<div class="wrapper">
		<c:import url="/staff/fragment/staffHeader.jsp" />
		<div class="staff_content">
			<div class="main-content">

				<h2 class="info-content-title">Главная страница</h2>


				<div class="staff__container ">

					<div class="staff_menu">
						<div class="staff_sidebar">
							<div class="order-nav-wrapper">
								<a href="#" id="order_trigger" class="staff_menu_button">Заказы</a>

								<ul id="order_action_list" class="order_action_list trigger">
									<li><form class="staff_nav" action="/restraunt/controller">
											<input type="text" value="get_orders_by_state" hidden="true"
												name="action" /> <input type="text" value="awaiting"
												hidden="true" name="state" />
											<button type="submit" class="staff_button">Текущие
												заказы</button>
										</form></li>
									<li><form class="staff_nav" action="/restraunt/controller">
											<input type="text" value="get_orders_by_state" hidden="true"
												name="action" /> <input type="text"
												value="processing_cooking" hidden="true" name="state" />
											<button type="submit" class="staff_button">Готовятся</button>
										</form></li>

									<li><form class="staff_nav" action="/restraunt/controller">
											<input type="text" value="get_orders_by_state" hidden="true"
												name="action" /> <input type="text"
												value="awaiting_delivery" hidden="true" name="state" />
											<button type="submit" class="staff_button">В
												ожидании доставки</button>
										</form></li>

									<li><form class="staff_nav" action="/restraunt/controller">
											<input type="text" value="get_orders_by_state" hidden="true"
												name="action" /> <input type="text"
												value="processing_delivery" hidden="true" name="state" />
											<button type="submit" class="staff_button">В
												процессе доставки</button>
										</form></li>


									<li><form class="staff_nav" action="/restraunt/controller">
											<input type="text" value="get_orders_by_state" hidden="true"
												name="action" /> <input type="text" value="done"
												hidden="true" name="state" />
											<button type="submit" class="staff_button">Обслуженные</button>
										</form></li>

									<li><form class="staff_nav" action="/restraunt/controller">
											<input type="text" value="today_orders" hidden="true"
												name="action" />
											<button type="submit" class="staff_button">Сегодняшние
												заказы</button>
										</form></li>

									<li><form class="staff_nav" action="/restraunt/controller">
											<input type="text" value="yestarday_orders" hidden="true"
												name="action" />
											<button type="submit" class="staff_button">Вчерашние
												заказы</button>
										</form></li>

									<li><form class="staff_nav" action="/restraunt/controller">
											<input type="text" value="get_staff_orders_history"
												hidden="true" name="action" />
											<button type="submit" class="staff_button">Мои
												заказы</button>
										</form></li>


									<li><form class="staff_nav" action="/restraunt/controller">
											<input type="text" value="get_orders_by_state" hidden="true"
												name="action" /> <input type="text" value="cancelled"
												hidden="true" name="state" />
											<button type="submit" class="staff_button">Отмененные
												заказы</button>
										</form></li>

									<c:if test="${sessionScope.staff.admin}">
										<li>
											<button type="submit" id="order_by_id" class="staff_button">Заказ
												по номеру</button>
										</li>
									</c:if>
								</ul>
							</div>
							<form class="staff_nav" action="/restraunt/controller">
								<input type="text" value="get_order_changes" hidden="true"
									name="action" />
								<button type="submit" class="staff_menu_button">Моя
									история</button>
							</form>
							<c:if test="${not sessionScope.staff.admin}">
								<a href="/restraunt/staff/staffInfo.jsp"
									class="staff_menu_button">Обо мне</a>
							</c:if>
							<c:if test="${sessionScope.staff.admin}">

								<div class="order-nav-wrapper">
									<a href="#" id="personal_trigger" class="staff_menu_button">Сотрудники</a>

									<ul id="personal_action_list" class="order_action_list trigger">

										<li><form class="staff_nav"
												action="/restraunt/controller">
												<input type="text" value="get_all_personal" hidden="true"
													name="action" />
												<button type="submit" class="staff_button">Весь
													персонал</button>
											</form></li>


										<li>
											<button type="submit" id="personal_by_id"
												class="staff_button">Сотрудник по id</button>
										</li>

										<li>
											<button type="submit" id="personal_by_name"
												class="staff_button">Поиск по фамилии</button>
										</li>


										<li>
											<button type="submit" id="add_new_staff" class="staff_button">Зарегистрировать</button>
										</li>

									</ul>
								</div>
							</c:if>


							<div class="order-nav-wrapper">
								<a href="#" id="menu_trigger" class="staff_menu_button">Меню</a>

								<ul id="menu_list" class="menu_list trigger">

									<li><form class="staff_nav" action="/restraunt/controller">
											<input type="text" value="get_all_dishes" hidden="true"
												name="action" />
											<button type="submit" class="staff_button">Все блюда</button>
										</form></li>


									<li>
									<li><form class="staff_nav" action="/restraunt/controller">
											<input type="text" value="get_all_drinks" hidden="true"
												name="action" />
											<button type="submit" class="staff_button">Все
												напитки</button>
										</form></li>

									<c:if test="${sessionScope.staff.admin}">
										<li>
										<li>

											<button type="submit" id="add_dish" class="staff_button">Добавить
												блюдо</button>
										</li>

										<li>
											<button type="submit" id="add_drink" class="staff_button">Добавить
												напиток</button>
										</li>
									</c:if>
								</ul>
							</div>
							<c:if test="${sessionScope.staff.admin}">

								<div class="order-nav-wrapper">
									<a href="#" id="client_trigger" class="staff_menu_button">Клиенты</a>

									<ul id="client_list" class="client_list trigger">


										<li><form class="staff_nav"
												action="/restraunt/controller">
												<input type="text" value="get_all_clients" hidden="true"
													name="action" />
												<button type="submit" class="staff_button">Все
													клиенты</button>
											</form></li>


										<li>
											<button type="submit" id="client_by_id" class="staff_button">Клиент
												по id</button>
										</li>

										<li>
											<button type="submit" id="client_by_name"
												class="staff_button">Клиент по логину</button>
										</li>

									</ul>
								</div>
							</c:if>
						</div>

					</div>

					<div class="accinfo_content">

						<div class="row-button-staff">
							<h1 class="staff_title">Текущие заказы</h1>

							<form id="refresh">
								<input type="text" value="get_orders_by_state" hidden="true"
									name="action" /> <input type="text" value="awaiting"
									hidden="true" name="state" />
								<button type="submit" class="right-button popup_submit">Обновить</button>
							</form>

							<div class="clear-div"></div>
						</div>

						<table class="staff-table table-orders" cellspacing='0'>
							<colgroup>
								<col style="width: 19%">
								<col style="width: 19%">
								<col style="width: 16%">
								<col style="width: 18%">
								<col style="width: 27%">
							</colgroup>
							<caption>Список заказов</caption>
							<thead>
								<tr>
									<th>№</th>
									<th>Дата</th>
									<th>Время</th>
									<th>Общая стоимость</th>
									<th>Состояние</th>

								</tr>
							</thead>
							<tbody></tbody>


						</table>
						<table class="staff-table table-order-changes" cellspacing='0'>
							<colgroup>
								<col style="width: 30%">
								<col style="width: 20%">
								<col style="width: 20%">
								<col style="width: 30%">
							</colgroup>
							<caption>История установленных состояний</caption>
							<thead>
								<tr>
									<th>№ заказа</th>
									<th>Дата</th>
									<th>Время</th>
									<th>Установленное состояние</th>

								</tr>
							</thead>
							<tbody></tbody>


						</table>

						<table class="staff-table table-personal" cellspacing='0'>
							<colgroup>
								<col style="width: 30%">
								<col style="width: 20%">
								<col style="width: 20%">
								<col style="width: 30%">

							</colgroup>
							<caption>Список сотрудников</caption>
							<thead>
								<tr>
									<th>№</th>
									<th>Фамилия</th>
									<th>Имя</th>
									<th>Должность</th>
									<th></th>
									<th></th>

								</tr>
							</thead>
							<tbody></tbody>


						</table>

						<table class="staff-table table-menu" cellspacing='0'>
							<colgroup>
								<col style="width: 20%">
								<col style="width: 30%">
								<col style="width: 20%">
								<col style="width: 30%">

							</colgroup>
							<caption>Меню</caption>
							<thead>
								<tr>
									<th>№</th>
									<th>Название</th>
									<th>Цена</th>
									<th></th>

								</tr>
							</thead>
							<tbody></tbody>


						</table>

						<table class="staff-table table-clients" cellspacing='0'>
							<colgroup>
								<col style="width: 20%">
								<col style="width: 30%">
								<col style="width: 20%">
								<col style="width: 30%">
								<col style="width: 30%">

							</colgroup>
							<caption>Меню</caption>
							<thead>
								<tr>
									<th>№</th>
									<th>Логин</th>
									<th>Email</th>
									<th></th>
									<th></th>

								</tr>
							</thead>
							<tbody></tbody>


						</table>

						<div class="center_pagination">
							<ul class="pagination">
							</ul>

						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<c:import url="/staff/fragment/staffFooter.jsp" />
	</div>
	<div class="popup-message">
		<div class="popup">
			<div class="popup_row">
				<label for="name" class="popup_label message_title">Уведомление:</label>
				<p id="message"></p>
			</div>
			<div class="popup_row">
				<button type="submit" class="popup_submit close_message">OK</button>
			</div>
			<a href="#" class="close_message close_button">X</a>
		</div>
	</div>

	<div class="popup-form-by-id">
		<div class="popup">
			<div class="popup_row">
				<label for="name" class="popup_label message_title"></label>
			</div>

			<form id="smth" action="/restraunt/controller">
				<div class="popup_row">
					<label class="popup_label">Введите id:</label> <input name="id"
						type="number" placeholder="Введите id" min="0" class="popup_input" />
					<input name="action" type="text" value="" hidden="true" />
				</div>

				<div class="popup_row">
					<button type="submit" class="popup_submit">Поиск</button>
				</div>
			</form>
			<a href="#" class="popup_close close-form-by-id">X</a>
		</div>
	</div>


	<div class="popup-form-by-name">
		<div class="popup">
			<div class="popup_row">
				<label for="name" class="popup_label message_title"></label>
			</div>

			<form id="smth" action="/restraunt/controller">
				<div class="popup_row">
					<label class="popup_label">Введите параметр:</label> <input
						name="name" type="text" placeholder="Введите" class="popup_input" />
					<input name="action" type="text" value="" hidden="true" />
				</div>

				<div class="popup_row">
					<button type="submit" class="popup_submit">Поиск</button>
				</div>
			</form>
			<a href="#" class="popup_close close-form-by-name">X</a>
		</div>
	</div>

	<div class="popup-form-change">
		<div class="popup">
			<div class="popup_row">
				<label for="name" class="message_title">Изменить</label>
			</div>

			<form id="change" action="/restraunt/controller">
				<div class="popup_row">
					<label class="popup_label">Выберите новое состояние:</label> <select
						size="6" multiple name="state">
						<option value="awaiting">В ожидании</option>
						<option value="processing_cooking">Готовится</option>
						<option value="awaiting_delivery">В ожидании доставки</option>
						<option value="processing_delivery">В процессе доставки</option>
						<option value="done">Заказ обслужен</option>
						<option value="cancelled">Заказ отменен</option>
					</select>
					<p></p>
					<input name="action" type="text" value="update_order_state" hidden="true" />
					<input type="text" name="idOrder" value="" hidden="true">
				
				</div>

				<div class="popup_row">
					<button type="submit" class="popup_submit">Установить</button>
				</div>
			</form>
			<a href="#" class="popup_close" id="close-change-form">X</a>
		</div>
	</div>
	
	<div class="popup-staff-register">
		<div class="popup">
			<form id="staff-register" enctype="multipart/form-data"
				action="/restraunt/controller" method="post">
				<input type="text" name="action" value="register_staff"
					hidden="true">
				<div class="popup_row">
					<p class="popup_label message_title">Введите регистрационные
						данные :</p>
				</div>
				<p>
					<input type="text" name="name" value="" class="popup_input"
						placeholder="Введите имя" required>
				</p>
				<p>
					<input type="text" name="surname" value="" class="popup_input"
						placeholder="Введите фамилию" required>
				</p>
				<p>
					<input type="text" name="post" value="" class="popup_input"
						placeholder="Введите должность" required>
				</p>
				<p>
					<input type="text" name="login" value="" class="popup_input"
						placeholder="Введите логин" required>
				</p>
				<p>
					<input type="text" name="password" value="" class="popup_input"
						placeholder="Введите пароль" required>
				</p>
				<p>
					<input type="text" name="telephone" value="" class="popup_input"
						placeholder="Введите телефон" required>
				</p>
				<p>
					<input type="text" name="email" value="" class="popup_input"
						placeholder="Введите email" required>
				</p>
				<p>
					<input type="text" name="address" value="" class="popup_input"
						placeholder="Введите адрес" required>
				</p>
				<p style="text-align: center;">
					<input type="radio" id="admin_off" name="isAdmin" value="false"
						class="admin-checkbox" checked=""> <label for="admin_off"
						class="register-label">Смертный</label> <input type="radio"
						id="admin_on" name="isAdmin" value="true" class="admin-checkbox">
					<label for="admin_on" class="register-label">Админ</label>
				</p>
				<p>
					<b>Фото</b> <input type="file" name="photo" required>
				</p>

				<div class="popup_row">
					<p></p>
					<button type="submit" class="popup_submit">Зарегистрировать</button>
				</div>
				<a href="#" class="close_button close-staff-register">X</a>
			</form>
		</div>
	</div>

	<div class="popup-add-item">
		<div class="popup">
			<form id="add-item" action="/restraunt/controller" method="post">
				<input type="text" name="action" value="" hidden="true">
				<div class="popup_row">
					<p class="popup_label message_title" id="add_item_msg">:</p>
				</div>
				<p>
					<input type="text" name="name" value="" class="popup_input"
						placeholder="Название" required>
				</p>
				<p>
					<input type="number" name="price" value="" min="0"
						class="popup_input" placeholder="Цена" style="width: 60%;"
						required> <b>руб</b>
				</p>

				<div id="additional_dish">
					<p>
						<input type="number" name="timeToCook" value="" min="0"
							class="popup_input" placeholder="Время приг-я"
							style="width: 60%;"><b>мин</b>
					</p>
					<p>
						<input type="text" name="consists" value="" class="popup_input"
							placeholder="Состав">
					</p>

					<p>
						<b>Картинка</b> <input type="file" name="picture">
					</p>

				</div>

				<div id="additional_drink">

					<p>
						<input type="number" name="volume" value="" min="0"
							class="popup_input" placeholder="Объем" style="width: 60%;"><b>мл</b>

					</p>

					<p style="text-align: center;">
						<input type="radio" id="alco_off" name="isAlcoholic" value="false"
							class="alco-checkbox" checked=""> <label for="alco_off"
							class="alco-label">Безалк</label> <input type="radio"
							id="alco_on" name="isAlcoholic" value="true"
							class="alco-checkbox"> <label for="alco_on"
							class="alco-label">Алк</label>
					</p>

				</div>


				<div class="popup_row">
					<p></p>
					<button type="submit" class="popup_submit">Добавить</button>
				</div>
				<a href="#" class="close_button close-add-item">X</a>
			</form>
		</div>
	</div>


	<div class="popup-order">
		<div class="popup">
			<div class="popup_row">
				<label for="name" class="popup_label message_title"></label>
			</div>

			<div class="left-consist">
			
				<h1 class="message_title">Состав</h1>
				<table class="consist_table">
					<thead>
						<tr>
							<th>Товар</th>
							<th>Количество</th>
						</tr>
					</thead>
					<tbody id="consist_items">
					</tbody>
				</table>
				<div class=" delivery-container"></div>
			</div>

			<div class="right-changes">
				<h1 class="message_title">
					Текущее состояние: <span id="current_state"></span>
				</h1>
				<h1 class="message_title">
					Итоговая цена: <span id="total_cost"></span>
				</h1>
				<h1 class="message_title">
					Id клиента: <span id="client_id"></span>
				</h1>
				<p></p>
				<div class="list_changes">
					<h1 class="message_title">История :</h1>
					<ul>
					</ul>
				</div>
			</div>
			<div class="bottom-button">
				<button class="popup_submit close_items left">Отмена</button>
				<form id="update-order" action="/restraunt/controller">
					<input type="text" name="action" value="update_order_state"
						hidden="true"> <input type="text" name="idOrder" value=""
						hidden="true"> <input type="text" name="state" value=""
						hidden="true">
					<button type="submit" class="popup_submit right"></button>
				</form>
				<c:if test="${sessionScope.staff.admin}">
					<button class="popup_submit change_button" id="change_button">Изменить</button>
				</c:if>
			</div>
			<a href="#" class="close_items close_button">X</a>
		</div>
	</div>

	<script src="/restraunt/js/vendors/jquery-1.9.1.min.js"></script>
	<script src="/restraunt/js/index.js"></script>
</body>
</html>