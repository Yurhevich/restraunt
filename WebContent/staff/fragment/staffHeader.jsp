<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
	<header class="staff_header">
	<div class="staff_header_container">
		<div class="logo">
			<a href="/restraunt/staff/staffMain.jsp"><img
				src="/restraunt/img/logo.png" alt="логотип"></a>
		</div>
		<ul class="local">
			<li class="local__item"><a href="#" class="local__link"> <i
					class="icon-rus"></i>РУС
			</a></li>
			<li class="local__item"><a href="#" class="local__link"> <i
					class="icon-eng"></i>ENG
			</a></li>
		</ul>
		<h1 class="restaurant-name">Food♥Lover</h1>
		<div class="staff_authorization autorization-out">
			<div class="staff_authorization__title">Личный кабинет</div>


			<div class="authorization__login">
				<a href="/restraunt/controller?action=view_staff_info&idStaff=${sessionScope.staff.id}" class="user-name"> <i
					class="icon-user"></i>${sessionScope.staff.name} ${sessionScope.staff.surname} 
				</a>

			</div>
			<div class="authorization__login">
				<i class="post"> ${sessionScope.staff.post} </i>
			
			</div>
			<div class="authorization__login">
				<a href="/restraunt/controller?action=logout" class="login"> <i
					class="icon-key"></i>Выйти
				</a>
			</div>



		</div>
	</div>
	</header>
</body>
</html>