<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Информация об элементе меню</title>
<link rel="stylesheet" href="/restraunt/css/main.css" />
</head>
<body>
	<div class="wrapper">
		<c:import url="/staff/fragment/staffHeader.jsp" />
		<div class="content">
			<div class="main-content">
				<h2 class="content-title">Информация об элементе меню</h2>

				<div class="account__container ">

					<div class="accinfo_menu">
						<div class="staff_accinfo_sidebar">
							<ul>
								<li><a class="account_button"
									href="/restraunt/staff/staffMain.jsp">Назад к главной</a></li>
								<c:if test="${sessionScope.staff.admin}">
									<li><button type="submit" class="account_button"
											id="change_item">Изменить</button></li>
									<li><form class="menu_item_action"
											action="/restraunt/controller">
											<input type="text" value="delete_from_menu" hidden="true"
												name="action" /> <input type="text" value="${item.id}"
												hidden="true" name="idItem" />
											<button type="submit" class="account_button" id="delete_item">Удалить
												из меню</button>
										</form></li>
								</c:if>
							</ul>
						</div>
					</div>
					<c:if test="${type == 'dish'}">
						<div class="staff_photo">
							<div class="staff-photo-wrapper">
								<img src="${item.picture}" alt="" />
							</div>
						</div>
					</c:if>
					<div class="accinfo_content">
						<ul class="account__list">
							<li class="account__item"><span class="account__key">Id:
							</span><span class="account__value">${item.id}</span></li>
							<li class="account__item"><span class="account__key">Название:
							</span><span class="account__value">${item.name}</span></li>
							<li class="account__item"><span class="account__key">Цена:
							</span><span class="account__value">${item.price} руб</span></li>
							<li class="account__item"><span class="account__key">Скидка:
							</span><span class="account__value">${item.discount}%</span></li>
							<c:if test="${type == 'dish'}">
								<li class="account__item"><span class="account__key">Время
										приготовления: </span><span class="account__value">${item.timeToCook}
										мин</span></li>
								<li class="account__item"><span class="account__key">Состав:
								</span><span class="account__value">${item.consists}</span></li>
							</c:if>
							<c:if test="${type == 'drink'}">
								<li class="account__item"><span class="account__key">Объем:
								</span><span class="account__value">${item.volume} мл</span></li>
								<li class="account__item"><span class="account__key">Алкогольный:
								</span><span class="account__value">${item.alcoholic}</span></li>
							</c:if>
						</ul>

					</div>

				</div>
			</div>
		</div>
		<c:import url="/staff/fragment/staffFooter.jsp" />

	</div>


	<div class="popup-message">
		<div class="popup">
			<div class="popup_row">
				<label for="name" class="popup_label message_title">Заказ:</label>
				<p id="message"></p>
			</div>
			<div class="popup_row">
				<button type="submit" class="popup_submit close_message">OK</button>
			</div>
			<a href="#" class="close_message close_button">X</a>
		</div>
	</div>

	<div class="popup-change-item">
		<div class="popup">
			<form id="change-item-form" action="/restraunt/controller"
				method="post">
				<c:if test="${type == 'dish'}">
					<input type="text" name="action" value="change_dish" hidden="true">
				</c:if>
				<c:if test="${type == 'drink'}">
					<input type="text" name="action" value="change_drink" hidden="true">
				</c:if>
				<input type="text" name="id" value="${item.id}" hidden="true">
				<div class="popup_row">
					<p class="popup_label message_title" id="add_item_msg">Изменить
						параметры</p>
				</div>
				<p>
					<input type="text" name="name" value="${item.name}"
						class="popup_input" placeholder="Название" required>
				</p>
				<p>
					<input type="number" name="price" value="${item.price}" min="0"
						class="popup_input" placeholder="Цена" style="width: 60%;"
						required> <b>руб</b>
				</p>

				<c:if test="${type == 'dish'}">
					<p>
						<input type="number" name="timeToCook" value="${item.timeToCook}"
							min="0" class="popup_input" placeholder="Время приг-я"
							style="width: 60%;"><b> мин</b>
					</p>
					<p>
						<input type="text" name="consists" value="${item.consists}"
							class="popup_input" placeholder="Состав">
					</p>
				</c:if>
				<c:if test="${type == 'drink'}">
					<p>
						<input type="number" name="volume" value="${item.volume}" min="0"
							class="popup_input" placeholder="Объем" style="width: 60%;"><b>
							мл</b>

					</p>


					<p style="text-align: center;">
						<input type="radio" id="alco_off" name="isAlcoholic" value="false"
							class="alco-checkbox" checked=""> <label for="alco_off"
							class="alco-label">Безалк</label> <input type="radio"
							id="alco_on" name="isAlcoholic" value="true"
							class="alco-checkbox"> <label for="alco_on"
							class="alco-label">Алк</label>
					</p>

				</c:if>
				<div class="popup_row">
					<p></p>
					<button type="submit" class="popup_submit">Изменить</button>
				</div>
				<a href="#" class="close_button" id="close-change-item">X</a>
			</form>
		</div>
	</div>


	<div class="popup-item-delete">
		<div class="popup">
			<form id="item-delete-form" action="/restraunt/controller"
				method="post">
				<input type="text" name="action" value="remove_item" hidden="true">
				<input type="text" name="id" value="${item.id}" hidden="true">
				<input type="text" name="type" value="${type}" hidden="true">
				<div class="popup_row">
					<p class="popup_label message_title">Подтверждение :</p>
				</div>

				<div class="popup_row">
					<p class="popup_label">Вы уверены что хотите удалить продукт?</p>
				</div>

				<div class="popup_row">
					<p></p>
					<button type="submit" class="popup_submit">Удалить</button>
				</div>
				<a href="#" class="close_button" id="close_delete_item">X</a>
			</form>
		</div>
	</div>
	<script src="/restraunt/js/vendors/jquery-1.9.1.min.js"></script>
	<script src="/restraunt/js/index.js"></script>
</body>
</html>