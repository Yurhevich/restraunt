<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="resources.localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="userMenu.nav.about" var="about"/>
<fmt:message bundle="${loc}" key="userMenu.nav.menu" var="menu"/>
<fmt:message bundle="${loc}" key="userMenu.nav.contacts" var="contacts"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
</head>
<body>
	<nav class="main-nav">
	<div class="container">
		<ul class="main-nav__list">
			<li class="main-nav__item  "><a href="/restraunt/common/main.jsp"
				class="main-nav__link ">${about}</a></li>
			<li class="main-nav__item "><a
				href="/restraunt/controller?action=view_menu" class="main-nav__link ">${menu}</a></li>
			<li class="main-nav__item " a><a href="/restraunt/common/contacts.jsp"
				class="main-nav__link">${contacts}</a></li>
		</ul>
	</div>
	</nav>
</body>
</html>