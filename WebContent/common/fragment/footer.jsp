<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="resources.localization.local" var="loc" />
<fmt:message bundle="${loc}" key="footer.copyright" var="copyright" />
<fmt:message bundle="${loc}" key="footer.staff.authorization" var="staff_aut" />
<fmt:message bundle="${loc}" key="footer.staff.message" var="staff_mes" />
<fmt:message bundle="${loc}" key="footer.staff.authorization.login" var="staff_login" />
<fmt:message bundle="${loc}" key="footer.staff.authorization.password" var="staff_pass" />
<fmt:message bundle="${loc}" key="footer.staff.authorization.title" var="aut_title" />
<fmt:message bundle="${loc}" key="footer.staff.message.title" var="msg_title" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
</head>

<body>
	<footer class="footer">
	<div class="footer__container">
		<c:choose>
			<c:when test="${(sessionScope.client != null)}">
				<span class="copyright">${copyright}</span>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${(sessionScope.staff != null)}">
						<a href="/restraunt/staff/staffMain.jsp" class="login"> <i
							class="icon-key"></i>${staff_aut} <span>${staff_mes}</span></a>
						<span class="copyright">${copyright} </span>
					</c:when>
					<c:otherwise>
						<a href="#" id="showPopupPersonal" class="login"> <i
							class="icon-key"></i>${staff_aut} <span>${staff_mes}</span></a>
						<span class="copyright">${copyright} </span>
					</c:otherwise>
				</c:choose>

			</c:otherwise>
		</c:choose>
	</div>
	</footer>

	<div class="popup-overlay">
		<div class="popup popup_personal">
			<div class="popup_row">
				<p class=" message_title">${aut_title}:</p>
			</div>
			<form class="popup_form" action="/restraunt/controller" method="post">
				<input type="hidden" name="action" value="login_staff" />
				<p id="staff_message" class="login-msg popup_row"></p>
				<div class="popup_row">
					<label class="popup_label">${staff_login}:</label> <input
						name="login" type="text" class="popup_input" />
				</div>
				<div class="popup_row">
					<label class="popup_label">${staff_pass}:</label> <input
						name="password" type="password" class="popup_input" />
				</div>
				<div class="popup_row">
					<button type="submit" class="popup_submit">${staff_aut}</button>
				</div>
			</form>
			<a href="#" class="popup_close">X</a>
		</div>
	</div>
	<div class="popup-message">
		<div class="popup">
			<div class="popup_row">
				<label for="name" class="popup_label message_title">${msg_title}:</label>
				<p id="message"></p>
			</div>
			<div class="popup_row">
				<button type="submit" class="popup_submit close_message">OK</button>
			</div>
			<a href="#" class="close_message close_button">X</a>
		</div>
	</div>
	
	
</body>
</html>