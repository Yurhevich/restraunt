<%@ page language="java" contentType="text/html; charset=utf-8 "
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="resources.localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="userHeader.authorization.title" var="title"/>
<fmt:message bundle="${loc}" key="userHeader.authorization.logout" var="logout"/>
<fmt:message bundle="${loc}" key="userHeader.authorization.cart" var="cart"/>
<fmt:message bundle="${loc}" key="userHeader.authorization.login" var="login"/>
<fmt:message bundle="${loc}" key="userHeader.authorization.registrate" var="registrate"/>
<fmt:message bundle="${loc}" key="userHeader.local.ru_button" var="ru_button"/>
<fmt:message bundle="${loc}" key="userHeader.local.en_button" var="en_button"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
</head>
<body>
	<header class="header">
	<div class="header__container">
		<div class="logo">
			<a href="/restraunt/common/main.jsp"><img
				src="/restraunt/img/logo.png" alt="логотип" /></a>
		</div>
		<ul class="local">
			<li class="local__item"><a href="/restraunt/controller?action=local&locale=ru" class="local__link"> <i
					class="icon-rus"></i>${ru_button}
			</a></li>
			<li class="local__item"><a href="/restraunt/controller?action=local&locale=en" class="local__link"> <i
					class="icon-eng"></i>${en_button}
			</a></li>
		</ul>
		<h1 class="restaurant-name">Food&#x2665Lover</h1>

		<c:choose>
			<c:when test="${sessionScope.client != null}">
				<div class="atorization autorization-out">
					<div class="autorization__title">${title}</div>
					<div class="atorization__login">
						<a href="/restraunt/user/accinfo.jsp" class="user-name"> <i
							class="icon-user"></i>${sessionScope.client.login}</a><a
							href="/restraunt/controller?action=logout" class="login"> <i
							class="icon-key"></i>${logout}
						</a>
					</div>
					<a href="/restraunt/controller?action=order" class="mybasket">
						<i class="icon-basket"></i>${cart}
					</a>
				</div>

			</c:when>
			<c:when test="${sessionScope.staff != null}">
			</c:when>

			<c:otherwise>
				<div class="atorization autorization-out">
					<div class="autorization__title">${title}</div>
					<div class="atorization__login">
						<a href="/restraunt/common/reg/login.jsp" id="autorizationOut"
							class="login"> <i class="icon-key"></i>${login}
						</a>
					</div>
					<div class="atorization__login">
						<a href="/restraunt/common/reg/registration.jsp"
							id="autorizationOut" class="login"> <i class="icon-key"></i>${registrate}
						</a>
					</div>
				</div>
			</c:otherwise>
		</c:choose>

	</div>
	</header>
</body>
</html>