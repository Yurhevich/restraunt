<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="resources.localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="main.title.first" var="first_title"/>
<fmt:message bundle="${loc}" key="main.title.second" var="second_title"/>
<fmt:message bundle="${loc}" key="main.artice.first" var="first_art"/>
<fmt:message bundle="${loc}" key="main.artice.second" var="second_art"/>
<fmt:message bundle="${loc}" key="main.artice.third" var="third_art"/>
<fmt:message bundle="${loc}" key="main.page.title" var="page_title"/>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<link rel="stylesheet" href="/restraunt/css/main.css" />
<title>${page_title}</title>
</head>
<body>
	<div class="wrapper">
		<c:import url="/common/fragment/userHeader.jsp" />
		<div class="content">
			<c:import url="/common/fragment/userMenu.jsp" />
			<div class="main-content">
				<h2 class="content-title">${first_title}</h2>
				<p class="info">${first_art}</p>
				<div id="slider1_container"
					style="position: relative; top: 0px; left: 0px; width: 600px; height: 300px;"
					class="slider-container">
					<div u="slides" class="slides">
						<div>
							<img src="/restraunt/img/cafe_1.jpg" u="image" />
						</div>
						<div>
							<img src="/restraunt/img/cafe_2.jpg" u="image" />
						</div>
						<div>
							<img src="/restraunt/img/cafe_3.jpg" u="image" />
						</div>
						<div>
							<img src="/restraunt/img/cafe_4.jpg" u="image" />
						</div>
					</div>
				</div>
				<p class="info">${second_art}</p>
				<h3 class="content-title">${second_title}</h3>
				<p class="info">${third_art}</p>
			</div>
		</div>
		<c:import url="/common/fragment/footer.jsp" />
	</div>

	<script src="/restraunt/js/vendors/jquery-1.9.1.min.js"></script>
	<script src="/restraunt/js/vendors/jssor.slider.mini.js"></script>
	<script src="/restraunt/js/main.js"></script>
</body>
</html>