<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="resources.localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="menu.title" var="title"/>
<fmt:message bundle="${loc}" key="menu.content.title.dish" var="dish_title"/>
<fmt:message bundle="${loc}" key="menu.content.title.drink" var="drink_title"/>
<fmt:message bundle="${loc}" key="menu.dish.consist" var="dish_consists"/>
<fmt:message bundle="${loc}" key="menu.dish.time" var="dish_time"/>
<fmt:message bundle="${loc}" key="menu.dish.price" var="price"/>
<fmt:message bundle="${loc}" key="menu.drink.name" var="drink_name"/>
<fmt:message bundle="${loc}" key="menu.drink.volume" var="drink_volume"/>
<fmt:message bundle="${loc}" key="menu.drink.alcoholic" var="drink_alc"/>
<fmt:message bundle="${loc}" key="menu.drink.ml" var="drink_ml"/>
<fmt:message bundle="${loc}" key="menu.yes" var="yes"/>
<fmt:message bundle="${loc}" key="menu.no" var="no"/>
<fmt:message bundle="${loc}" key="menu.order" var="order"/>
<fmt:message bundle="${loc}" key="menu.order.success" var="success"/>
<fmt:message bundle="${loc}" key="menu.order.need_login" var="not_aut"/>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<link rel="stylesheet" href="css/main.css" />
<title>${title}</title>
</head>
<body>
	<div class="wrapper">
		<c:import url="/common/fragment/userHeader.jsp" />
		<div class="content">
			<c:import url="/common/fragment/userMenu.jsp" />
			<div class="main-content">
				<h2 class="content-title">${title}:</h2>
				<h2 class="ribbon">${dish_title}</h2>
				<ul class="products__list">
					<c:forEach var="elem" items="${dishes}">

						<li class="products__item">

							<form class="add-item" action="/restraunt/controller">
								<input type="text" value="add_to_cart" hidden="true"
									name="action" /> <input type="text" value="${elem.id}"
									hidden="true" name="itemId" />
								<div class="img-wrapper">
									<img src="${elem.picture}" alt="" />
								</div>
								<span class="products__title">${elem.name}</span>
								<p class="progucts__description">${dish_consists} : ${elem.consists}</p>
								<p class="progucts__description">${dish_time} :
									${elem.timeToCook}</p>
								<p class="progucts__description">${price} : ${elem.price}</p>

								<button class="products__buy" type="submit">${order}</button>
							</form>
						</li>

					</c:forEach>
				</ul>
				<h2 class="ribbon">${drink_title}</h2>
				<table class="drink" cellspacing='0'>
				<tr>
					<th>${drink_name}</th>
					<th>${drink_volume}</th>
					<th>${drink_alc}</th>
					<th>${price}</th>
					<th></th>
				</tr>

				<c:forEach var="elem" items="${drink}">
					<form class="add-item" action="/restraunt/controller">
						<input type="text" value="add_to_cart" hidden="true" name="action" />
						<input type="text" value="${elem.id}" hidden="true" name="itemId" />
						<tr>
							<td class="col_0">${elem.name}</td>
							<td class="col_1">${elem.volume} ${drink_ml}</td>
							<td class="col_2"><c:choose><c:when test="${elem.alcoholic}">${yes}</c:when><c:otherwise>${no}</c:otherwise></c:choose></td>
							<td class="col_3">${elem.price}</td>
							<td class="col_4"><button class="products__buy" type="submit">${order}</button></td>
						</tr>

					</form>
				</c:forEach>
			</table>
				
			</div>

		</div>

		<c:import url="/common/fragment/footer.jsp" />
	</div>
	<script src="js/vendors/jquery-1.9.1.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>