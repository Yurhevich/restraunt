<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="resources.localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="contacts.page.title" var="page_title"/>
<fmt:message bundle="${loc}" key="contacts.title" var="title"/>
<fmt:message bundle="${loc}" key="contacts.phone" var="phone"/>
<fmt:message bundle="${loc}" key="contacts.vk" var="vk"/>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/restraunt/css/main.css" />
<title>${page_title}</title>
</head>
<body>
	<div class="wrapper">
		<c:import url="/common/fragment/userHeader.jsp" />
		<div class="content">
			<c:import url="/common/fragment/userMenu.jsp" />
			<div class="main-content">
				<h2 class="content-title">${title}</h2>
				<div class="contacts__container">
					<div class="phonetalk-wrapper">
						<img src="/restraunt/img/phonetalk.png" alt="Деньги звонят" />
					</div>
					<ul class="contacts__list">
						<li class="contacts__item"><span class="contacts__title">${phone}:</span><a href="tel:+375298709313" class="contacts__link">+375(29)
								870-93-13</a></li>
						<li class="contacts__item"><span class="contacts__title">${vk}:
						</span><a href="http://vk.com/id126778776" class="contacts__link">vk.com/id126778776</a></li>
						<li class="contacts__item"><span class="contacts__title">Skype:
						</span><a href="skype:lubospodkidos" class="contacts__link">lubospodkidos</a></li>
					</ul>
				</div>
			</div>
		</div>
		<c:import url="/common/fragment/footer.jsp" />
	</div>

	
	<script src="/restraunt/js/vendors/jquery-1.9.1.min.js"></script>
	<script src="/restraunt/js/main.js"></script>
	
</body>
</html>