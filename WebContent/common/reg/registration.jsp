<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="resources.localization.local" var="loc" />
<fmt:message bundle="${loc}" key="registration.title" var="title" />
<fmt:message bundle="${loc}" key="registration.form.name" var="name" />
<fmt:message bundle="${loc}" key="registration.form.surname" var="surname" />
<fmt:message bundle="${loc}" key="registration.form.username" var="username" />
<fmt:message bundle="${loc}" key="registration.form.email" var="email" />
<fmt:message bundle="${loc}" key="registration.form.password" var="password" />
<fmt:message bundle="${loc}" key="registration.submit" var="submit" />
<fmt:message bundle="${loc}" key="registration.message.exist" var="exist" />
<fmt:message bundle="${loc}" key="registration.message.valid" var="not_valid" />
<fmt:message bundle="${loc}" key="registration.login" var="log_in" />

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="/restraunt/css/main.css" media="screen"
	type="text/css" />
<title>${title}</title>	
</head>

<body>

	<div id="login">
		<div class="flip">

			<div class="form-signup">
				<h1>${title}</h1>
				<fieldset>
					<p class="login-msg"></p>
					<form action="/restraunt/controller" method="post">
						<input type="text" name="name" placeholder="${name}" required />
						<input type="text" name="surname" placeholder="${surname}"
							required /> <input type="text" name="login"
							placeholder="${username}" required /> <input type="email"
							name="email" placeholder="${email}" required />
						<input type="password" name="password"
							placeholder="${password}" required />

						<c:if test="${message != null}">
							<p class="login-msg">${message}</p>
						</c:if>

						<input type="submit" name="action" value="Registration"
							placeholder="${submit}" />



					</form>
					<a href="/restraunt/common/reg/login.jsp" class="flipper">${log_in}</a>
				</fieldset>
			</div>
		</div>
	</div>


</body>
</html>