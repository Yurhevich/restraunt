<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="resources.localization.local" var="loc" />
<fmt:message bundle="${loc}" key="login.page.title" var="page_title" />
<fmt:message bundle="${loc}" key="login.title" var="title" />
<fmt:message bundle="${loc}" key="login.authorization.login" var="login" />
<fmt:message bundle="${loc}" key="login.authorization.password" var="pass" />
<fmt:message bundle="${loc}" key="login.submit" var="submit" />
<fmt:message bundle="${loc}" key="login.message.registration" var="reg" />
<fmt:message bundle="${loc}" key="login.message.fail" var="fail" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>${page_title}</title>
<link rel="stylesheet" href="/restraunt/css/main.css" media="screen"
	type="text/css" />
</head>
<body>

	<div id="login">
		<div class="flip">

			<div class="form-login">
				<h1>${title}</h1>
				<fieldset>
					<form action="/restraunt/controller" method="post">
						<input type="text" name="login" placeholder="${login}" required />
						<input type="password" name="password" placeholder="${pass}"
							required />
						<c:if test="${message != null}">
							<p class="login-msg">${fail}</p>
						</c:if>
						<input type="submit" name="action" value="Login"
							placeholder="${submit}" />
					</form>
					<p>
						<a href="/restraunt/common/reg/registration.jsp" class="flipper">${reg}</a><br>
				</fieldset>
			</div>

		</div>
	</div>

</body>
</html>