var message = (function() {

    var init = function() {
        _setUpListners();
    };

    var _setUpListners = function() {
        $(".close_message").on("click", function(e) {
            e.preventDefault();
            $(".popup-message").hide();
        });
    };

    return {
        init: init
    }
})();

function showMessage(data) {
    $("#message").html(data);
    $(".popup-message").toggle();
};

$("ul.pagination").on('click', 'a.page', function(e) {
    e.preventDefault();

    values = $("#refresh").serialize();


    pageNum = $(this).text();

    values += "&page=" + pageNum;

    executeUpdate(values);

    $("ul.pagination a").removeClass('active');

    $(this).addClass('active');
});


$("ul.pagination").on('click', 'a.switch-pages', function(e) {
    e.preventDefault();

    txt = $(this).text();
    numberAllPages = $("ul.pagination a.page").length;

    if (txt == "»") {
        lastVisiblePage = $("ul.pagination a.page:visible").last();

        if (lastVisiblePage.text() == numberAllPages) {
            lastVisiblePage.trigger("click");
        } else {
            $("ul.pagination a.page:visible").hide();
            number = parseInt(lastVisiblePage.text());
            if (number + 3 >= numberAllPages) {
                for (var i = number + 1, l = numberAllPages; i <= l; i++) {
                    $("#page_" + i).show();
                }
            } else {
                for (var i = number + 1, l = number + 3; i <= l; i++) {
                    $("#page_" + i).show();
                }
            }
            $("ul.pagination a.page:visible").first().trigger("click");
        }
    } else {
        firstVisiblePage = $("ul.pagination a.page:visible").first();
        if (firstVisiblePage.text() == 1) {
            firstVisiblePage.trigger("click");
        } else {
            $("ul.pagination a.page:visible").hide();
            for (var i = parseInt(firstVisiblePage.text()) - 3, l = firstVisiblePage.text(); i < l; i++) {
                $("#page_" + i).show();
            }
            $("ul.pagination a.page:visible").first().trigger("click");
        }

    }
});

$(document).ready(function() {
    message.init();
    //validate();
    title = $("title").text();
    if (title == "Текущие заказы") {

        dataString = {
            action: "get_orders_by_state",
            state: "awaiting"
        }


        updateOrderTable(dataString);

        $(".staff_title").html(title);
    }
});

function updateOrderTable(dataString) {
    $(".staff-table").hide();
    $(".table-orders").show();

    $.ajax({
        type: "POST",
        url: "/restraunt/controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else {

                var json = $.parseJSON(data);
                $("table.table-orders tbody tr").each(
                    function() {
                        $this = $(this)
                        $this.remove();
                    });

                if (json.count_pages != null) {
                    updatePagination(json.count_pages);

                }


                if (json.orders.length == 0) {
                    $(".table-orders tbody").append('<tr><td colspan="5">Нет результатов.</td></tr>')
                     for (var i = 2 , l = 8; i <= l; i++) {
          			  $(".table-orders tbody").append(
          			            "<tr class='empty_slot'></tr>");
                    }
                } else {

                    	 appendTrToOrderTable(json.orders);

                }

            }

        }
    })
};

function appendTrToOrderTable(orders) {

	  $.each(orders, function(index, value) {
		   datetime = formatDate(value.time);
   id = value.id;
   $(".table-orders tbody").append(
           "<tr class='table-order-item'><form action='/restraunt/controller' method='post'  id='order_" + id + "'> </form> <td>" + id + "</td><td>  " + datetime.date + "</td> <td>" + datetime.time + "</td><td>" + value.totalCost +
           "</td> <td>" + getFullCurrentState(value.state) + "</td></tr>");
       $("#order_" + id).append("<input type='text' value='" + id + "' hidden='true' name='id' />");
       $("#order_" + id).append("<input type='text' value='get_order_info' hidden='true' name='action' />");
  });
	  
	  if (orders.length < 8){
		  for (var i = orders.length+1 , l = 8; i <= l; i++) {
			  $(".table-orders tbody").append(
			            "<tr class='empty_slot'></tr>");
        }
	  }

}

function formatDate(date) {
    var time = Date.parse(date).toString();
    str = time.replace(/\D/g, "");
    d = new Date(parseInt(str));

    year = d.getFullYear();
    month = pad(d.getMonth() + 1);
    day = pad(d.getDate());
    hour = pad(d.getHours());
    minutes = pad(d.getMinutes());

    finalDate = day + "-" + month + "-" + year;
    finalTime = hour + ":" + minutes;
    return dataString = {
        date: finalDate,
        time: finalTime
    }

}

function pad(num) {
    num = "0" + num;
    return num.slice(-2);
}

function updatePagination(count) {
    $("ul.pagination li").each(
        function() {
            $this = $(this)
            $this.remove();
        });

    if (count > 1) {


        for (var i = 1, l = count; i <= l; i++) {
            $("ul.pagination").append('<li><a  href="#" class="page" id="page_' + i + '">' + i + '</a></li>');

        }
        if (count > 3) {
            for (var i = 4, l = count; i <= l; i++) {
                $("#page_" + i).hide();
            }
        }


        $("ul.pagination a").first().addClass('active');

        $("ul.pagination").append('<li><a class="switch-pages" href="#">»</a></li>');
        $("ul.pagination").prepend('<li><a class="switch-pages" href="#">«</a></li>');

    }
}


$(".staff_nav").on('submit', function(e) {
    e.preventDefault();
    title = $(this).find("button[type='submit']").text();
    dataString = $(this).serialize();


    $("#refresh input").remove();


    $(this).find("input").clone().appendTo('#refresh');




    executeUpdate(dataString);


    $("title").html(title);
    $(".staff_title").html(title);

});

function getFullNextState(state) {
    if (state == "awaiting") {
        return "Заказ готовится";
    } else if (state == "processing cooking") {
        return "Заказ приготовлен";
    } else if (state == "awaiting delivery") {
        return "Доставка принята";
    } else if (state == "processing delivery") {
        return "Доставка осуществленна";
    } else if (state == "done") {
        return "Заказ обслужен";
    } else if (state == "cancelled") {
        return "Заказ отменен";
    }
}

function getFullCurrentState(state) {
    if (state == "awaiting") {
        return "В ожидании";
    } else if (state == "processing cooking") {
        return "Готовится";
    } else if (state == "awaiting delivery") {
        return "В ожидании доставки";
    } else if (state == "processing delivery") {
        return "В процессе доставки";
    } else if (state == "done") {
        return "Заказ обслужен";
    } else if (state == "cancelled") {
        return "Заказ отменен";
    }
}

function cleanOrder() {
    $("#consist_items td").each(function() {
        $this = $(this)
        $this.remove();
    });
    $(".list_changes ul li").each(function() {
        $this = $(this);
        $this.remove();
    });
    $("div.delivery-container").empty();
}

function showOrder(dataString) {
    $.ajax({
        type: "POST",
        url: "/restraunt/controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else {
                json = $.parseJSON(data);
                $(".popup-order label.message_title").text("Заказ №" + json.idOrder);
                $("#current_state").text(getFullCurrentState(json.state.current));
                $("#total_cost").text(json.totalCost + ' рублей');
                $('#client_id').text(json.idClient);

                $.each(json.items, function(key, value) {
                    $("#consist_items").append(
                        "<tr> <td>" + key + "</td><td> x " + value + "</td></tr>");
                });


                $(json.changes).each(function(value) {

                    datetime = formatDate(this.timeOfChange);

                    $('<li></li>').appendTo('div.list_changes ul').html('<p> ' + datetime.date + ' ' +
                        datetime.time + ' ' + getFullCurrentState(this.establishedState) +
                        ' by <a href="/restraunt/controller?action=view_staff_info&idStaff=' + this.staff.id + '">' + this.staff.surname + ' ' + this.staff.name +
                        ' (' + this.staff.post + ')</a> </p>');


                });


                if (json.delivery != null) {
                    $("div.delivery-container").append("<h1 class='message_title'>Доставка</h1>");
                    $("div.delivery-container").append("<p class='delivery'>Телефон: " + json.delivery.telephone + "</h1>");
                    $("div.delivery-container").append("<p class='delivery'>Адрес: " + json.delivery.address + "</h1>");
                }


                if (json.state.next == null) {
                    $("#update-order").hide();
                }else{
                	$("#update-order").show();
                }

                    $("#update-order").find("input[name='idOrder']").attr("value", json.idOrder);
                    $("#update-order").find("input[name='state']").attr("value", json.state.next);
                    $("#update-order").find("button[type='submit']").text(getFullNextState(json.state.current));
                    
                    $(".popup-order").show();
            };

        }
    });
}

$("table.table-orders").on('click', "tr.table-order-item", function() {
    values = $(this).find("form").serialize();
    showOrder(values);
});

$("table.table-order-changes").on('click', "tr.table-order-item", function() {
    values = $(this).find("form").serialize();
    showOrder(values);
});



$("#update-order").on('submit', function(e) {
    e.preventDefault();
    id = $(this).find("input[name='idOrder']").attr("value");

    dataString = $(this).serialize();
    $(".popup-order").hide();
    $.ajax({
        type: "POST",
        url: "/restraunt/controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {

            if (data == "failed") {

                showMessage('Сервис недоступен. Попробуйте позже');
                $("#refresh").submit();

            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
                $("#refresh").submit();
            } else if (data == "not_allowed") {

                showMessage('Операция недоступна.Обновите данные');
                $("#refresh").submit();
            } else {
                showMessage("Заказу №" + id + " успешно установлено новое состояние");
                $("#refresh").submit();
            }

        }
    });
    cleanOrder();

});

$("#change").on('submit', function(e) {
    e.preventDefault();
    id = $(this).find("input[name='idOrder']").attr("value");
    dataString = $(this).serialize();
    $(".popup-form-change").hide();
    $.ajax({
        type: "POST",
        url: "/restraunt/controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {

            if (data == "failed") {

                showMessage('Сервис недоступен. Попробуйте позже');

            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
                $("#refresh").submit();
            } else if (data == "not_allowed") {
                showMessage('Операция недоступна.Обновите данные');
                $("#refresh").submit();
            } else {
                showMessage("Заказу №" + id + " успешно установлено новое состояние");
                $("#refresh").submit();
            }

        }
    });
    cleanOrder();

});

$("#refresh").on("submit", function(e) {
    e.preventDefault();
    dataString = $(this).serialize();
    executeUpdate(dataString);
});

function executeUpdate(dataString, action) {

    if (action == null) {
        curAction = $("#refresh").find("input[name='action']").attr("value");
    } else {
        curAction = action;
    }

    if (curAction == "get_orders_by_state") {
        updateOrderTable(dataString);
    } else if (curAction == "get_order_changes") {
        updateOrderChangesTable(dataString);
    } else if (curAction == "get_all_personal") {
        updatePersonalTable(dataString);
    } else if (curAction == "get_staff_by_id") {
        updatePersonalTable(dataString);

    } else if (curAction == "get_staff_by_name") {
        updatePersonalTable(dataString);

    } else if (curAction == "get_order_info") {
        showOrder(dataString);

    } else if (curAction == "get_all_dishes") {
        updateMenuTable(dataString);
    } else if (curAction == "get_all_clients") {
        updateClientTable(dataString);
    } else if (curAction == "get_all_drinks") {
        updateMenuTable(dataString);
    } else if (curAction == "get_client_by_id") {
        updateClientTable(dataString);
    } else if (curAction == "get_client_by_login") {
        updateClientTable(dataString);
    } else {
        updateOrderTable(dataString);
    }


}

$(".close_items").on("click", function(e) {
    e.preventDefault();
    $(".popup-order").hide();
    cleanOrder();

});

$("#order_trigger").on("mouseenter", function(e) {
    e.preventDefault();
    $(".trigger").stop(true, true).slideUp();
    $("#order_action_list").stop(true, true).slideDown();
});


$("#order_action_list").on("mouseleave", function(e) {
    e.preventDefault();
    $("#order_action_list").stop(true, true).slideUp();
});


$("#personal_trigger").on("mouseenter", function(e) {
    e.preventDefault();
    $(".trigger").stop(true, true).slideUp();
    $("#personal_action_list").stop(true, true).slideDown();
});


$("#personal_action_list").on("mouseleave", function(e) {
    e.preventDefault();
    $("#personal_action_list").stop(true, true).slideUp();
});

$("#menu_trigger").on("mouseenter", function(e) {
    e.preventDefault();
    $(".trigger").stop(true, true).slideUp();
    $("#menu_list").stop(true, true).slideDown();
});


$("#menu_list").on("mouseleave", function(e) {
    e.preventDefault();
    $("#menu_list").stop(true, true).slideUp();
});

$("#client_trigger").on("mouseenter", function(e) {
    e.preventDefault();
    $(".trigger").stop(true, true).slideUp();
    $("#client_list").stop(true, true).slideDown();
});


$("#client_list").on("mouseleave", function(e) {
    e.preventDefault();
    $("#client_list").stop(true, true).slideUp();
});

$('#order_by_id').click(function(e) {
    e.preventDefault();
    title = $(this).text();
    showFormGetById(title, "get_order_info");
});

$('#personal_by_id').click(function(e) {
    e.preventDefault();
    title = $(this).text();
    showFormGetById(title, "get_staff_by_id");
});

$('#client_by_id').click(function(e) {
    e.preventDefault();
    title = $(this).text();
    showFormGetById(title, "get_client_by_id");
});

$('#personal_by_name').click(function(e) {
    e.preventDefault();
    title = $(this).text();
    showFormGetByName(title, "get_staff_by_name");
});

$('#client_by_name').click(function(e) {
    e.preventDefault();
    title = $(this).text();
    showFormGetByName(title, "get_client_by_login");
});

$('#add_new_staff').click(function(e) {
    e.preventDefault();
    $(".popup-staff-register").toggle();
});

$('#change_staff').click(function(e) {
    e.preventDefault();
    $(".popup-staff-register").toggle();
});

$('#delete_staff').click(function(e) {
    e.preventDefault();
    $(".popup-staff-delete").show();
});

$('#change_button').click(function(e) {
    e.preventDefault();
    id = $("#update-order input[name='idOrder']").first().attr("value");
    alert(id);
    $("#change input[name='idOrder']").first().attr("value",id);
    $(".popup-order").hide();
    $(".popup-form-change").toggle();
});


$('#change_item').click(function(e) {
    e.preventDefault();
    $(".popup-change-item").toggle();
});

$('#delete_item').click(function(e) {
    e.preventDefault();
    $(".popup-item-delete").toggle();
});

$('#add_dish').click(function(e) {
    e.preventDefault();
    $("#add-item").attr("enctype", "multipart/form-data");
    $("#add-item").find("input[name='action']").attr("value", "add_dish");
    $("#additional_drink").hide();
    $("#additional_dish").show();
    title = $(this).text();
    $("#add_item_msg").text(title);
    $(".popup-add-item").toggle();
});

$('#add_drink').click(function(e) {
    e.preventDefault();
    $("#add-item").removeAttr("enctype");
    $("#add-item").find("input[name='action']").attr("value", "add_drink");
    $("#additional_dish").hide();
    $("#additional_drink").show();
    title = $(this).text();
    $("#add_item_msg").text(title);
    $(".popup-add-item").toggle();
});

function showFormGetById(title, action) {
    $(".popup-form-by-id .message_title").text(title);
    $(".popup-form-by-id form").attr("id", action);
    $(".popup-form-by-id form input[name='action']").attr('value', action);
    $(".popup-form-by-id").toggle();
}

function showFormGetByName(title, action) {
    $(".popup-form-by-name .message_title").text(title);
    $(".popup-form-by-name form").attr("id", action);
    $(".popup-form-by-name form input[name='action']").attr('value', action);
    $(".popup-form-by-name").toggle();
}

$(".close-form-by-id").on("click", function(e) {
    e.preventDefault();
    $(".popup-form-by-id").hide();
});

$("#close_delete_staff").on("click", function(e) {
    e.preventDefault();
    $(".popup-staff-delete").hide();
});

$("#close_delete_item").on("click", function(e) {
    e.preventDefault();
    $(".popup-item-delete").hide();
});

$(".close-form-by-name").on("click", function(e) {
    e.preventDefault();
    $(".popup-form-by-name").hide();
});

$(".close-staff-register").on("click", function(e) {
    e.preventDefault();
    $(".popup-staff-register").hide();
});

$(".close-add-item").on("click", function(e) {
    e.preventDefault();
    $(".popup-add-item").hide();
});

$("#close-change-form").on("click", function(e) {
    e.preventDefault();
    $(".popup-form-change").hide();
});

$("#close-change-item").on("click", function(e) {
    e.preventDefault();
    $(".popup-change-item").hide();
});

$(".popup-form-by-id").on('submit', 'form', function(e) {
    e.preventDefault();
    $(".popup-form-by-id").hide();
    values = $(this).serialize();
    action = $(this).closest('form').find("input[name='action']").val();
    id = $(this).closest('form').find("input[name='id']").val();
    if (action != "get_order_info") {
        $("#refresh input").remove();
        $(this).find("input[name='action']").clone().appendTo('#refresh');
        $('#refresh').append('<input name="id" hidden=""true" type="number" value="' + id + '"/>');
    }
    $(this).closest('form').find("input[name='id']").val("");
    executeUpdate(values, action);
    title = $(".popup-form-by-id").find("label.message_title").text();
    $("title").html(title);
    $(".staff_title").html(title);

})

$(".popup-form-by-name").on('submit', 'form', function(e) {
    e.preventDefault();
    $(".popup-form-by-name").hide();
    values = $(this).serialize();
    action = $(this).closest('form').find("input[name='action']").val();
    name = $(this).closest('form').find("input[name='name']").val();
    $("#refresh input").remove();
    $(this).find("input[name='action']").clone().appendTo('#refresh');
    $('#refresh').append('<input name="name" hidden="true" type="text" value="' + name + '"/>');
    $(this).closest('form').find("input[name='name']").val("");
    executeUpdate(values);
    title = $(".popup-form-by-name").find("label.message_title").text();
    $("title").html(title);
    $(".staff_title").html(title);

})

$("#add-item").on('submit', function(e) {
    e.preventDefault();
    $(".popup-add-item").hide();
    enctype = $(this).attr("enctype");
    if (enctype == "multipart/form-data") {
        var data = new FormData(jQuery($(this))[0]);
        $.ajax({
            type: "POST",
            url: "/restraunt/controller",
            cache: false,
            data: data,
            dataType: "text",
            contentType: false,
            processData: false,
            success: function(data) {;
                processAddingItem(data);
            }
        })
    } else {
        var data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/restraunt/controller",
            cache: false,
            data: data,
            dataType: "text",
            success: function(data) {
                processAddingItem(data);
            }
        });
    }
    $("#add-item")[0].reset();
})



function processAddingItem(data) {

    if (data == "failed") {
        showMessage('Сервис недоступен. Попробуйте позже');

    } else if (data == "need_login") {
        showMessage('Войдите в сиситему чтобы осуществить операцию');
    } else if (data == "not_allowed") {

        showMessage('Операция недоступна для вашего аккаунта.');
    } else {
        showMessage("Операция прошла успешно");
    }

}




$("#staff-register").on('submit', function(e) {
    e.preventDefault();
    //values = $(this).serialize();
    var data = new FormData(jQuery("#staff-register")[0]);
    $.ajax({
        type: "POST",
        url: "/restraunt/controller",
        cache: false,
        data: data,
        dataType: "text",
        contentType: false,
        processData: false,
        success: function(data) {
        	
            $(".popup-staff-register").hide();
            
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');

            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else if (data == "not_allowed") {

                showMessage('Операция недоступна для вашего аккаунта.');
            } else if (data == "already_exist") {

                showMessage('Уже существует.');
            } else {
                showMessage("Сотрудник успешно зарегистрирован");
            }

            $("#staff-register")[0].reset();
        }

    })

})



$("#staff-change-form").on('submit', function(e) {
    e.preventDefault();
    requestString = $(this).serialize();
    $.ajax({
    	 type: "POST",
         url: "/restraunt/controller",
         cache: false,
         data: requestString,
         dataType: "text",
         success: function(data) {
        	 
        	 $(".popup-staff-register").hide();
            
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else if (data == "not_allowed") {
            	
               showMessage('Операция недоступна для вашего аккаунта.');
            } else if (data == "not_valid") {

                showMessage('Неверные параметры для изменения.');
            } else {
                showMessage("Изменения успешно изменены");
            }

            $("#staff-change-form")[0].reset();
        }

    })

})


$("#change-item-form").on('submit', function(e) {
    e.preventDefault();
    requestString = $(this).serialize();
    $.ajax({
    	 type: "POST",
         url: "/restraunt/controller",
         cache: false,
         data: requestString,
         dataType: "text",
         success: function(data) {
        	 
        	 $(".popup-change-item").hide();
            
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else if (data == "not_allowed") {
            	
               showMessage('Операция недоступна для вашего аккаунта.');
            } else if (data == "not_valid") {

                showMessage('Неверные параметры для изменения.');
            } else {
                showMessage("Изменения успешно изменены");
            }

            $("#change-item-form")[0].reset();
        }

    })

})

$("a.staff_menu_button").click (function(e) {
    e.preventDefault();
})



$("#staff-delete-form").on('submit', function(e) {
    e.preventDefault();
    requestString = $(this).serialize();
    $.ajax({
    	 type: "POST",
         url: "/restraunt/controller",
         cache: false,
         data: requestString,
         dataType: "text",
         success: function(data) {
        	 
        	 $(".popup-staff-delete").hide();
            
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else if (data == "not_allowed") {
            	
               showMessage('Операция недоступна для вашего аккаунта.');
            } 
             else {
                showMessage("Аккаунт успешно удален");
            }

        }

    })

})

$("#item-delete-form").on('submit', function(e) {
    e.preventDefault();
    requestString = $(this).serialize();
    $.ajax({
    	 type: "POST",
         url: "/restraunt/controller",
         cache: false,
         data: requestString,
         dataType: "text",
         success: function(data) {
        	 
        	 $(".popup-item-delete").hide();
            
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else if (data == "not_allowed") {
            	
               showMessage('Операция недоступна для вашего аккаунта.');
            } 
             else {
                showMessage("Продукт был успешно удален");
            }

        }

    })

})


function updateOrderChangesTable(dataString) {
    $(".staff-table").hide();
    $(".table-order-changes").show();

    $.ajax({
        type: "POST",
        url: "/restraunt/controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else {

                var json = $.parseJSON(data);
                $("table.table-order-changes tbody tr").each(
                    function() {
                        $this = $(this)
                        $this.remove();
                    });

                if (json.count_pages != null) {
                    updatePagination(json.count_pages);

                }


                if (json.changes.length == 0) {
                    $(".table-order-changes tbody").append('<tr><td colspan="4">Нет результатов.</td></tr>');
                    for (var i = 2 , l = 8; i <= l; i++) {
          			  $(".table-order-changes tbody").append(
          			            "<tr class='empty_slot'></tr>");
                    }
                } else {

                	appendTrToChangesTable(json.changes);
                 

            }

        }
    }})
};

function appendTrToChangesTable(changes) {

	  $.each(changes, function(index, value) {
		   datetime = formatDate(value.timeOfChange);
     id = value.idOrder;
    
    $(".table-order-changes tbody").append(
            "<tr class='table-order-item'><form action='/restraunt/controller' method='post'  id='change_" + index + "'> </form> <td>" + id + "</td><td>  " + datetime.date + "</td> <td>" + datetime.time + "</td>" +
            " <td>" + getFullCurrentState(value.establishedState) + "</td></tr>");
        $("#change_" + index).append("<input type='text' value='" + id + "' hidden='true' name='id' />");
        $("#change_" + index).append("<input type='text' value='get_order_info' hidden='true' name='action' />");
    });
	  
	  if (changes.length < 8){
		  for (var i = changes.length+1 , l = 8; i <= l; i++) {
			  $(".table-order-changes tbody").append(
			            "<tr class='empty_slot'></tr>");
          }
	  }

}


function updateMenuTable(dataString) {

    $(".staff-table").hide();
    $(".table-menu").show();

    $.ajax({
        type: "POST",
        url: "/restraunt/controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else {

                var json = $.parseJSON(data);

                $("table.table-menu tbody tr").each(
                    function() {
                        $this = $(this)
                        $this.remove();
                    });

                if (json.count_pages != null) {
                    updatePagination(json.count_pages);

                }


                if (json.menu_items.length == 0) {
                    $(".table-menu tbody").append('<tr><td colspan="4">Нет результатов.</td></tr>')
                     for (var i = 2 , l = 8; i <= l; i++) {
          			  $(".table-menu tbody").append(
          			            "<tr class='empty_slot' style='height: 83px;'></tr>");
                    }
                } else {

                    appendTrToMenuTable(json.menu_items);

                }



            }
        }
    })
};

function appendTrToMenuTable(items) {

	  $.each(items, function(index, value) {

        $(".table-menu tbody").append(
                '<tr class="table-order-item"> <td>' + value.id + '</td><td>  ' + value.name + '</td> <td>' + value.price + '</td>' + '</td><td></td></tr>');
            $(".table-menu tr:last-child td:last-child").html($('<a class="table-personal-button" href="/restraunt/controller?action=view_menu_item&idItem=' + value.id + ' ">Подробнее</a>'));
	  });
	  
	  if (items.length < 8){
		  for (var i = items.length+1 , l = 8; i <= l; i++) {
			  $(".table-menu tbody").append(
			            "<tr class='empty_slot' style='height: 83px;'></tr>");
        }
	  }

}


function updatePersonalTable(dataString) {

    $(".staff-table").hide();
    $(".table-personal").show();

    $.ajax({
        type: "POST",
        url: "/restraunt/controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else if (data == "not_found") {
                showMessage('Сотрудник с таким id не существует');
            } else {

                var json = $.parseJSON(data);

                $("table.table-personal tbody tr").each(
                    function() {
                        $this = $(this)
                        $this.remove();
                    });

                if (json.count_pages != null) {
                    updatePagination(json.count_pages);

                }

                if (json.list_staff == null) {
                	staff = json.staff;
                	  id = staff.id;
            		  $(".table-personal tbody").append(
            			        '<tr class="table-order-item"> <td>' + id + '</td><td>  ' + staff.surname + '</td> <td>' + staff.name + '</td><td>' + staff.post + '</td><td><form class="personal" action="/restraunt/controller" method="post"  id="personal_1 "> <button type="submit" class="table-personal-button">История</button></form></td><td></td></tr>');
            			    $("#personal_1").append("<input type='text' value='" + id + "' hidden='true' name='idStaff' />");
            			    $("#personal_1").append("<input type='text' value='get_order_changes' hidden='true' name='action' />");
            			    $(".table-personal tr:last-child td:last-child").html($('<a class="table-personal-button" href="/restraunt/controller?action=view_staff_info&idStaff=' + id + ' ">Аккаунт</a>'));
            			    for (var i = 2 , l = 8; i <= l; i++) {
                    			  $(".table-personal tbody").append(
                    			            "<tr class='empty_slot' style='height: 83px;'></tr>");
                              }
            		  
                } else {

                    if (json.list_staff.length == 0) {
                        $(".table-personal tbody").append('<tr><td colspan="3">Нет результатов.</td></tr>')
                         for (var i = 2 , l = 8; i <= l; i++) {
          			  $(".table-personal tbody").append(
          			            "<tr class='empty_slot' style='height: 83px;'></tr>");
                    }
                    } else {
      
                        	 appendTrToPersonalTable(json.list_staff);

                    }
                }



            }
        }
    })
};



function appendTrToPersonalTable(staff) {

	  $.each(staff, function(index, value) {
		  id = value.id;
		  $(".table-personal tbody").append(
			        '<tr class="table-order-item"> <td>' + id + '</td><td>  ' + value.surname + '</td> <td>' + value.name + '</td><td>' + value.post + '</td><td><form class="personal" action="/restraunt/controller" method="post"  id="personal_' + index +
			        '"> <button type="submit" class="table-personal-button">История</button></form></td><td></td></tr>');
			    $("#personal_" + index).append("<input type='text' value='" + id + "' hidden='true' name='idStaff' />");
			    $("#personal_" + index).append("<input type='text' value='get_order_changes' hidden='true' name='action' />");
			    $(".table-personal tr:last-child td:last-child").html($('<a class="table-personal-button" href="/restraunt/controller?action=view_staff_info&idStaff=' + id + ' ">Аккаунт</a>'));
	   
		  });
		  
		  if (staff.length < 8){
			  for (var i = staff.length+1 , l = 8; i <= l; i++) {
				  $(".table-personal tbody").append(
				            "<tr class='empty_slot' style='height: 83px;'></tr>");
	        }
		  }
    
    
}




$(".table-personal").on('submit', 'form', function(e) {
    e.preventDefault();
    dataString = $(this).serialize();

    title = "История";

    $("#refresh input").remove();


    $(this).find("input").clone().appendTo('#refresh');

    updateOrderChangesTable(dataString);

    $("title").html(title);
    $(".staff_title").html(title);

});

function updateClientTable(dataString) {

    $(".staff-table").hide();
    $(".table-clients").show();

    $.ajax({
        type: "POST",
        url: "/restraunt/controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else if (data == "not_found") {
                showMessage('Клиент с таким параметром не существует');
            } else {
                var json = $.parseJSON(data);

                $("table.table-clients tbody tr").each(
                    function() {
                        $this = $(this)
                        $this.remove();
                    });

                if (json.count_pages != null) {
                    updatePagination(json.count_pages);

                }

                if (json.list_clients == null) {
                	value = json.client;
                	id = value.id;
              	    $(".table-clients tbody").append(
              	        '<tr class="table-order-item"> <td>' + id + '</td><td>  ' + value.login + '</td> <td>' + value.email + '</td>' + 
              	        '<td><form class="client" action="/restraunt/controller" method="post"  id="client_1"> <button type="submit" class="table-personal-button">История</button></form></td><td></td></tr>');
              	    $("#client_1").append("<input type='text' value='" + id + "' hidden='true' name='idClient' />");
              	    $("#client_1").append("<input type='text' value='client_order_history' hidden='true' name='action' />");
              	    $(".table-clients tr:last-child td:last-child").html($('<a class="table-personal-button" href="/restraunt/controller?action=view_client_info&idClient=' + id + ' ">Аккаунт</a>'));
                    
              	  for (var i = 2 , l = 8; i <= l; i++) {
        			    $(".table-clients tbody").append(
        			            "<tr class='empty_slot' style='height: 83px;'></tr>");
                  }
                } else {

                    if (json.list_clients.length == 0) {
                        $(".table-clients tbody").append('<tr><td colspan="3">Нет результатов.</td></tr>')
                        for (var i = 2 , l = 8; i <= l; i++) {
          			    $(".table-clients tbody").append(
          			            "<tr class='empty_slot' style='height: 83px;'></tr>");
                    }
                    } else {

                            appendTrToClientTable(json.list_clients);

                    }
                }


            }
        }
    })
};

function appendTrToClientTable(clients) {


  	  $.each(clients, function(index, value) {
  		id = value.id;
  	    $(".table-clients tbody").append(
  	        '<tr class="table-order-item"> <td>' + id + '</td><td>  ' + value.login + '</td> <td>' + value.email + '</td>' + '<td><form class="client" action="/restraunt/controller" method="post"  id="client_' + index +
  	        '"> <button type="submit" class="table-personal-button">История</button></form></td><td></td></tr>');
  	    $("#client_" + index).append("<input type='text' value='" + id + "' hidden='true' name='idClient' />");
  	    $("#client_" + index).append("<input type='text' value='client_order_history' hidden='true' name='action' />");
  	    $(".table-clients tr:last-child td:last-child").html($('<a class="table-personal-button" href="/restraunt/controller?action=view_client_info&idClient=' + id + ' ">Аккаунт</a>'));
        
  	  });
  	  
  	  if (clients.length < 8){
  		  for (var i = clients.length+1 , l = 8; i <= l; i++) {
  			  $(".table-clients tbody").append(
  			            "<tr class='empty_slot' style='height: 83px;'></tr>");
          }
  	  

  }
}

$(".table-clients").on('submit', 'form', function(e) {
    e.preventDefault();
    dataString = $(this).serialize();

    title = "История клиента";

    $("#refresh input").remove();


    $(this).find("input").clone().appendTo('#refresh');

    updateOrderTable(dataString);

    $("title").html(title);
    $(".staff_title").html(title);

});

/*function validate(){
	  $('#staff-register').validate({
	    rules: {},
	    messages: {},
	    submitHandler: function () {
	      return false
	    }
	  });
	  $('input[name="photo"]').rules('add', {
	      required: true,
	      accept: "image/jpeg, image/pjpeg"
	    })
	};
*/