var autorization = (function() {

    var _autorization = function() {
        var hide = document.querySelector(".autorization-out");
        hide.style.display = 'none';
        var show = document.getElementById("autorizationIn");

        show.addEventListener('click', function(e) {
            e.preventDefault();
            this.parentNode.parentNode.style.display = 'none';
            hide.style.display = 'block';
        });
    }

    var init = function() {
        _setUpListners();
    };

    var _setUpListners = function() {
        _autorization();
    };

    return {
        init: init
    }
})();

var popup = (function() {

    var init = function() {
        _setUpListners();
    };

    var _setUpListners = function() {
        $("#showPopupPersonal").on('click', function(e) {
            e.preventDefault();
            $(".popup-overlay").toggle();
        });

        $(".popup_close").on("click", function(e) {
            e.preventDefault();
            $(".popup-overlay").hide();
        });
    };

    return {
        init: init
    }
})();

var deposit = (function() {

    var init = function() {
        _setUpListners();
    };

    var _setUpListners = function() {
        $("#showDeposit").on('click', function(e) {
            e.preventDefault();
            $(".popup-deposit").toggle();
        });

        $(".close_deposit").on("click", function(e) {
            e.preventDefault();
            $(".popup-deposit").hide();
        });
    };

    return {
        init: init
    }
})();

var payment = (function() {

    var init = function() {
        _setUpListners();
    };

    var _setUpListners = function() {

        $("#payment")
            .on(
                'click',
                function(e) {
                    var total = document.getElementById("total").cells
                        .item(1).innerHTML;
                    if (total == "0") {
                        showMessage('Заказ пустой. Закажите товары чтобы совершить заказ');
                    } else {
                        setTotalCost(total);
                        e.preventDefault();
                        $(".popup-pay").toggle();
                    }
                });

        $(".close-payment").on("click", function(e) {
            e.preventDefault();
            $(".popup-pay").hide();
        });

    };

    return {
        init: init
    }
})();

$('#delete_user').click(function(e) {
    e.preventDefault();
    $(".popup-user-delete").toggle();
});

$('#change_balance').click(function(e) {
    e.preventDefault();
    $(".popup-change-balance").toggle();
});


$("#close_change_balance").on("click", function(e) {
    e.preventDefault();
    $(".popup-change-balance").hide();
});

$("#close_delete_user").on("click", function(e) {
    e.preventDefault();
    $(".popup-user-delete").hide();
});

$("#change-balance").on('submit', function(e) {
    e.preventDefault();
    requestString = $(this).serialize();
    $.ajax({
    	 type: "POST",
         url: "/restraunt/controller",
         cache: false,
         data: requestString,
         dataType: "text",
         success: function(data) {
        	 
        	 $(".popup-change-balance").hide();
            
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else if (data == "not_allowed") {
            	
               showMessage('Операция недоступна для вашего аккаунта.');
            } else {
                showMessage("Балан успешно изменен");
            }

            $("#change-balance")[0].reset();
        }

    })

})




$("#user-delete-form").on('submit', function(e) {
    e.preventDefault();
    requestString = $(this).serialize();
    $.ajax({
    	 type: "POST",
         url: "/restraunt/controller",
         cache: false,
         data: requestString,
         dataType: "text",
         success: function(data) {
        	 
        	 $(".popup-user-delete").hide();
            
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else if (data == "not_allowed") {
            	
               showMessage('Операция недоступна для вашего аккаунта.');
            } 
             else {
                showMessage("Пользователь был успешно удален");
            }

        }

    })

})

var message = (function() {

    var init = function() {
        _setUpListners();
    };

    var _setUpListners = function() {
        $(".close_message").on("click", function(e) {
            e.preventDefault();
            $(".popup-message").hide();
        });
    };

    return {
        init: init
    }
})();

var items = (function() {

    var init = function() {
        _setUpListners();
    };

    var _setUpListners = function() {
        $(".close_items").on("click", function(e) {
            e.preventDefault();
            $(".popup-items").hide();
            $("#consist_items td").each(function() {
                $this = $(this)
                $this.remove();
            });
        });
    };

    return {
        init: init
    }
})();

var changePass = (function() {
    var changeButton = $('#change_pass'),
        accForm = $('.account__form');

    var _setupListeners = function() {
        changeButton.on('click', function(e) {
            e.preventDefault();
            accForm.slideToggle('slow');
        });

    }

    var init = function() {
        _setupListeners();
    }

    return {
        init: init
    }
})();

function showMessage(data) {
    $("#message").html(data);
    $(".popup-message").toggle();
};

function closePaymentForm() {

    $(".popup-pay").hide();
};

function closeDepositForm() {

    $(".popup-deposit").hide();
};

$(document).ready(function(e) {
    jssor_slider1_starter = function(containerId) {
        var options = {
            $AutoPlay: true
        };

        if (typeof($JssorSlider$) !== 'undefined') {
            var jssor_slider1 = new $JssorSlider$(containerId, options);
        }

    };

    jssor_slider1_starter('slider1_container');

    popup.init();
    message.init();
    payment.init();
    changePass.init();
    deposit.init();
    items.init();
});

function deleteRow(rowid) {
    var row = document.getElementById(rowid);
    var table = row.parentNode;
    while (table && table.tagName != 'TABLE')
        table = table.parentNode;
    if (!table)
        return;
    table.deleteRow(row.rowIndex);
}

function delivery(data) {
    if (data == 'no') {
        $("#delivery").hide();
    } else if (data == 'yes') {
        $("#delivery").toggle();
    }
}

function updateRow(rowid, data) {
    document.getElementById(rowid).cells.item(2).innerHTML = data;
}

function updateTotalCost(data) {
    document.getElementById("total").cells.item(1).innerHTML = data;
}

function setTotalCost(data) {
    document.getElementById("total_cost").innerHTML = data;
}

function setBalance(data) {
    document.getElementById("balance").innerHTML = data;
}

$(".popup_form").on('submit', function(e) {
    e.preventDefault();
    dataString = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "/restraunt/controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {
            if (data == "success")
                window.location.replace("/restraunt/staff/staffMain.jsp");
            else if (data == "not_valid") {
                $("#staff_message").html("Проверьте правильность введенных данных");
                $("input.popup_input").val('');
            } else {
                $("#staff_message").html('Ошибка, сервис недоступен');
                $("input.popup_input").val('');
            }
        }
    });
});

$(".add-item").on('submit', function(e) {
    e.preventDefault();
    dataString = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {
            if (data == "success")
                showMessage('Продукт успешно добавлен в корзину');
            else if (data == "need_login")
                showMessage('Войдите в систему чтобы заказать продукт');
            else
                showMessage('Ошибка, сервис недоступен');
        }
    });
});

$(".remove-item").on('submit', function(e) {
    e.preventDefault();
    id = $(this).find("input[name='itemId']").attr("value");
    dataString = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else {
                var json = $.parseJSON(data);
                if (json[0] == "0") {
                    deleteRow(id);
                    updateTotalCost(json[1]);
                } else {
                    updateRow(id, json[0]);
                }
                if ($("table.table-order tbody tr").length == 0) {
                    $("table.table-order tbody").append('<tr><td colspan="4">Корзина пуста</td></tr>');
                }
                updateTotalCost(json[1]);
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            showMessage('Сервис недоступен. Попробуйте позже');
        }
    });
});

$(".payment").on('submit',
    function(e) {
        e.preventDefault();
        dataString = $(this).serialize();
        $
            .ajax({
                type: "POST",
                url: "controller",
                cache: false,
                data: dataString,
                dataType: "text",
                success: function(data) {
                    closePaymentForm();
                    if (data == "success") {
                        updateTotalCost(0);
                        $("table.table-order tbody tr").each(
                            function() {
                                $this = $(this)
                                $this.remove();
                            });
                        showMessage('Заказ принят');
                    } else if (data == "need_money") {
                        showMessage('У вас недостаточно средств на счете. Пополните баланс');
                    } else if (data == "empty_order") {
                        showMessage('Заказ пустой. Закажите товары чтобы совершить заказ');
                    } else
                        showMessage('Сервис недоступен. Попробуйте позже');
                }
            });
    });

$(".deposit").on('submit', function(e) {
    e.preventDefault();
    dataString = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "/restraunt/controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {
            closeDepositForm();
            if (data == "need_login") {
                showMessage('Войдите в сиcтему, чтобы пополнить баланс');
            } else if (data == "failed")
                showMessage('Ошибка, сервис недоступен.');
            else {
                setBalance(data);
                showMessage('Счет успешно пополнен');
            }
        }
    });
});

$(".change_password").on('submit', function(e) {
    e.preventDefault();
    dataString = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "/restraunt/controller",
        cache: false,
        data: dataString,
        dataType: "text",
        success: function(data) {
            $('.account__form').slideToggle('slow');
            if (data == "success") {
                showMessage('Пароль успешно изменен');
            } else if (data == "need_login")
                showMessage('Войдите в сиситему, чтобы изменить пароль');
            else if (data == "wrong_pass")
                showMessage('Неправильный пароль, попробуйте еще раз.');
            else
                showMessage('Ошибка, сервис недоступен.');
        }
    });
});


$("#list_order").on('click', 'tr', function(e) {
    e.preventDefault();

    id = $(this).find("td:first-child").text();
    requestString= {
            action: "view_order_items",
            idOrder: id
        };
    $.ajax({
        type: "POST",
        url: "controller",
        cache: false,
        data: requestString,
        dataType: "text",
        success: function(data) {
            if (data == "failed") {
                showMessage('Сервис недоступен. Попробуйте позже');
            } else if (data == "need_login") {
                showMessage('Войдите в сиситему чтобы осуществить операцию');
            } else {
                var json = $.parseJSON(data);
                $.each(json, function(key, value) {
                    $("#consist_items").append(
                        "<tr> <td>" + key + "</td><td> x " + value + "</td></tr>");
                });
                $(".popup-items").toggle();
            }
        }
    });
    
}
)

