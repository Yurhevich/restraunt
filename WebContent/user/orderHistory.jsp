<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="resources.localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="history.ribbon" var="ribbon"/>
<fmt:message bundle="${loc}" key="history.table.title" var="title"/>
<fmt:message bundle="${loc}" key="history.table.column.number" var="num"/>
<fmt:message bundle="${loc}" key="history.table.column.date" var="date"/>
<fmt:message bundle="${loc}" key="history.table.column.time" var="time"/>
<fmt:message bundle="${loc}" key="history.table.column.cost" var="cost"/>
<fmt:message bundle="${loc}" key="history.table.column.state" var="state"/>
<fmt:message bundle="${loc}" key="history.button.menu" var="button_menu"/>
<fmt:message bundle="${loc}" key="history.button.info" var="button_info"/>
<fmt:message bundle="${loc}" key="history.table.rubles" var="rubles"/>
<fmt:message bundle="${loc}" key="history.popup.title" var="popup_title"/>
<fmt:message bundle="${loc}" key="history.popup.count" var="count"/>
<fmt:message bundle="${loc}" key="history.popup.product" var="product"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>${ribbon}</title>

<link rel="stylesheet" href="/restraunt/css/main.css" />
</head>
<body>

	<div class="wrapper">
		<c:import url="/common/fragment/userHeader.jsp" />
		<div class="content">
			<div class="main-content">

				<h2 class="info-content-title">${ribbon}</h2>

				<table class="simple-little-table" cellspacing='0'>
					<colgroup>

						<col style="width: 10%">
						<col style="width: 10%">
						<col style="width: 10%">
						<col style="width: 20%">
						<col style="width: 43%">

					</colgroup>
					<caption>${title}</caption>
					<thead>
					<tr>
						<th>${num}</th>
						<th>${date}</th>
						<th>${time}</th>
						<th>${cost}</th>
						<th>${state}</th>

					</tr>
					</thead>
                    <tbody id="list_order">
					<c:forEach var="elem" items="${orders}">
						<form class="view-order" action="/restraunt/controller">
							<input type="text" value="view_order_items" hidden="true"
								name="action" /> <input type="text" value="${elem.id}"
								hidden="true" name="idOrder" />
								</form>
							<tr>
							
								<td>${elem.id}</td>
								<td><fmt:formatDate pattern="yyyy-MM-dd"
										value="${elem.time}" /></td>
								<td><fmt:formatDate type="time" timeStyle="short"
										value="${elem.time}" /></td>
								<td>${elem.totalCost} ${rubles}</td>
								<td>${elem.state}</td>

							</tr>

					</c:forEach>
					</tbody>
				</table>

				<div class="center_pagination">
					<ul class="pagination">

						<c:if test="${max_page ne 1}">
							<c:if test="${left_switch != null}">
								<li><a class="switch-pages"
									href="/restraunt/controller?action=view_history&page=${left_switch}">«</a></li>
							</c:if>
							<c:forEach begin="${min_page}" end="${max_page}" var="val">
								<c:choose>
									<c:when test="${cur_page == val}">
										<li><a
											href="/restraunt/controller?action=view_history&page=${val}"
											class="page active">${val}</a></li>
									</c:when>
									<c:otherwise>
										<li><a
											href="/restraunt/controller?action=view_history&page=${val}"
											class="page">${val}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<c:if test="${right_switch != null}">
								<li><a class="switch-pages"
									href="/restraunt/controller?action=view_history&page=${right_switch}">»</a></li>
							</c:if>

						</c:if>

					</ul>
				</div>


				<div class="row-button-2">
					<a class="left-button popup_submit"
						href="/restraunt/controller?action=view_menu">${button_menu}</a> <a
						class="right-button popup_submit"
						href="/restraunt/user/accinfo.jsp">${button_info}</a>
					<div class="clear-div"></div>
				</div>


			</div>
		</div>
		<c:import url="/common/fragment/footer.jsp" />
	</div>

	<div class="popup-items">
		<div class="popup">
			<div class="popup_row">
				<label for="name" class="popup_label message_title">${popup_title}:</label>
				<table class="consist_table">
					<thead>
						<tr>
							<th>${product}</th>
							<th>${count}</th>
						</tr>
					</thead>
					<tbody id="consist_items">
					</tbody>
				</table>
			</div>
			<div class="popup_row">
				<button type="submit" class="popup_submit close_items">OK</button>
			</div>
			<a href="#" class="close_items close_button">X</a>
		</div>
	</div>




	<script src="/restraunt/js/vendors/jquery-1.9.1.min.js"></script>
	<script src="/restraunt/js/main.js"></script>
</body>
</html>