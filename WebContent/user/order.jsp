<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="resources.localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="order.title" var="title"/>
<fmt:message bundle="${loc}" key="order.ribbon" var="ribbon"/>
<fmt:message bundle="${loc}" key="order.table.column.1" var="column_1"/>
<fmt:message bundle="${loc}" key="order.table.column.2" var="column_2"/>
<fmt:message bundle="${loc}" key="order.table.column.3" var="column_3"/>
<fmt:message bundle="${loc}" key="order.button.menu" var="button_menu"/>
<fmt:message bundle="${loc}" key="order.button.pay" var="button_pay"/>
<fmt:message bundle="${loc}" key="order.form.title" var="pay_title"/>
<fmt:message bundle="${loc}" key="order.form.content.1" var="pay_content"/>
<fmt:message bundle="${loc}" key="order.form.content.checkbox.deloff" var="del_off"/>
<fmt:message bundle="${loc}" key="order.form.content.checkbox.delon" var="del_on"/>
<fmt:message bundle="${loc}" key="order.form.content.address" var="address"/>
<fmt:message bundle="${loc}" key="order.form.content.telephone" var="telephone"/>
<fmt:message bundle="${loc}" key="order.form.submit" var="pay_submit"/>
<fmt:message bundle="${loc}" key="order.form.rubles" var="rubles"/>
<fmt:message bundle="${loc}" key="order.form.content.2" var="content_2"/>
<fmt:message bundle="${loc}" key="order.form.content.3" var="content_3"/>
<fmt:message bundle="${loc}" key="order.button.remove" var="button_remove"/>
<fmt:message bundle="${loc}" key="order.table.total" var="total"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>${title}</title>

<link rel="stylesheet" href="css/main.css" />
</head>
<body>

	<div class="wrapper">
		<c:import url="/common/fragment/userHeader.jsp" />
		<div class="content">
			<div class="main-content">

				<h2 class="info-content-title">${title}</h2>



				<table class="table-order" cellspacing='0'>
				<colgroup>
								<col style="width: 40%">
								<col style="width: 20%">
								<col style="width: 20%">
								<col style="width: 20%">
							</colgroup>
					<caption>${ribbon}</caption>
					<thead>
						<tr>
							<th>${column_1}</th>
							<th>${column_2}</th>
							<th>${column_3}</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<c:choose>
					<c:when test="${not empty order.items}">
						<c:forEach var="entry" items="${order.items}">
							<tr id="${entry.key.id}">
								<td>${entry.key.name}</td>
								<td>${entry.key.price}</td>
								<td>${entry.value}</td>
								<td><form class="remove-item"
										action="/restraunt/controller">
										<input type="text" value="remove_from_cart" hidden="true"
											name="action" /> <input type="text" value="${entry.key.id}"
											hidden="true" name="itemId" />
										<button class="popup_submit" type="submit">${button_remove}</button>
									</form></td>
							</tr>
						</c:forEach>
						</c:when>
						<c:otherwise>
						<tr><td colspan="4">Корзина пуста</td></tr>
						</c:otherwise>
						</c:choose>
					</tbody>
					<tfoot>
						<tr id="total">
							<td colspan="3">${total}</td>
							<td>${order.totalCost}</td>
						</tr>
					</tfoot>
				</table>

				<div class="row-button">
					<a class="left-button popup_submit"
						href="/restraunt/controller?action=view_menu">${button_menu}</a>
					<button class="right-button popup_submit" id="payment">${button_pay}</button>
					<div class="clear-div"></div>
				</div>


			</div>
		</div>
		<c:import url="/common/fragment/footer.jsp" />
	</div>

	<div class="popup-pay">
		<div class="popup">
			<form class="payment" action="/restraunt/controller" method="post">
				<input type="hidden" name="action" value="pay" />
				<div class="popup_row">
					<label for="name" class="popup_label message_title">${pay_title}:</label>
				</div>
				<div class="popup_row">
					<p>
						<b>${pay_content}:</b>
					</p>
					<p>
						<input type="radio" id="radio1" name="inPlace" value="yes"
							class="delivery-checkbox" onclick="delivery('no');" checked /> <label
							for="radio1" class="button-label">${del_off}</label> <input
							type="radio" id="radio2" name="inPlace" value="no"
							class="delivery-checkbox" onclick="delivery('yes');" /> <label
							for="radio2" class="button-label">${del_on}</label>
					</p>

					<div id="delivery">

						<p>
							<input type="text" name="address" placeholder="${address}"
								class="popup_input" />
						</p>
						<p>
							<input type="text" name="telephone" placeholder="${telephone}"
								class="popup_input" />
						</p>
					</div>
					<p>
						<b>${content_2}:</b> ${client.balance} ${rubles}
					</p>
					<p>
						<b>${content_3}:</b> <b id="total_cost"></b> ${rubles}
					</p>

				</div>
				<div class="popup_row">
					<button type="submit" class="popup_submit">${pay_submit}</button>
				</div>
			</form>
			<a href="#" class="close_button close-payment">X</a>
		</div>
	</div>


	<script src="js/vendors/jquery-1.9.1.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>