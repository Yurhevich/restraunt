<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="resources.localization.local" var="loc"/>
<fmt:message bundle="${loc}" key="accinfo.ribbon" var="ribbon"/>
<fmt:message bundle="${loc}" key="accinfo.title" var="title"/>
<fmt:message bundle="${loc}" key="accinfo.name" var="name"/>
<fmt:message bundle="${loc}" key="accinfo.surname" var="surname"/>
<fmt:message bundle="${loc}" key="accinfo.username" var="username"/>
<fmt:message bundle="${loc}" key="accinfo.email" var="email"/>
<fmt:message bundle="${loc}" key="accinfo.button.menu" var="button_menu"/>
<fmt:message bundle="${loc}" key="accinfo.button.deposit" var="button_deposit"/>
<fmt:message bundle="${loc}" key="accinfo.button.history" var="button_history"/>
<fmt:message bundle="${loc}" key="accinfo.button.change" var="button_change"/>
<fmt:message bundle="${loc}" key="accinfo.button.submitchange" var="submit_change"/>
<fmt:message bundle="${loc}" key="accinfo.change.old" var="old_pass"/>
<fmt:message bundle="${loc}" key="accinfo.change.new" var="new_pass"/>
<fmt:message bundle="${loc}" key="accinfo.balance.info" var="balance_info"/>
<fmt:message bundle="${loc}" key="accinfo.balance.rubles" var="rubles"/>
<fmt:message bundle="${loc}" key="accinfo.balance.form.title" var="deposit_title"/>
<fmt:message bundle="${loc}" key="accinfo.balance.form.message" var="deposit_message"/>
<fmt:message bundle="${loc}" key="accinfo.balance.form.submit" var="deposit_submit"/>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<link rel="stylesheet" href="/restraunt/css/main.css" />
<title>${ribbon}</title>
</head>
<body>
	<div class="wrapper">
		<c:import url="/common/fragment/userHeader.jsp" />
		<div class="content">
			<div class="main-content">
				<h2 class="info-content-title">${ribbon}</h2>
				<h2 class="content-title">${title}</h2>


				<div class="account__container ">
					<div class="accinfo_sidebar">
						<div class="accinfo_menu">
							<c:choose>
								<c:when test="${not empty sessionScope.client}">
									<ul>
										<li><a class="account_button"
											href="/restraunt/controller?action=view_menu">${button_menu}</a></li>
										<li><button type="submit" id="showDeposit"
												class="account_button">${button_deposit}</button></li>
										<li><a class="account_button"
											href="/restraunt/controller?action=view_history">${button_history}</a></li>
									</ul>
								</c:when>
								<c:otherwise>
									<ul>
									<li><a class="account_button"
											href="/restraunt/staff/staffMain.jsp">Назад к главной</a></li>
										<li><button type="submit" id="change_balance"
												class="account_button">Изменить баланс</button></li>
										<li><button type="submit" 
												class="account_button" id="delete_user">Удалить аккаунт
											</button></li>
											
					
									</ul>
								</c:otherwise>
							</c:choose>
						</div>


					</div>

					<div class="accinfo_content">
						<ul class="account__list">

							<li class="account__item"><span class="account__key">${name}:
							</span><span class="account__value">${client.name}</span></li>
							<li class="account__item"><span class="account__key">${surname}:
							</span><span class="account__value">${client.surname}</span></li>
							<li class="account__item"><span class="account__key">${username}:</span><span
								class="account__value">${client.login}</span></li>
							<li class="account__item"><span class="account__key">email:
							</span><span class="account__value">${client.email}</span></li>
						</ul>
						<c:if test="${not empty sessionScope.client}">
						<a href="#" id="change_pass" class="change-password">${button_change}:</a>
							</c:if>
						<div class="account__form">
							<form class="change_password" action="/restraunt/controller"
								method="post">
								<input type="hidden" name="action" value="change_password" />
								<div class="form__row">
									<label for="pass" class="form__label">${old_pass}:</label> <input
										type="password" name="password" id="pass" />
								</div>
								<div class="form__row">
									<label for="accept_pass" class="form__label">${new_pass}:
									</label> <input type="password" name="newPassword"
										id="accept_pass" />
								</div>
								<div class="form__row">
									<button type="submit" class="popup_submit">${submit_change}</button>
								</div>
							</form>
						</div>

						<p>
							${balance_info} : <b id="balance">${client.balance}</b> ${rubles}
						</p>

					</div>

				</div>
			</div>
		</div>
		<c:import url="/common/fragment/footer.jsp" />

	</div>

	<div class="popup-deposit">
		<div class="popup">
			<form class="deposit" action="/restraunt/controller" method="post">
				<input type="hidden" name="action" value="deposit" />
				<div class="popup_row">
					<label for="name" class="popup_label message_title">${deposit_title}:</label>
				</div>
				<div class="popup_row">
					<p class="popup_label ">${deposit_message} :</p>
				</div>
				<p>
					<input type="number" name="sum" min="0" value=""
						class="popup_input" required /> <b>${rubles}</b>
				</p>

				<div class="popup_row">
					<p></p>
					<button type="submit" class="popup_submit">${deposit_submit}</button>
				</div>
				<a href="#" class="close_button close_deposit">X</a>
			</form>
		</div>
	</div>


<div class="popup-change-balance">
		<div class="popup">
			<form id="change-balance" action="/restraunt/controller" method="post">
				<input type="hidden" name="action" value="change_balance" />
				<input type="text" name="id" value="${client.id}" hidden="true">
				<div class="popup_row">
					<label for="name" class="popup_label message_title">Изменение счета:</label>
				</div>
				<div class="popup_row">
					<p class="popup_label ">Введите сумму счета :</p>
				</div>
				<p>
					<input type="number" name="sum" min="0" value=""
						class="popup_input" placeholder="Сумма" /> <b>рублей</b>
				</p>

				<div class="popup_row">
					<p></p>
					<button type="submit" class="popup_submit">Пополнить</button>
				</div>
				<a href="#" class="close_button" id="close_change_balance">X</a>
			</form>
		</div>
	</div>

	<div class="popup-user-delete">
		<div class="popup">
			<form id="user-delete-form" action="/restraunt/controller"
				method="post">
				<input type="text" name="action" value="remove_user" hidden="true">
				<input type="text" name="id" value="${client.id}" hidden="true">
				<div class="popup_row">
					<p class="popup_label message_title">Подтверждение :</p>
				</div>

				<div class="popup_row">
					<p class="popup_label">Вы уверены что хотите удалить пользователя?</p>
				</div>

				<div class="popup_row">
					<p></p>
					<button type="submit" class="popup_submit">Удалить</button>
				</div>
				<a href="#" class="close_button" id="close_delete_user">X</a>
			</form>
		</div>
	</div>


	<script src="/restraunt/js/vendors/jquery-1.9.1.min.js"></script>
	<script src="/restraunt/js/main.js"></script>
</body>
</html>